<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHakAksesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hak_akses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_menu')->unsigned()->index()->nullable();
            $table->integer('id_role')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign("id_menu")->references("id")->on("master_menu")->onDelete("cascade");
            $table->foreign("id_role")->references("id")->on("master_role")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hak_akses');
    }
}
