<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdPlayerToMasterAxie extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_axie', function (Blueprint $table) {
            //
            $table->integer('id_player')->unsigned()->nullable()->default(NULL);

            $table->foreign("id_player")->references("id")->on("users")->onDelete("set null");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_axie', function (Blueprint $table) {
            //
        });
    }
}
