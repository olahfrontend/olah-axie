<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalCutiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_cuti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->date("tanggal_cuti_mulai");
            $table->date("tanggal_cuti_selesai");
            $table->string("alasan");
            $table->timestamps();

            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_cuti');
    }
}
