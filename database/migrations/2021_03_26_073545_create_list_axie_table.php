<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Psr\Log\LogLevel;

class CreateListAxieTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_axie', function (Blueprint $table) {
            $table->increments('id');
            $table->string("email_akun");
            $table->string("negara");
            $table->integer("level")->default(1);
            $table->integer("status_terpakai")->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_axie');
    }
}
