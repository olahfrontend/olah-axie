<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string("no_telp")->nullable();
            $table->string("nama_darurat_1")->nullable();
            $table->string("no_telp_darurat_1")->nullable();
            $table->string("hubungan_darurat_1")->nullable();
            $table->string("nama_darurat_2")->nullable();
            $table->string("no_telp_darurat_2")->nullable();
            $table->string("hubungan_darurat_2")->nullable();
            $table->string("alamat")->nullable();
            $table->string("no_ktp")->nullable();
            $table->string("foto_ktp")->nullable();
            $table->string("foto_selfie")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
