<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDailyReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daily_report', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->integer('id_axie')->unsigned()->index()->nullable();
            $table->integer('slp_adventure')->nullable();
            $table->integer('slp_pvp')->nullable();
            $table->integer('slp_quest')->nullable();
            $table->integer('last_inventory')->nullable();
            $table->timestamps();
            
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("id_axie")->references("id")->on("master_axie")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daily_report');
    }
}
