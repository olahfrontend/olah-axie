<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouchUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('couch_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_coach')->unsigned()->index()->nullable();
            $table->integer('id_user')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign("id_coach")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("id_user")->references("id")->on("users")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('couch_users');
    }
}
