<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailJadwalCutiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_jadwal_cuti', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_jadwal_cuti')->unsigned()->index()->nullable();
            $table->integer('id_pengganti')->unsigned()->index()->nullable();
            $table->integer('id_axie')->unsigned()->index()->nullable();
            $table->timestamps();

            $table->foreign("id_pengganti")->references("id")->on("users")->onDelete("cascade");
            $table->foreign("id_jadwal_cuti")->references("id")->on("jadwal_cuti")->onDelete("cascade");
            $table->foreign("id_axie")->references("id")->on("master_axie")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_jadwal_cuti');
    }
}
