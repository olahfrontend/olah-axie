<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogMmrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_mmr', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_axie')->unsigned()->index()->nullable();
            $table->integer('mmr')->nullable();
            $table->timestamps();

            $table->foreign("id_axie")->references("id")->on("master_axie")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_mmr');
    }
}
