<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddDetailToMasterMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_menu', function (Blueprint $table) {
            //
            $table->string('url')->nullable()->default("");
            $table->string('active_page')->nullable()->default("");
            $table->string('icon_fa')->nullable()->default("");
            $table->integer('have_submenu')->nullable()->default(0);
            $table->integer("urutan")->nullable()->default(99);
            $table->string('id_parent_menu')->nullable()->default(NULL);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_menu', function (Blueprint $table) {
            //
        });
    }
}
