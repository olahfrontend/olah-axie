<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get("/admin", "Admin\LoginController@index");

Route::post("/admin/do-login", "Admin\LoginController@dologin");
Route::get("/admin/dashboard", "Admin\DashboardController@index");
Route::get("/admin/dashboard/detail", "Admin\DashboardController@detail");
Route::post("/logout", "Admin\MasterUserController@logout");

Route::get("/admin/master_user", "Admin\MasterUserController@index");
Route::get("/admin/list_cuti", "Admin\MasterUserController@listcuti");
Route::post("/admin/master_user/do_edit_cuti", "Admin\MasterUserController@doeditcuti");
Route::get("/admin/list_reimburs", "Admin\MasterUserController@listreimburs");
Route::get("/admin/list_denda", "Admin\MasterUserController@listdenda");
Route::get("/admin/master_user/add", "Admin\MasterUserController@add");
Route::post("/admin/master_user/do_add", "Admin\MasterUserController@doadd");

Route::get("/admin/master_user/add_cuti", "Admin\MasterUserController@addCuti");
Route::post("/admin/master_user/do_add_cuti", "Admin\MasterUserController@doAddCuti");
Route::post("/admin/master_user/do_batal_cuti", "Admin\MasterUserController@doBatalCuti");

Route::post("/admin/master_user/do_edit_kantor", "Admin\MasterUserController@do_edit_kantor");
Route::post("/admin/master_user/do_edit_detail", "Admin\MasterUserController@do_edit_detail");
Route::get("/admin/master_user/start_command_cuti", "Admin\MasterUserController@startCommandCuti");
Route::post("/admin/master_user/toggle_status_pegawai/{id}", "Admin\MasterUserController@toggle_status_pegawai");
Route::get("/admin/master_user/add_denda", "Admin\MasterUserController@addDenda");
Route::post("/admin/master_user/do_add_denda", "Admin\MasterUserController@do_add_denda");
Route::get("/admin/master_user/laporan_daily/{id}", "Admin\MasterUserController@laporan_daily");
Route::get("/admin/master_user/detail_coach/{id}", "Admin\MasterUserController@detailCoach");
Route::get("/admin/master_user/laporan/{id}", "Admin\MasterUserController@detailLaporan");
Route::get("/admin/master_user/detail_cuti/{id}", "Admin\MasterUserController@detailCuti");
Route::get("/admin/master_user/{id}", "Admin\MasterUserController@detail");

Route::get("/admin/master_axie", "Admin\MasterAxieController@index");
Route::get("/admin/master_axie/add", "Admin\MasterAxieController@add");
Route::post("/admin/master_axie/do_add", "Admin\MasterAxieController@doadd");
Route::get("/admin/master_axie/{id}", "Admin\MasterAxieController@detail");
Route::post("/admin/master_axie/do_edit", "Admin\MasterAxieController@doedit");
Route::post("/admin/master_axie/take_axie", "Admin\MasterAxieController@takeAxie");
Route::post("/admin/do_topup_slp", "Admin\MasterAxieController@doTopupSLP");
Route::post("/admin/do_withdraw_slp", "Admin\MasterAxieController@doWithdrawSLP");

Route::get("/admin/daily_report", "Admin\DailyReportController@index");
Route::get("/admin/daily_report/{id}", "Admin\DailyReportController@detail");
Route::get("/admin/do_approve", "Admin\DailyReportController@approve");

Route::get("/admin/log_server", "Admin\MasterUserController@log");

Route::get("/admin/master_setting", "Admin\MasterSettingController@index");
Route::post("/admin/setting/update_general", "Admin\MasterSettingController@updateGeneral");
Route::post("/admin/master_setting/do_add_setting", "Admin\MasterSettingController@do_add_setting");
Route::post("/admin/master_setting/do_edit_setting", "Admin\MasterSettingController@do_update_setting");
Route::post("/admin/master_setting/do_delete_setting", "Admin\MasterSettingController@do_delete_setting");
Route::get("/admin/laporan_daily_report", "Admin\DashboardController@laporan_daily_report");
Route::post("/admin/master_setting/do_add_setting_pvp", "Admin\MasterSettingController@do_add_setting_pvp");
Route::post("/admin/master_setting/do_edit_setting_pvp", "Admin\MasterSettingController@do_update_setting_pvp");
Route::post("/admin/master_setting/do_delete_setting_pvp", "Admin\MasterSettingController@do_delete_setting_pvp");

Route::get("/admin/axie_tracker", "Admin\DashboardController@axie_live");
Route::get("/admin/search_axie_tracker", "Admin\DashboardController@search_axie_tracker");
Route::get("/admin/detail_tracker/{id}", "Admin\DashboardController@detail_axie_tracker");

Route::get("/admin/master_kantor", "Admin\MasterKantorController@index");
Route::get("/admin/master_kantor/add", "Admin\MasterKantorController@add");
Route::post("/admin/master_kantor/do_add", "Admin\MasterKantorController@doadd");
Route::post("/admin/master_kantor/do_edit", "Admin\MasterKantorController@doedit");
Route::get("/admin/master_kantor/{id}", "Admin\MasterKantorController@detail");

Route::get("/admin/hak_akses", "Admin\MasterRoleController@index");
Route::get("/admin/hak_akses/add", "Admin\MasterRoleController@viewadd");
Route::post("/admin/hak_akses/do_add", "Admin\MasterRoleController@doadd");
Route::post("/admin/hak_akses/do_edit", "Admin\MasterRoleController@doedit");
Route::get("/admin/hak_akses/{id}", "Admin\MasterRoleController@detail");

Route::post("/admin/do_adjustment", "Admin\DailyReportController@adjustment");
Route::post("/admin/uploadpp", "Admin\MasterUserController@uploadPhoto");
