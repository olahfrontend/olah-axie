<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//USER
Route::post("/user/register", "WebServices\UserWebService@register");
Route::post("/user/login", "WebServices\UserWebService@login");
Route::post("/user/update", "WebServices\UserWebService@updateProfile");
Route::post("/user/add_contact", "WebServices\UserWebService@addContact");
Route::get("/user/get_contact", "WebServices\UserWebService@getContactCount");
Route::post("/user/logout", "WebServices\UserWebService@logout");
Route::post("/user/change_password", "WebServices\UserWebService@changePassword");
Route::post("/user/update_password", "WebServices\UserWebService@updatePassword");

Route::get("/user/list", "WebServices\UserWebService@getList");
Route::get("/user/detail", "WebServices\UserWebService@getDetailPegawai");

Route::get("/initial_data", "WebServices\UserWebService@getInitialData");

//AXIE
Route::get("/axie/list", "WebServices\AxieWebServices@getListAxie");
Route::post("/axie/add", "WebServices\AxieWebServices@addAxie");
Route::post("/axie/take", "WebServices\AxieWebServices@takeAxie");
Route::get("/axie/my_axie", "WebServices\AxieWebServices@myAxie");
Route::get("/axie/detail", "WebServices\AxieWebServices@getDetailAxie");
Route::post("/axie/update", "WebServices\AxieWebServices@updateAxie");
Route::post("/axie/updatelevel", "WebServices\AxieWebServices@updateLevel");
Route::post("/axie/topup_slp", "WebServices\AxieWebServices@topupSLP");
Route::post("/axie/withdraw_slp", "WebServices\AxieWebServices@withdrawSLP");
Route::get("/axie/search", "WebServices\AxieWebServices@searchAxie");

//REPORT
Route::post("/report/set_daily", "WebServices\ReportWebServices@dailyReport");
Route::get("/report/get_my_daily_report", "WebServices\ReportWebServices@getMyDailyReport");
Route::post("/report/approve", "WebServices\ReportWebServices@approveDailyReport");
Route::get("/report/get_laporan_gaji_weekly", "WebServices\ReportWebServices@getLaporanGajiWeekly");
Route::get("/report/get_slp_report", "WebServices\ReportWebServices@getLaporanSLP");
Route::get("report/daily_report_admin", "WebServices\ReportWebServices@getListReport");
Route::get("/report/history_slp_axie", "WebServices\ReportWebServices@report_axie");
Route::get("/report/get_log_gaji", "WebServices\ReportWebServices@get_log_gaji");

Route::get("/log", "WebServices\UserWebService@getLogServer");
Route::get("/get_leaderboard", "WebServices\UserWebService@getLeaderboard");

Route::post("/denda/add", "WebServices\DendaInventoriWebServices@addDenda");

//SETTING
Route::get("/get_general_setting", "WebServices\SettingWebServices@get_setting");
Route::get("/get_level_setting", "WebServices\SettingWebServices@get_setting_level");
Route::get("/get_pvp_setting", "WebServices\SettingWebServices@get_setting_pvp");
Route::post("/add_setting_level", "WebServices\SettingWebServices@add_setting_level");
Route::post("/edit_setting_level", "WebServices\SettingWebServices@edit_setting_level");
Route::post("/delete_setting_level", "WebServices\SettingWebServices@delete_setting_level");
Route::post("/add_setting_pvp", "WebServices\SettingWebServices@add_setting_pvp");
Route::post("/edit_setting_pvp", "WebServices\SettingWebServices@edit_setting_pvp");
Route::post("/delete_setting_pvp", "WebServices\SettingWebServices@delete_setting_pvp");


//CUTI
Route::post("/do_add_detail_cuti", "WebServices\UserWebService@do_add_detail_cuti");
Route::get("/get_detail_cuti", "WebServices\UserWebService@get_detail_cuti");
Route::post("/delete_jadwal_cuti", "WebServices\UserWebService@delete_detail_cuti");

//LAPORAN
Route::get("/laporan/slp_by_owner", "WebServices\LaporanWebServices@laporan_slp_by_owner");
Route::get("/laporan/laporan_pegawai", "WebServices\LaporanWebServices@laporan_pegawai");
Route::get("/laporan/laporan_daily_monthly", "WebServices\LaporanWebServices@laporan_daily_monthly");
Route::get("/laporan/get_laporan_daily_monthly", "WebServices\LaporanWebServices@get_laporan_monthly");
Route::get("/laporan/laporan_gain_slp", "WebServices\LaporanWebServices@get_laporan_gain_slp");
Route::get("/laporan/perolehan_slp", "WebServices\LaporanWebServices@get_laporan_perolehan_slp");
Route::get("/laporan/pengeluaran", "WebServices\LaporanWebServices@getLaporanPengeluaran");

Route::post("/add_kantor", "WebServices\UserWebService@add_kantor");

Route::get("/get_dashboard", "WebServices\UserWebService@get_dashboard");

Route::post("/reimburs/add", "WebServices\UserWebService@add_reimburs");
Route::post("/reimburs/approve", "WebServices\UserWebService@approve_reimburs");
Route::get("/reimburs/list", "WebServices\UserWebService@get_list_reimburs");

//COACH
Route::get("/coach/get", "WebServices\UserWebService@get_coach_user");
Route::post("/coach/add", "WebServices\UserWebService@add_coach_user");
Route::post("/coach/delete", "WebServices\UserWebService@delete_coach_user");

Route::get("/test", "WebServices\SettingWebServices@test");
