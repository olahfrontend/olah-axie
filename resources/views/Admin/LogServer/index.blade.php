@include('Admin.header',['activePage' => 'log_server'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<div class="main">
    <div class="loading-overlay" id="loading-overlay">
        <i class="fa fa-spinner fa-spin spin-big"></i>
        <h5>Please Wait ...</h5>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Log Server</h3>
            </div>
            <div class="form-group" style="padding:25px;text-align:center;">
                <label>Tanggal Log Server</label>
                <input type="date" id="tanggal" class="form-control input-lg" onchange="loaddata()">
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        <tr>
                            <td>{{ $dt->created_at }}</td>
                            <td>{{ strtoupper($dt->description) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Log Server Error</h3>
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Status</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_server_error as $dt)
                        <tr>
                            <td>{{ $dt->created_at }}</td>
                            <td>{{ $dt->status }}</td>
                            <td>{{ strtoupper($dt->description) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
        $('#myTable2').DataTable({
            "order": [
                [0, "desc"]
            ]
        });

        console.log("ASd");
        // var datenow = new Date();
        let now = new Date();
        var datenow = moment(now).format("YYYY-MM-DD");
        console.log(datenow);
        $("#tanggal").val(datenow);
        loaddata();
    });

    function loaddata() {
        $("#loading-overlay").css("visibility", "visible");
        date = $("#tanggal").val();
        date = moment(date).format("MM/DD/YYYY");
        $.ajax({
            type: "GET",
            url: "/api/log?date=" + date,
            success: function(data) {
                // console.log(data.payload);
                var t = $("#myTable").DataTable();
                t.clear();
                t.draw();
                data.payload.forEach(function(item, index) {
                    t.row.add([
                        item.created_at,
                        item.description
                    ]).draw(false);
                });

                $("#loading-overlay").css("visibility", "hidden");
            }
        });
    }
</script>