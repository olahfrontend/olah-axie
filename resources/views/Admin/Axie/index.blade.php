@include('Admin.header',['activePage' => 'master_axie'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .btn {
        border-radius: 15px;
    }

    .spin-big {
        font-size: 50px;
        height: 50px;
        width: 50px;
        color: #74B7BD;
    }

    .loading-overlay {
        visibility: hidden;
        width: 100%;
        height: 100%;
        padding-left: calc(50vw - 100px);
        padding-top: calc(50vh - 100px);
        position: fixed;
        color: white;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 99;
    }

    .loading-overlay h5 {
        font-weight: bold;
        font-size: 18px;
        margin-left: -20px;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }
</style>
<div class="main">
    <div class="loading-overlay" id="loading-overlay">
        <i class="fa fa-spinner fa-spin spin-big"></i>
        <h5>Please Wait ...</h5>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Axie</h3>
                <a href="/admin/master_axie/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Axie</div>
                </a>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Email Akun</th>
                            <th>Player</th>
                            <th>Level</th>
                            <th>Inventory SLP</th>
                            <th>Status Boss</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        <tr>
                            <td>{{ $dt->email_akun }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->level }}</td>
                            <td>{{ $dt->total_slp }}</td>
                            <td>{{ $dt->status_boss == 1 ? 'Clear' : 'Not Clear' }} </td>
                            <td>
                                <a href="{{ URL('admin/master_axie/') . '/' . $dt->id }}">
                                    <div class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </div>
                                </a>
                                <div class="btn btn-success" onclick="openModalTopup({{$dt->id}})">
                                    <i class="fa fa-download"></i>
                                </div>
                                <div class="btn btn-danger" onclick="openModalWithdraw({{$dt->id}},{{$dt->total_slp}})">
                                    <i class="fa fa-upload"></i>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">History Topup & Withdrawal</h3>
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Admin</th>
                            <th>Email Akun</th>
                            <th>Last Inventory</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_history as $dt)
                        <tr>
                            <td>{{ $dt->created_at }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->email_akun }}</td>
                            <td>{{ $dt->last_inventory }}</td>
                            <td>{{ $dt->slp_quest }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

<div id="modalTopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Topup SLP</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form id="form_topup" action="{{ URL('/admin/do_topup_slp') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_topup" name="id_axie" />
                        <input type="number" step="any" id="value_topup" name="amount" /> SLP<br><br>
                        <button id="btn_topup" class="btn btn-primary" onclick="clickButton('topup')">TOPUP</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalWithdraw" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Withdraw SLP</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form id="form_withdraw" action="{{ URL('/admin/do_withdraw_slp') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_withdraw" name="id_axie" />
                        <input type="number" step="any" id="value_withdraw" name="amount" /> SLP<br><br>
                        <div class="btn btn-info" onclick="max()">MAX</div>
                        <button id="btn_withdraw" class="btn btn-primary" onclick="clickButton('withdraw')">Withdraw</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    //FUNCTION SHOW MODAL
    function openModalTopup(id) {
        console.log(id);
        $("#modalTopup").modal();

        $("#id_topup").val(id);
    }

    function clickButton(type) {
        if (type == 'withdraw') {
            $("#btn_withdraw").prop("disabled", "disabled");
            $("#btn_withdraw").html("Please wait...");
            $("#form_withdraw").submit();
        } else {
            $("#btn_topup").prop("disabled", "disabled");
            $("#btn_topup").html("Please wait...");
            $("#form_topup").submit();
        }
    }

    var max_slp = 0;

    function openModalWithdraw(id, max) {
        console.log(id);
        console.log(max);
        max_slp = max;
        $("#value_withdraw").val(0);
        $("#modalWithdraw").modal();

        $("#id_withdraw").val(id);
    }

    function max() {
        $("#value_withdraw").val(max_slp);
    }

    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ],
            initComplete: function() {
                this.api().columns().every(function(index) {
                    if (index == 4) {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            }
        });
        $('#myTable2').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>