@include('Admin.header',['activePage' => 'master_axie'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 33%;
    }

    .form-group label {
        margin: 0px;
    }

    .btn {
        border-radius: 25px;
        font-weight: bold;
    }

    #myChart {
        width: 1000px !important;
        height: 500px !important;
        margin: auto;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Informasi Axie</h3>
                <form action="{{URL('/admin/master_axie/do_edit')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="form-group">
                        <label>Owner</label><br>
                        <select class="form-control input-lg theSelect" name="id_owner">
                            @foreach($data_owner as $dt)
                            <option value="{{$dt->id}}" <?php echo $dt->id == $data->id_owner ? 'selected' : ''; ?>>{{$dt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Email Akun</label><br>
                        <input type="text" name="email_akun" value="{{$data->email_akun}}" class="form-control input-lg">
                    </div>
                    <div class="form-group">
                        <label>Negara</label><br>
                        <input type="text" name="negara" value="{{$data->negara}}" class="form-control input-lg">
                    </div>
                    <div class="form-group">
                        <label>Level</label><br>
                        <input type="number" name="level" value="{{$data->level}}" class="form-control input-lg">
                    </div>
                    <div class="form-group">
                        <label>Address Ronin</label><br>
                        <input type="text" name="address_ronin" value="{{$data->address_ronin}}" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Status Boss</label>
                        <select class="form-control input-lg" name="status_boss">
                            <option value="0" {{$data->status_boss == 0 ? 'selected' : ''}}>Not Clear</option>
                            <option value="1" {{$data->status_boss == 1 ? 'selected' : ''}}>Clear</option>
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary" value="EDIT">
                </form>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Pengguna Axie</h3>
                <form action="{{URL('/admin/master_axie/take_axie')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" value="{{$data->id}}">
                    <div class="form-group">
                        <label>Pegawai</label><br>
                        <input type="hidden" value="{{$data->user_id}}" name="id_user_aktif" />
                        <input type="hidden" value="{{$data->id}}" name="id_axie" />
                        <select style="width:100%;" name="id_user" class="form-control input-lg theSelect">
                            <option value="-1">Tidak Dipakai</option>
                            @foreach($dataPegawai as $dt)
                            @if($data->user_id == $dt->id)
                            <option value="{{$dt->id}}" selected>{{$dt->name}}</option>
                            @else
                            <option value="{{$dt->id}}">{{$dt->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <input type="submit" class="btn btn-primary">
                </form>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Grafik Gain SLP</h3>
            </div>
            <div class="panel-body">
                <div>
                    <div class="form-group">
                        <label>Periode</label>
                        <input id="filter_tanggal" type="month" class="form-control input-lg" onchange="filterChange()" />
                    </div>
                    <div class="form-group">
                        <label>Type</label>
                        <select id="type" class="form-control input-lg" onchange="filterChange()">
                            <option value="daily">Daily</option>
                            <option value="monthly">Monthly</option>
                            <option value="yearly">Yearly</option>
                        </select>
                    </div>
                </div>
                <canvas id="myChart"></canvas>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Inventory SLP</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Total SLP</label>
                    <p>{{$data->total_slp}}</p>
                    <div class="btn btn-primary" onclick="openModalTopup({{$data->id}})">Topup SLP</div>
                    <div class="btn btn-danger" onclick="openModalWithdraw({{$data->id}})">Withdraw SLP</div>
                </div>
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Player</th>
                            <th>Adventure</th>
                            <th>PVP</th>
                            <th>Quest</th>
                            <th>Total Gain SLP</th>
                            <th>Level Before</th>
                            <th>Last Inventory</th>
                            <th>Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($dataReport as $dt)
                        <tr>
                            <td>{{$dt->created_at}}</td>
                            <td>{{$dt->name}}</td>
                            @if(!isset($dt->slp_adventure))
                            <td style="border:none;visibility:hidden;">{{$dt->slp_quest > 0 ? 'TOPUP' : 'WITHDRAW'}}</td>
                            <td style="text-align:center;font-weight:bold;border:none;">{{$dt->slp_quest > 0 ? 'TOPUP' : 'WITHDRAW'}}</td>
                            <td style="border:none;visibility:hidden;">{{$dt->slp_quest > 0 ? 'TOPUP' : 'WITHDRAW'}}</td>
                            @else
                            <td>{{$dt->slp_adventure}}</td>
                            <td>{{$dt->slp_pvp}}</td>
                            <td>{{isset($dt->slp_adventure) ? $dt->slp_quest : ''}}</td>
                            @endif
                            <td>{{($dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest)}}</td>
                            <td>{{($dt->level_before)}}</td>
                            <td>{{$dt->last_inventory}}</td>
                            @if(isset($dt->slp_adventure))
                            <td>
                                <a href="/admin/daily_report/{{$dt->id}}">
                                    <div class="btn btn-primary"><i class="fa fa-eye"></i></div>
                                </a>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>


<div id="modalTopup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Topup SLP</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form id="form_topup" action="{{ URL('/admin/do_topup_slp') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_topup" name="id_axie" />
                        <input type="number" step="any" id="value_topup" name="amount" /> SLP<br><br>
                        <button id="btn_topup" class="btn btn-primary" onclick="clickButton('topup')">TOPUP</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="modalWithdraw" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Withdraw SLP</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
                    <form id="form_withdraw" action="{{ URL('/admin/do_withdraw_slp') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id_withdraw" name="id_axie" />
                        <input type="number" step="any" id="value_withdraw" name="amount" /> SLP<br><br>
                        <div class="btn btn-info" onclick="max()">MAX</div>
                        <button id="btn_withdraw" class="btn btn-primary" onclick="clickButton('withdraw')">Withdraw</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('Admin.footer')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');

    function filterChange() {
        loaddata();
    }

    function getMax(array) {
        var max = 0;
        array.forEach((data) => {
            // console.log(data);
            if (max < data) {
                max = data;
            }
        });
        return max;
    }

    function loaddata() {
        var filter_tanggal = $("#filter_tanggal").val();
        // console.log(filter_tanggal);
        var temp = filter_tanggal.split("-");
        var month = temp[1];
        var year = temp[0];
        var type = $("#type").val();
        var id_axie = <?php echo $data->id; ?>;
        $.ajax({
            type: "GET",
            url: "/api/laporan/laporan_gain_slp?month=" + month + "&year=" + year + "&type=" + type + "&id_axie=" + id_axie,
            // url: "https://api.allorigins.win/raw?url=https://api.lunaciaproxy.cloud/_battles/" + address + "/50/0",
            success: function(data) {
                var label_line = [];
                var data_line = [];
                data.payload.forEach((dt) => {
                    if (type == "monthly") {
                        dt.date = "Week " + dt.date;
                    }
                    label_line.push(dt.date);
                    data_line.push(dt.total_gain);
                });

                var max_array = getMax(data_line);
                var step_size = 50;
                console.log("max_array : " + max_array);
                if (max_array > 1000) {
                    step_size = 500;
                } else if (max_array > 10000) {
                    step_size = 1000;
                } else if (max_array > 100000) {
                    step_size = 10000;
                }
                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: label_line,
                        datasets: [{
                            label: 'Gain SLP',
                            data: data_line,
                            fill: false,
                            borderColor: 'rgb(75, 192, 192)'
                        }]
                    },
                    options: {
                        elements: {
                            line: {
                                tension: 0
                            }
                        },
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    display: true
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: step_size,
                                }
                            }]
                        },
                    }
                });
            },
            error: function(err) {
                //COBA API KE 2
                console.log(err);
            }
        });
    }

    $(function() {
        $("#filter_tanggal").val("2021-10");
        loaddata();
        $(".theSelect").select2();
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });
    //FUNCTION SHOW MODAL
    function openModalTopup(id) {
        console.log(id);
        $("#modalTopup").modal();

        $("#id_topup").val(id);
    }

    function openModalWithdraw(id) {
        console.log(id);
        $("#modalWithdraw").modal();

        $("#id_withdraw").val(id);
    }

    function max() {
        $("#value_withdraw").val(<?php echo $data->total_slp; ?>);
    }

    function clickButton(type) {
        if (type == 'withdraw') {
            $("#btn_withdraw").prop("disabled", "disabled");
            $("#btn_withdraw").html("Please wait...");
            $("#form_withdraw").submit();
        } else {
            $("#btn_topup").prop("disabled", "disabled");
            $("#btn_topup").html("Please wait...");
            $("#form_topup").submit();
        }
    }
</script>