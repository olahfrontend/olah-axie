@include('Admin.header',['activePage' => 'master_axie'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 50%;
    }

    .form-group label {
        margin: 0px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Informasi Axie</h3>
                <form action="{{URL('/admin/master_axie/do_add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Owner</label><br>
                        <select class="form-control input-lg" name="id_owner" {{$dataUser->role != 2 && $dataUser->role != 6 ? 'disabled' : ''}}>
                            @if($dataUser->role == 2 || $dataUser->role == 6)
                            @foreach($data_owner as $dt)
                            <option value='{{$dt->id}}'>{{$dt->name}}</option>
                            @endforeach
                            @else
                            <option value='{{$dataUser->id}}'>{{$dataUser->name}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Email Akun</label><br>
                        <input type="email" name="email_akun" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Negara</label><br>
                        <input type="text" name="negara" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Level</label><br>
                        <input type="number" name="level" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Address Ronin</label><br>
                        <input type="text" name="address_ronin" class="form-control input-lg" />
                    </div>
                    <input type="submit" class="btn btn-primary" value="Add">
                </form>
            </div>
        </div>

    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')