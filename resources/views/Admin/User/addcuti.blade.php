@include('Admin.header',['activePage' => 'list_cuti'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Tambah Jadwal Cuti / Absen</h3>
                <form action="{{URL('/admin/master_user/do_add_cuti')}}" method="POST">
                    @csrf
                    <div class="form-container">
                        <div class="form-group">
                            <label>Pengaju Cuti</label><br>
                            <select id="id_user" class="form-control input-lg theSelect" name="id_user" onchange="selectChange()">
                                @foreach($data_user as $dt)
                                <option value="{{$dt->id}}">{{$dt->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <input type="text" id="from" name="tanggal_cuti_mulai" class="form-control input-lg" readonly>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="text" id="to" name="tanggal_cuti_selesai" class="form-control input-lg" readonly>
                        </div>
                        <div class="form-group">
                            <label>Alasan</label>
                            <input type="text" name="alasan" class="form-control input-lg">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status_absen" id="flexRadioDefault1" checked value="0">
                                <label class="form-check-label" for="flexRadioDefault1">
                                    CUTI
                                </label>
                                <input class="form-check-input" style="margin-left:15px;" type="radio" name="status_absen" id="flexRadioDefault2" value="1">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    ABSEN
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Tambah Jadwal">
                </form>
            </div>
        </div>

    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<script>
    $(function() {

        $(".theSelect").select2();
        var dateFormat = "mm/dd/yy",
            from = $("#from")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1
            })
            .on("change", function() {
                var tgl = getDate(this);
                tgl.setDate(tgl.getDate() + 4);
                to.datepicker("option", "minDate", getDate(this));
                to.datepicker("option", "maxDate", tgl);
            }),

            to = $("#to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1
            })
            .on("change", function() {
                from.datepicker("option", "maxDate", getDate(this));
            });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }
            
            return date;
        }

        $("#from").change();
        $("#to").change();
    });

    function selectChange() {
        console.log($("#id_user").val());
    }
</script>