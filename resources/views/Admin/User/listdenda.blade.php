@include('Admin.header',['activePage' => 'list_denda'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Denda</h3>
                <div>
                    <a href="/admin/master_user/add_denda">
                        <div class="btn btn-primary" style="margin-top:25px;">Tambah Denda</div>
                    </a>
                </div>
            </div>


            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Pegawai</th>
                            <th>Deskripsi</th>
                            <th>Nominal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_denda as $dt)
                        <tr>
                            <td>{{ $dt->created_at }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->description }}</td>
                            <td>{{ number_format($dt->total_denda,0,",",".") }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <!-- END BORDERED TABLE -->
    </div>


    <!-- END MAIN CONTENT -->
</div>

<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>
<!-- END MAIN -->
@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>