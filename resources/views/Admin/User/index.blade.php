@include('Admin.header',['activePage' => 'master_user'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Pegawai</h3>
                <a href="/admin/master_user/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Pegawai</div>
                </a>
            </div>

            <div class="panel-body">
                <h5 style="font-weight:bold;color:#74B7BD;">Status Pegawai</h5>
                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                    <li class="nav-item active" role="presentation">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Aktif</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Tidak Aktif</a>
                    </li>
                </ul>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active in" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <table id="myTable" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Alamat Kantor</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($data as $dt)
                                @if($dt->status == 1)
                                <tr>
                                    <td>{{ $dt->name }}</td>
                                    <td>{{ $dt->email }}</td>
                                    <td>{{ $dt->alamat_kantor }}</td>
                                    <td>{{$dt->nama_role}}</td>
                                    <td>
                                        <a href="{{ URL('admin/master_user/') . '/' . $dt->id }}">
                                            <div class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </a>
                                        @if($dt->role == 1 || $dt->role == 5)
                                        <a href="{{ URL('admin/master_user/laporan/') . '/' . $dt->id }}">
                                            <div class="btn btn-primary" style="min-width:50px;">
                                                <i class="fa fa-book"></i>
                                            </div>
                                        </a>
                                        <!-- <a href="{{ URL('admin/master_user/laporan_daily/') . '/' . $dt->id }}">
                                            <div class="btn btn-primary" style="min-width:50px;">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </a> -->
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <table id="myTable3" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Alamat Kantor</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach ($data as $dt)
                                @if($dt->status == 0)
                                <tr>
                                    <td>{{ $dt->name }}</td>
                                    <td>{{ $dt->email }}</td>
                                    <td>{{ $dt->alamat_kantor }}</td>
                                    <td>{{$dt->nama_role}}</td>
                                    <td>
                                        <a href="{{ URL('admin/master_user/') . '/' . $dt->id }}">
                                            <div class="btn btn-primary">
                                                <i class="fa fa-edit"></i>
                                            </div>
                                        </a>
                                        @if($dt->role == 1 || $dt->role == 5)
                                        <a href="{{ URL('admin/master_user/laporan/') . '/' . $dt->id }}">
                                            <div class="btn btn-primary" style="min-width:50px">
                                                <i class="fa fa-book"></i>
                                            </div>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {

        $('#myTable').DataTable({
            "order": [
                [0, "asc"]
            ],
            initComplete: function() {
                this.api().columns().every(function(index) {
                    if (index == 2 || index == 3) {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            }
        });

        $('#myTable3').DataTable({
            "order": [
                [0, "asc"]
            ],
            initComplete: function() {
                this.api().columns().every(function(index) {
                    if (index == 2 || index == 3) {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function() {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function(d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    }
                });
            }
        });

        // $('#myTable').DataTable({});
        $('#myTable2').DataTable({
            "order": [
                [2, "desc"]
            ]
        });

    });
</script>