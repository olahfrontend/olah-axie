@include('Admin.header',['activePage' => 'master_user'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .td-center {
        text-align: center;
        font-weight: bold;
        vertical-align: middle;
    }

    .label {
        color: black;
        font-size: 16px;
        text-align: left;
    }

    .label div:first-child {
        font-weight: bold;
        color: #74b7bd;
    }

    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 150px;
        padding-left: 5%;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9);
        /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 500px;
    }

    /* Caption of Modal Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation */
    .modal-content,
    #caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
        from {
            -webkit-transform: scale(0)
        }

        to {
            -webkit-transform: scale(1)
        }
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }

        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 150px;
        right: 10%;
        color: white;
        z-index: 2;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }
</style>
<div class="main" id="pdfcontent">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Laporan Gaji Pegawai</h3>
                <div class="form-group">
                    <label>Nama Pegawai</label><br>
                    <input type="text" value="{{$data_user->name}}" class="form-control input-lg" readonly />
                </div>
                <div class="form-group">
                    <label>Periode</label><br>
                    <input id="periode" type="month" class="form-control input-lg" onchange="dateChange()" />
                </div>
                <!-- <div class="btn btn-primary" onclick="downloadPDF()">
                    Download PDF
                </div> -->
                <div class="btn btn-primary" onclick="printPDF()">
                    Print / Save As PDF
                </div>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Laporan PVP</h3>
            </div>
            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Account Name</th>
                            <th>Total Day Play</th>
                            <th>Total Gain SLP</th>
                            <th>Average</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Laporan Gaji</h3>
            </div>
            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th colspan="2">Account Name</th>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                            <th>Sunday</th>
                            <th>Weekly Bonus</th>
                            <th>Total</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="footer-container">
                    <div class="label">
                        <div>Total Gaji Daily</div>
                        <div id="total_gaji_daily">0</div>
                    </div>
                    <div class="label">
                        <div>Total Bonus Weekly</div>
                        <div id="total_bonus_weekly">0</div>
                    </div>
                    <div class="label">
                        <div>Total Bonus Monthly</div>
                        <div id="total_bonus_monthly">0</div>
                    </div>
                    <div class="label">
                        <div>Total Bonus PVP</div>
                        <div id="total_bonus_pvp">0</div>
                    </div>
                    <div class="label">
                        <div>Total Reimburs</div>
                        <div id="total_reimburs">0</div>
                    </div>
                    <div class="label">
                        <div>Total Denda</div>
                        <div id="total_denda" style="color:red;">0</div>
                    </div>
                    <div class="label">
                        <div>Total Gaji</div>
                        <div id="total_gaji">0</div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Laporan Denda</h3>
            </div>
            <div class="panel-body">
                <table id="myTable3" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Description</th>
                            <th>Total Denda</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel" id="ignorePDF">
            <div class="panel-heading">
                <h3 class="panel-title">Laporan Reimburs</h3>
            </div>
            <div class="panel-body">
                <table id="myTable4" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Description</th>
                            <th>Bukti</th>
                            <th>Total Reimburs</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Log Gaji</h3>
            </div>
            <div class="panel-body">
                <table id="myTable5" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Tanggal</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>


    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.4.0/jspdf.umd.min.js" integrity="sha512-V/C9Axb8EEL4ix79ERIJmpRd6Mp1rWVSxa2PIBCdCxqhEsfCBWp/R0xJ4U495czhcuDWrGOFYo8+QI3lJ9DK5g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script> -->
<script src="{{URL::asset('js/jspdf.js')}}"></script>

<script>
    var file_name;

    function printPDF() {
        if (!$('body').hasClass('layout-fullwidth')) {
            $('body').addClass('layout-fullwidth');
        }
        window.print()
    }

    function downloadPDF() {
        var periode = $("#periode").val();
        var temp = periode.split("-");
        var year = temp[0];
        var month = temp[1];
        file_name = 'LAPORAN_GAJI_<?php echo strtoupper($data_user->name); ?>_' + month + '_' + year;
        var doc = new jsPDF('l', 'mm', [297, 210]);
        // var elementHandler = {
        //     '#ignorePDF': function(element, renderer) {
        //         return true;
        //     }
        // };
        var source = window.document.getElementById("pdfcontent");
        doc.fromHTML(
            source,
            15,
            15, {
                'width': 800,
                // 'elementHandlers': elementHandler
            },
            function(bla) {
                doc.save(file_name);
            }, );

        // doc.output("dataurlnewwindow");

        // const doc = new jsPDF({
        //     orientation: "landscape",
        //     unit: "in",
        //     format: [4, 2]
        // });
        // doc.text("Hello world!", 1, 1);
        // doc.save("two-by-four.pdf");
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
    $(document).ready(function() {
        var d = new Date();
        var month = d.getMonth() + 1;
        if (month <= 9) {
            month = "0" + month;
        }
        var year = d.getFullYear();
        var value = year + "-" + month;
        $("#periode").val(value);
        dateChange();
    });

    function dateChange() {
        loadLaporan();
        loadLog();
    }

    function loadLog() {
        var periode = $("#periode").val();
        var temp = periode.split("-");
        var year = temp[0];
        var month = temp[1];
        $.ajax({
            type: "GET",
            url: "/api/report/get_log_gaji?id_user=<?php echo $data_user->id; ?>&month=" + month + "&year=" + year,
            cache: false,
            success: function(data) {
                $("#myTable5 tbody").empty();
                row = "";
                data.payload.forEach(function(item, i) {
                    row += "<tr>" +
                        "<td>" + item.created_at + "</td>" +
                        "<td>" + item.description + "</td>" +
                        "</tr>";
                });
                $('#myTable5 tbody').html(row);
            }
        });
    }

    function loadLaporan() {
        var periode = $("#periode").val();
        var temp = periode.split("-");
        var year = temp[0];
        var month = temp[1];
        $.ajax({
            type: "GET",
            url: "/api/report/get_laporan_gaji_weekly?id_user=<?php echo $data_user->id; ?>&month=" + month + "&year=" + year,
            cache: false,
            success: function(data) {
                console.log(data);
                $("#myTable tbody").empty();
                var row = "";
                var total_bonus_week = 0;
                var total_bonus_month = data.payload.total_bonus_bulanan;
                var total_gaji = 0;
                var total_reimburs = data.payload.total_reimburs;
                var total_denda = data.payload.total_denda;
                data.payload.data_weekly.forEach(function(item, i) {
                    var total_week = 0;
                    var total_sunday = 0;
                    var total_bonus_weekly = 0;
                    item.data_axie.forEach(function(itm, j) {
                        var account_name = itm.detail_axie.email_akun;
                        var monday = "-";
                        var tuesday = "-";
                        var wednesday = "-";
                        var thursday = "-";
                        var friday = "-";
                        var saturday = "-";
                        var total = 0;
                        var total_bonus = "";
                        var sunday = "-";
                        var week = null;
                        if (j == 0) {
                            week = "WEEK " + item.week;
                        }
                        itm.data_gaji.forEach(function(dt, k) {
                            if (dt.day == "Monday") {
                                monday = dt.gaji;
                            } else if (dt.day == "Tuesday") {
                                tuesday = dt.gaji;
                            } else if (dt.day == "Wednesday") {
                                wednesday = dt.gaji;
                            } else if (dt.day == "Thursday") {
                                thursday = dt.gaji;
                            } else if (dt.day == "Friday") {
                                friday = dt.gaji;
                            } else if (dt.day == "Saturday") {
                                saturday = dt.gaji;
                            } else if (dt.day == "Sunday") {
                                sunday = dt.gaji;
                                total_sunday += sunday;
                            }
                            total += dt.gaji;
                        });
                        total_week += total;
                        total_gaji += total;

                        //ADD TABLE ROW NEW
                        if (j == 0) {
                            row += "<tr>" +
                                "<td>" + account_name + "</td>" +
                                "<td rowspan='" + item.data_axie.length + "' class='td-center' style='vertical-align:middle;'>" + week + "</td>" +
                                "<td>" + numberWithCommas(monday) + "</td>" +
                                "<td>" + numberWithCommas(tuesday) + "</td>" +
                                "<td>" + numberWithCommas(wednesday) + "</td>" +
                                "<td>" + numberWithCommas(thursday) + "</td>" +
                                "<td>" + numberWithCommas(friday) + "</td>" +
                                "<td>" + numberWithCommas(saturday) + "</td>" +
                                "<td>" + numberWithCommas(sunday) + "</td>" +
                                "<td rowspan='" + item.data_axie.length + "'>" + numberWithCommas(total_bonus) + "</td>" +
                                "<td>" + numberWithCommas(total) + "</td>" +
                                "</tr>";
                        } else {
                            row += "<tr>" +
                                "<td>" + account_name + "</td>" +
                                "<td>" + numberWithCommas(monday) + "</td>" +
                                "<td>" + numberWithCommas(tuesday) + "</td>" +
                                "<td>" + numberWithCommas(wednesday) + "</td>" +
                                "<td>" + numberWithCommas(thursday) + "</td>" +
                                "<td>" + numberWithCommas(friday) + "</td>" +
                                "<td>" + numberWithCommas(saturday) + "</td>" +
                                "<td>" + numberWithCommas(sunday) + "</td>" +
                                // "<td>" + total_bonus + "</td>" +
                                "<td>" + numberWithCommas(total) + "</td>" +
                                "</tr>";
                        }
                    });
                    data.payload.data_bonus_mingguan.forEach(function(dtbonus) {
                        if (dtbonus.week == item.week) {
                            total_bonus_weekly = dtbonus.gaji;
                            total_week += total_bonus_weekly;
                            total_bonus_week += total_bonus_weekly;
                        }
                    });
                    row += "<tr>" +
                        "<td colspan='9' class='td-center'>TOTAL WEEK " + item.week + "</td>" +
                        "<td class='td-center'>" + numberWithCommas(total_bonus_weekly) + "</td>" +
                        "<td>" + numberWithCommas(total_week) + "</td>" +
                        "</tr>";
                });
                $('#myTable tbody').html(row);

                //ISI TABLE PVP
                $("#myTable2 tbody").empty();
                var row = "";
                var total_bonus_pvp = data.payload.est_total_bonus_pvp;
                data.payload.data_pvp.forEach(function(item, i) {
                    var account_name = item.data_axie.email_akun;
                    var total_slp = item.total_slp_pvp;
                    var total_play = item.total_play;
                    var average = item.avg_slp;
                    var color = "green";
                    if (total_play < 7) {
                        color = "red";
                    }
                    row += "<tr>" +
                        "<td>" + account_name + "</td>" +
                        "<td>" + total_play + "</td>" +
                        "<td>" + numberWithCommas(total_slp) + "</td>" +
                        "<td style='background-color:" + color + ";color:white;font-weight:bold;'>" + numberWithCommas(average) + "</td>" +
                        "</tr>";
                });
                row += "<tr>" +
                    "<td colspan='3' class='td-center'>TOTAL AVERAGE PVP</td>" +
                    "<td>" + numberWithCommas(data.payload.total_average_pvp) + "</td>" +
                    "</tr>";
                $('#myTable2 tbody').html(row);

                var total_gaji_daily = data.payload.total_gaji - total_bonus_month - total_bonus_week;
                $("#total_gaji_daily").html("Rp. " + numberWithCommas(total_gaji_daily));
                $("#total_bonus_pvp").html("Rp. " + numberWithCommas(total_bonus_pvp));
                $("#total_bonus_weekly").html("Rp. " + numberWithCommas(total_bonus_week));
                $("#total_bonus_monthly").html("Rp. " + numberWithCommas(total_bonus_month));
                $("#total_reimburs").html("Rp. " + numberWithCommas(total_reimburs));
                $("#total_denda").html("Rp. " + numberWithCommas(total_denda));
                total_gaji = data.payload.total_gaji + total_bonus_pvp;
                $("#total_gaji").html("Rp. " + numberWithCommas(total_gaji));

                //ISI TABLE DENDA
                $("#myTable3 tbody").empty();
                var row = "";
                var total_denda = 0;
                data.payload.data_denda.forEach(function(item, i) {
                    var tanggal = item.created_at;
                    var description = item.description;
                    var denda = parseInt(item.total_denda);
                    row += "<tr>" +
                        "<td>" + tanggal + "</td>" +
                        "<td>" + description + "</td>" +
                        "<td>" + numberWithCommas(denda) + "</td>" +
                        "</tr>";
                    total_denda += denda;
                });
                row += "<tr>" +
                    "<td colspan='2' class='td-center'>TOTAL DENDA</td>" +
                    "<td>" + numberWithCommas(total_denda) + "</td>" +
                    "</tr>";
                $('#myTable3 tbody').html(row);

                //ISI TABLE REIMBURS
                $("#myTable4 tbody").empty();
                var row = "";
                var total_reimburs = 0;
                data.payload.data_reimburs.forEach(function(item, i) {
                    var tanggal = item.created_at;
                    var description = item.keterangan;
                    var reimburs = parseInt(item.nominal);
                    var url_bukti = item.url_bukti;
                    row += "<tr>" +
                        "<td>" + tanggal + "</td>" +
                        "<td>" + description + "</td>" +
                        "<td><img src='https://olahproxy.herokuapp.com/" + url_bukti + "' onclick=\"imageclick(\'" + url_bukti + "\')\" style='cursor:pointer;width:250px; height:250px;object-fit:contain;'/></td>" +
                        "<td>" + numberWithCommas(reimburs) + "</td>" +
                        "</tr>";
                    total_reimburs += reimburs;
                });
                row += "<tr>" +
                    "<td colspan='3' class='td-center'>TOTAL REIMBURS</td>" +
                    "<td>" + numberWithCommas(total_reimburs) + "</td>" +
                    "</tr>";
                $('#myTable4 tbody').html(row);
            }
        });
    }

    function imageclick(img_src) {
        console.log("Asd");
        modal.style.display = "block";
        modalImg.src = img_src;
        captionText.innerHTML = img_src;
    }

    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById("myImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>