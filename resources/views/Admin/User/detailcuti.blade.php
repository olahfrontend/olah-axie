@include('Admin.header',['activePage' => 'list_cuti'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Edit Jadwal Cuti</h3>
                <form action="{{URL('/admin/master_user/do_edit_cuti')}}" method="POST">
                    @csrf
                    <input type="hidden" value="{{$data_user->id}}" name="id" />
                    <div class="form-container">
                        <div class="form-group">
                            <label>Pengaju Cuti</label><br>
                            <input type="text" value="{{$data_user->name}}" class="form-control input-lg" readonly />
                        </div>
                        <div class="form-group">
                            <label>Tanggal Mulai</label>
                            <input value="{{$data_user->tanggal_cuti_mulai}}" type="text" id="from" name="tanggal_cuti_mulai" class="form-control input-lg" readonly {{$data_user->status != 0 ? 'disabled' : ''}}>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Masuk</label>
                            <input type="text" value="{{$data_user->tanggal_cuti_selesai}}" id="to" name="tanggal_cuti_selesai" class="form-control input-lg" readonly {{$data_user->status != 0 ? 'disabled' : ''}}>
                        </div>
                        <div class="form-group">
                            <label>Alasan</label>
                            <input type="text" name="alasan" class="form-control input-lg" value="{{$data_user->alasan}}" {{$data_user->status != 0 ? 'disabled' : ''}}>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status_absen" id="flexRadioDefault1" value="0" {{$data_user->status_absen == 0 ? 'checked' : ''}}>
                                <label class="form-check-label" for="flexRadioDefault1">
                                    CUTI
                                </label>
                                <input class="form-check-input" style="margin-left:15px;" type="radio" name="status_absen" id="flexRadioDefault2" value="1" {{$data_user->status_absen == 1 ? 'checked' : ''}}>
                                <label class="form-check-label" for="flexRadioDefault2">
                                    ABSEN
                                </label>
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Edit Jadwal" {{$data_user->status != 0 ? 'disabled' : ''}}>
                    <div id="btn-cancel" class="btn btn-danger {{$data_user->status != 0 ? 'disabled' : ''}}" onclick="cancelCuti()">Batalkan Jadwal</div>
                </form>
                <form id="formdelete" action="{{URL('/admin/master_user/do_batal_cuti')}}" method="POST" style="margin-top:25px;">
                    @csrf
                    <input type="hidden" value="{{$data_user->id}}" name="id" />
                </form>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Axie</h3>
            </div>

            <div class="panel-body">
                <div style="margin-bottom:25px;">
                    <div class="form-group">
                        <label>Axie</label>
                        <select id="add_detail_axie" class="form-control input-lg theSelect">
                            @foreach($data_axie_user as $dt)
                            <option value="{{$dt->id_axie}}">{{$dt->email_akun}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pengganti</label>
                        <select id="add_detail_pengganti" class="form-control input-lg theSelect">
                            @foreach($data_user_pengganti as $dt)
                            <option value="{{$dt->id}}">{{$dt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="btn btn-primary" onclick="doAddDetail()" id="btnSimpan" {{$data_user->status != 0 ? 'disabled' : ''}}>Simpan</div>
                    <div class="btn btn-primary" disabled style="display:none;" id="btnSimpanDisabled" {{$data_user->status != 0 ? 'disabled' : ''}}>Simpan</div>
                </div>
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama Pengganti</th>
                            <th>Axie</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>



    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<script>
    function cancelCuti() {

        if (!$("#btn-cancel").hasClass("disabled")) {
            swal({
                    title: "Apakah anda yakin?",
                    text: "Jadwal yang telah dibatalkan tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $("#formdelete").submit();
                    }
                });
        }
    }

    function doAddDetail() {
        var axie = $("#add_detail_axie").val();
        var pengganti = $("#add_detail_pengganti").val();
        // console.log(axie);
        // console.log(pengganti);
        var formData = new FormData();
        formData.append("_token", '<?php echo csrf_token(); ?>');
        formData.append("id_jadwal_cuti", <?php echo $data_user->id; ?>);
        formData.append("id_pengganti", pengganti);
        formData.append("id_axie", axie);

            $.ajax({
                type: "POST",
                data: formData,
                processData: false,
                contentType: false,
                url: "/api/do_add_detail_cuti",
                cache: false,
                success: function(data) {
                    loadDetail();
                    swal("Sukses", "Data berhasil ditambah", "success");
                },
                error: function(data) {
                    console.log(data.responseJSON.code);
                    if (data.responseJSON.code == 400) {
                        swal("Failed", data.responseJSON.error_msg, "error");
                    } else {
                        swal("Failed", "Oops Something Wrong", "error");
                    }
                }
            });

    }
    var data_detail_cuti;

    function loadDetail() {
        // var id_user = $("#id_user").val();
        // console.log("id " + id_user);

        $.ajax({
            type: "GET",
            url: "/api/get_detail_cuti?id=<?php echo $data_user->id; ?>",
            cache: false,
            success: function(data) {
                console.log(data);
                data = JSON.parse(data);
                // console.log(data.jadwal_cuti);
                data_detail_cuti = data.jadwal_cuti;
                var t = $("#myTable").DataTable();
                t.clear();
                data.jadwal_cuti.forEach(function(item, index) {
                    t.row.add([
                        item.name,
                        item.email_akun,
                        "<td><div style='border-radius:15px;' class='btn btn-danger' onclick='doDelete(" + index + ")' <?php echo $data_user->status != 0 ? 'disabled' : ''; ?>><i class='fa fa-trash'></i></div><td>"
                    ]).draw(false);
                });

                $("#add_detail_axie").find("option").remove();

                if (data.axie_user.length > 0) {
                    $("#btnSimpan").css("display", "block");
                    $("#btnSimpanDisabled").css("display", "none");
                    data.axie_user.forEach(function(item, index) {
                        var opt = new Option(item.email_akun, item.id_axie);
                        $("#add_detail_axie").append(opt);
                    });
                } else {
                    $("#add_detail_axie").append("<option disabled selected>No Axie Available</option>");
                    $("#btnSimpan").css("display", "none");
                    $("#btnSimpanDisabled").css("display", "block");

                }

            }
        });
    }

    function doDelete(index) {

        if (<?php echo $data_user->status == 0 ? 'true' : 'false'; ?>) {

            var myTable = $('#myTable').DataTable();

            swal({
                    title: "Apakah anda yakin?",
                    text: "Data yang telah dihapus tidak dapat dikembalikan",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        console.log(data_detail_cuti[index].id);
                        var formData = new FormData();
                        formData.append("_token", '<?php echo csrf_token(); ?>');
                        formData.append("id", data_detail_cuti[index].id);
                        $.ajax({
                            type: "POST",
                            data: formData,
                            processData: false,
                            contentType: false,
                            url: "/api/delete_jadwal_cuti",
                            cache: false,
                            success: function(data) {
                                loadDetail();
                                swal("Sukses", "Data berhasil dihapus", "success");
                            },
                            fail: function(data) {
                                swal("Failed", "Oops Something Wrong", "error");
                            }
                        });
                    }
                });
        }
    }

    $(function() {
        $(".theSelect").select2();
        loadDetail();
        var dateFormat = "mm/dd/yy",
            from = $("#from")
            .datepicker({
                changeMonth: true,
                numberOfMonths: 1
            })
            .on("change", function() {
                var tgl = getDate(this);
                tgl.setDate(tgl.getDate() + 4);
                to.datepicker("option", "minDate", getDate(this));
                to.datepicker("option", "maxDate", tgl);
            }),

            to = $("#to").datepicker({
                changeMonth: true,
                numberOfMonths: 1
            })
            .on("change", function() {
                from.datepicker("option", "maxDate", getDate(this));
            });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }

        $("#from").change();
        $("#to").change();
    });
</script>