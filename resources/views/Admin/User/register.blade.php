@include('Admin.header',['activePage' => 'master_user'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 50%;
    }

    .form-group label {
        margin: 0px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Detail Information</h3>
                <form action="{{URL('/admin/master_user/do_add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Name</label><br>
                        <input type="text" name="name" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Email</label><br>
                        <input type="email" name="email" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Role</label><br>
                        <select id="role" name="role" class="form-control input-lg" onchange="roleChange()">
                            @if($dataUser->role == 2)
                            @foreach($data_role as $dt)
                            <option value="{{$dt->id}}">{{$dt->nama_role}}</option>
                            @endforeach
                            @elseif($dataUser->role == 3)
                            <option value="6">ADMIN</option>
                            <option value="4">PENGAWAS</option>
                            <option value="7">COACH</option>
                            <option value="1">PEGAWAI</option>
                            <option value="5">TRAINEE</option>
                            @elseif($dataUser->role == 6)
                            <option value="4">PENGAWAS</option>
                            <option value="7">COACH</option>
                            <option value="1">PEGAWAI</option>
                            <option value="5">TRAINEE</option>
                            @else
                            <option value="6">ADMIN</option>
                            <option value="4">PENGAWAS</option>
                            <option value="7">COACH</option>
                            <option value="1">PEGAWAI</option>
                            <option value="5">TRAINEE</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Kantor</label>
                        <select id="id_kantor" name="id_kantor" class="form-control input-lg">
                            <option value="">-</option>
                            @foreach($data_kantor as $dt)
                            <option value="{{$dt->id}}">{{$dt->alamat}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Password</label><br>
                        <input id="password" type="password" name="password" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label><br>
                        <input id="cpassword" type="password" name="cpassword" class="form-control input-lg" onkeyup="passwordChange()" />
                    </div>
                    <p id="warning" style="color:red;font-size:11px;">*confirm password tidak sama dengan password</p>
                    <input id="btn-submit" type="submit" class="btn btn-primary" disabled value="Add">
                </form>
            </div>
        </div>

    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')

<script>
    var data_role = JSON.parse('<?php echo json_encode($data_role); ?>');
    $(document).ready(function() {
        roleChange();
        console.log(data_role);
    });

    function roleChange() {
        var role = document.getElementById("role");

        console.log(role.value);
        var jabatan = 0;
        data_role.forEach((data) => {
            if (role.value == data.id) {
                jabatan = data.jabatan;
            }
        })
        if (jabatan == 2) {
            $("#id_kantor").attr("disabled", "disabled");
            $("#id_kantor").val("").change();
            $("#id_kantor :nth-child(1)").removeAttr("disabled");
        } else {
            $("#id_kantor").removeAttr("disabled");
            $("#id_kantor :nth-child(2)").prop("selected", true);
            $("#id_kantor :nth-child(1)").attr("disabled", "disabled");
        }
    }

    function passwordChange() {
        var password = $("#password").val();
        var cpassword = $("#cpassword").val();

        if (password == cpassword) {
            $("#warning").css("display", "none");
            $("#btn-submit").removeAttr("disabled");
        } else {
            $("#warning").css("display", "block");
            $("#btn-submit").attr("disabled", "disable");
        }
    }
</script>