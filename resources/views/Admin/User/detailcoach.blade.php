@include('Admin.header',['activePage' => 'list_cuti'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Edit List Training Coach</h3>
                <input type="hidden" value="{{$data_user->id}}" id="id_coach" />
                <div class="form-container">
                    <div class="form-group">
                        <label>Coach Name</label><br>
                        <input type="text" value="{{$data_user->name}}" class="form-control input-lg" readonly />
                    </div>
                    <div class="form-group">
                        <label>Apprentice</label>
                        <select class="form-control input-lg theSelect" id="id_user">
                            @foreach($dataPegawai as $dt)
                            <option value="{{$dt->id}}">{{$dt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="btn btn-primary" onclick="doAddDetail()" id="btnSimpan">Tambah</div>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama Apperentice</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>


    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')
<script>
    function doAddDetail() {
        var id_coach = $("#id_coach").val();
        var id_user = $("#id_user").val();
        // console.log(axie);
        // console.log(pengganti);
        var formData = new FormData();
        formData.append("_token", '<?php echo csrf_token(); ?>');
        formData.append("id_coach", id_coach);
        formData.append("id_user", id_user);

        $.ajax({
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            url: "/api/coach/add",
            cache: false,
            success: function(data) {
                loadDetail();
                swal("Sukses", "Data berhasil ditambah", "success");
            },
            fail: function(data) {
                console.log(data);
                swal("Failed", "Oops Something Wrong", "error");
            }
        });

    }
    var data_coach;

    function loadDetail() {
        // var t = $("#myTable").DataTable();
        var id_coach = $("#id_coach").val();
        console.log("id " + id_coach);

        $.ajax({
            type: "GET",
            url: "/api/coach/get?id_coach=" + id_coach,
            cache: false,
            success: function(data) {
                console.log(data);
                // data = JSON.parse(data);
                var t = $("#myTable").DataTable();
                t.clear();
                data_coach = data.payload;
                data.payload.forEach(function(item, index) {
                    console.log(item.name);
                    t.row.add([
                        item.name,
                        "<td><div style='border-radius:15px;' class='btn btn-danger' onclick='doDelete(" + index + ")'><i class='fa fa-trash'></i></div><td>"
                    ]).draw(false);
                });

            }
        });
    }

    function doDelete(index) {

        var myTable = $('#myTable').DataTable();

        swal({
                title: "Are you sure?",
                text: "Data yang telah dihapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    console.log(data_coach[index].id);
                    var formData = new FormData();
                    formData.append("_token", '<?php echo csrf_token(); ?>');
                    formData.append("id", data_coach[index].id);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        url: "/api/coach/delete",
                        cache: false,
                        success: function(data) {
                            loadDetail();
                            swal("Sukses", "Data berhasil dihapus", "success");
                        },
                        fail: function(data) {
                            swal("Failed", "Oops Something Wrong", "error");
                        }
                    });
                }
            });

    }

    $(function() {
        $(".theSelect").select2();
        console.log("asd");
        loadDetail();
    });
</script>