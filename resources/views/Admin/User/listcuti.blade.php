@include('Admin.header',['activePage' => 'list_cuti'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Jadwal Cuti / Absen</h3>
                <div style="display:flex">
                    <a href="/admin/master_user/add_cuti" style="flex-basis:50%;">
                        <div class="btn btn-primary" style="margin-top:25px;">Tambah Jadwal Cuti / Absen</div>
                    </a>
                    <a href="/admin/master_user/start_command_cuti" style="flex-basis:50%;text-align:right;">
                        <div class="btn btn-primary" style="margin-top:25px;background-color:green;">Start Command Cuti / Absen</div>
                    </a>
                </div>
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Masuk</th>
                            <th>Alasan</th>
                            <th>Status</th>
                            <th>Cuti/Absen</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_cuti as $dt)
                        <tr>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->tanggal_cuti_mulai }}</td>
                            <td>{{ $dt->tanggal_cuti_selesai }}</td>
                            <td>{{ $dt->alasan }}</td>
                            <td>{{$dt->status == 0 ? 'Belum Jalan' : ($dt->status == 1 ? 'Sedang Jalan' : 'Selesai')}}</td>
                            <td>{{$dt->status_absen == 0 ? 'Cuti' : 'Absen'}}</td>
                            <td>
                                <a href="{{ URL('admin/master_user/detail_cuti') . '/' . $dt->id }}">
                                    <div class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable2').DataTable({
            "order": [
                [2, "desc"]
            ]
        });
    });
</script>