@include('Admin.header',['activePage' => 'list_reimburs'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<style>
    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 150px;
        padding-left: 5%;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9);
        /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 500px;
    }

    /* Caption of Modal Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation */
    .modal-content,
    #caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
        from {
            -webkit-transform: scale(0)
        }

        to {
            -webkit-transform: scale(1)
        }
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }

        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 150px;
        right: 10%;
        color: white;
        z-index: 2;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }

    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px) {
        .modal-content {
            width: 100%;
        }
    }
</style>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Reimburs</h3>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Pengaju</th>
                            <th>Keterangan</th>
                            <th>Nominal</th>
                            <th>Bukti</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        @if($dt->status == 0)
                        <tr>
                            <td>{{ $dt->tanggal }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->keterangan }}</td>
                            <td>{{ $dt->nominal }}</td>
                            <td>
                                <img src="{{$dt->url_bukti}}" onclick="imageclick('<?php echo $dt->url_bukti; ?>')" style="cursor:pointer;width:250px; height:250px;object-fit:contain;" />
                            </td>
                            <td style="display:flex; flex-direction:column;justify-content:center;height:250px;">
                                <div class="btn btn-primary" style="margin-bottom:5px;" onclick="approve(<?php echo $dt->id; ?>,1,this)">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="btn btn-danger" style="border-radius:15px;" onclick="approve(<?php echo $dt->id; ?>,-1,this)">
                                    <i class="fa fa-close"></i>
                                </div>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">History Reimburs</h3>
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Pengaju</th>
                            <th>Keterangan</th>
                            <th>Nominal</th>
                            <th>Bukti</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        @if($dt->status != 0)
                        <tr>
                            <td>{{ $dt->tanggal }}</td>
                            <td>{{ $dt->email }}</td>
                            <td>{{ $dt->keterangan }}</td>
                            <td>{{ $dt->nominal }}</td>
                            <td>
                                <img src="{{$dt->url_bukti}}" onclick="imageclick('<?php echo $dt->url_bukti; ?>')" style="cursor:pointer;width:250px; height:250px;object-fit:contain;" />
                            </td>
                            <td style="display:flex; flex-direction:column;justify-content:center;height:250px;">
                                {{ $dt->status == 1 ? 'Diterima' : 'Ditolak'}}
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>


    <!-- END MAIN CONTENT -->
</div>

<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>
<!-- END MAIN -->
@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
        $('#myTable2').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
        $('#myTable3').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });

    function approve(id, status, comp) {
        // $("#id_approve").val(status);
        if (status == -1) {
            swal({
                    title: "Reject this Reimburs?",
                    text: "Are you sure to reject this Reimburs?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        confirmApprove(id, status, comp);
                        // $("#formApprove").submit();
                    }
                });
        } else {
            swal({
                    title: "Approve this Reimburs?",
                    text: "Are you sure to Approve this Reimburs?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        confirmApprove(id, status, comp);
                        // $("#formApprove").submit();
                    }
                });
        }
    }

    function confirmApprove(id, status, comp) {
        // console.log(comp);
        // var id_daily_report = $("#id_daily_report").val();
        var formData = new FormData();
        formData.append("_token", '<?php echo csrf_token(); ?>');
        formData.append("id_reimburs", id);
        formData.append("status", status);
        $.ajax({
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            url: "/api/reimburs/approve",
            cache: false,
            success: function(data) {
                $(comp).closest('tr').remove();
                // console.log(data);
                swal("Sukses", "Update Reimburs Berhasil", "success");
            },
            fail: function(data) {
                swal("Failed", "Oops Something Wrong", "error");
            }
        });
    }

    function imageclick(img_src) {
        console.log("Asd");
        modal.style.display = "block";
        modalImg.src = img_src;
        captionText.innerHTML = img_src;
    }

    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById("myImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>