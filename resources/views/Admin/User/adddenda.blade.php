@include('Admin.header',['activePage' => 'list_denda'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Tambah Denda</h3>
                <form action="{{URL('/admin/master_user/do_add_denda')}}" method="POST">
                    @csrf
                    <div class="form-container">
                        <div class="form-group">
                            <label>Pegawai</label><br>
                            <select class="form-control input-lg theSelect" name="id_user">
                                @foreach($data_pegawai as $dt)
                                <option value="{{$dt->id}}">{{$dt->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Deksripsi Denda</label>
                            <input type="text" name="description" class="form-control input-lg">
                        </div>
                        <div class="form-group">
                            <label>Total Denda</label>
                            <input type="number" name="total_denda" class="form-control input-lg">
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" value="Tambah Denda">
                </form>
            </div>
        </div>

    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')