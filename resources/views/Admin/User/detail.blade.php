@include('Admin.header',['activePage' => 'master_user'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container.flex {
        display: flex;
    }

    .form-container .form-group.flex {
        flex-basis: 33%;
    }

    .form-group label {
        margin: 0px;
    }

    .file {
        visibility: hidden;
        position: absolute;
    }

    img {
        max-width: 300px;
        max-height: 300px;
        object-fit: cover;
    }

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Information</h3>
            </div>
            <div class="panel-body">
                <form action="{{ URL('/admin/master_user/do_edit_detail') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $data->user->id }}" name="id" />
                    <div class="form-container">
                        <div class="form-group">
                            <label>Nama</label><br>
                            <input type="text" value="{{ $data->user->name }}" name="name"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Role</label><br>
                            <select name="role" class="form-control input-lg">
                                @if ($dataUser->role == 2)
                                    @foreach ($data_role as $dt)
                                        <option value="{{ $dt->id }}"
                                            {{ $data->user->role == $dt->id ? 'selected' : '' }}>
                                            {{ $dt->nama_role }}
                                        </option>
                                    @endforeach
                                @elseif($dataUser->role == 3)
                                    <option value="6" {{ $data->user->role == 6 ? 'selected' : '' }}>ADMIN</option>
                                    <option value="4" {{ $data->user->role == 4 ? 'selected' : '' }}>PENGAWAS</option>
                                    <option value="7" {{ $data->user->role == 7 ? 'selected' : '' }}>COACH</option>
                                    <option value="1" {{ $data->user->role == 3 ? 'selected' : '' }}>PEGAWAI</option>
                                    <option value="5" {{ $data->user->role == 5 ? 'selected' : '' }}>TRAINEE</option>
                                @elseif($dataUser->role == 6)
                                    <option value="4" {{ $data->user->role == 4 ? 'selected' : '' }}>PENGAWAS
                                    </option>
                                    <option value="7" {{ $data->user->role == 7 ? 'selected' : '' }}>COACH</option>
                                    <option value="1" {{ $data->user->role == 3 ? 'selected' : '' }}>PEGAWAI</option>
                                    <option value="5" {{ $data->user->role == 5 ? 'selected' : '' }}>TRAINEE</option>
                                @else
                                    <option value="6" {{ $data->user->role == 6 ? 'selected' : '' }}>ADMIN</option>
                                    <option value="4" {{ $data->user->role == 4 ? 'selected' : '' }}>PENGAWAS
                                    </option>
                                    <option value="7" {{ $data->user->role == 7 ? 'selected' : '' }}>COACH</option>
                                    <option value="1" {{ $data->user->role == 3 ? 'selected' : '' }}>PEGAWAI</option>
                                    <option value="5" {{ $data->user->role == 5 ? 'selected' : '' }}>TRAINEE</option>
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Email</label><br>
                            <input type="text" value="{{ $data->user->email }}" name="email"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Alamat</label><br>
                            <input type="text" value="{{ $data->user->alamat }}" name="alamat"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>No KTP</label><br>
                            <input type="text" value="{{ $data->user->no_ktp }}" name="no_ktp"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>No HP</label><br>
                            <input type="text" value="{{ $data->user->no_telp }}" name="no_telp"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Nama Darurat 1</label><br>
                            <input type="text" value="{{ $data->user->nama_darurat_1 }}" name="nama_darurat_1"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Nomor Darurat 1</label><br>
                            <input type="text" value="{{ $data->user->no_telp_darurat_1 }}" name="no_telp_darurat_1"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Hubungan Darurat 1</label><br>
                            <input type="text" value="{{ $data->user->hubungan_darurat_1 }}"
                                name="hubungan_darurat_1" class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Nama Darurat 2</label><br>
                            <input type="text" value="{{ $data->user->nama_darurat_2 }}" name="nama_darurat_2"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Nomor Darurat 2</label><br>
                            <input type="text" value="{{ $data->user->no_telp_darurat_2 }}" name="no_telp_darurat_2"
                                class="form-control input-lg" />
                        </div>
                        <div class="form-group">
                            <label>Hubungan Darurat 2</label><br>
                            <input type="text" value="{{ $data->user->hubungan_darurat_2 }}"
                                name="hubungan_darurat_2" class="form-control input-lg" />
                        </div>
                        <div style="margin-top:25px;display:flex;">
                            <input type="submit" class="btn btn-primary" value="Edit Pegawai">
                            @if ($data->user->role == 7)
                                <a href="/admin/master_user/detail_coach/{{ $data->user->id }}"
                                    style="margin-left:15px;">
                                    <div class="btn btn-primary"><i class="fa fa-list"></i> List Coach</div>
                                </a>
                            @endif
                        </div>
                    </div>
                </form>
                <br>
                <form action="/admin/master_user/toggle_status_pegawai/<?php echo $data->user->id; ?>" method="POST">
                    @csrf
                    <input type="submit" class="btn {{ $data->user->status == 1 ? 'btn-danger' : 'btn-primary' }}"
                        style="border-radius:25px"
                        value="{{ $data->user->status == 1 ? 'Non Aktifkan Pegawai' : 'Aktifkan Pegawai' }}" />
                </form>
                {{-- <div class="form-container flex" style="margin-top:25px;">
                    <div class="form-group flex">
                        <label>Email Pegawai</label>
                        <p>{{ $data->user->email }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Bergabung Pada</label>
                        <p>{{ $data->user->created_at }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Alamat</label>
                        <p>{{ $data->user->alamat }}</p>
                    </div>
                </div>
                <div class="form-container flex">
                    <div class="form-group flex">
                        <label>No KTP</label>
                        <p>{{ $data->user->no_ktp }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>No HP</label>
                        <p>{{ $data->user->no_telp }}</p>
                    </div>
                </div>
                <div class="form-container flex">
                    <div class="form-group flex">
                        <label>Nama Darurat 1</label>
                        <p>{{ $data->user->nama_darurat_1 }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Nomor Darurat 1</label>
                        <p>{{ $data->user->no_telp_darurat_1 }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Hubungan Darurat 1</label>
                        <p>{{ $data->user->hubungan_darurat_1 }}</p>
                    </div>
                </div>
                <div class="form-container flex">
                    <div class="form-group flex">
                        <label>Nama Darurat 2</label>
                        <p>{{ $data->user->nama_darurat_2 }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Nomor Darurat 2</label>
                        <p>{{ $data->user->no_telp_darurat_2 }}</p>
                    </div>
                    <div class="form-group flex">
                        <label>Hubungan Darurat 2</label>
                        <p>{{ $data->user->hubungan_darurat_2 }}</p>
                    </div>
                </div> --}}
                <h5 style="text-align:center;font-weight:bold;font-size:16px;">DATA FOTO</h5>
                <div class="form-container flex">
                    <div class="form-group" style="flex-basis:50%;padding:10px 10%;">
                        <img src="{{ $data->user->foto_ktp }}" id="preview" class="img-thumbnail"
                            style="width:300px;">
                        <form method="post" id="image-form" action="{{ url('/admin/uploadpp') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <input id="inputfile1" type="file" name="file" class="file" accept="image/*">
                            <input type="hidden" name="type" value="ktp" />
                            <input type="hidden" name="id_user" value="{{ $data->user->id }}" />
                            <div class="input-group my-3">
                                {{-- <input id="inputfile2" type="text" class="form-control" disabled placeholder="Upload File" id="file"> --}}
                                <div class="input-group-append">
                                    <button id="browse1" type="button" class="browse btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="form-group" style="text-align:center;flex-basis:50%; text-align:left;">
                        <div class="form-group" style="flex-basis:50%;padding:10px 10%;">
                            <img src="{{ $data->user->foto_selfie }}" id="preview2" class="img-thumbnail"
                                style="width:300px;">
                            <form method="post" id="image-form2" action="{{ url('/admin/uploadpp') }}"
                                enctype="multipart/form-data">
                                @csrf
                                <input id="inputfile2" type="file" name="file" class="file" accept="image/*">
                                <input type="hidden" name="type" value="selfie" />
                                <input type="hidden" name="id_user" value="{{ $data->user->id }}" />
                                <div class="input-group my-3">
                                    {{-- <input type="text" class="form-control" disabled placeholder="Upload File" id="file2"> --}}
                                    <div class="input-group-append">
                                        <button id="browse2" type="button"
                                            class="browse btn btn-primary">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Lokasi Kantor</h3>
            </div>

            <div class="panel-body">
                <form action="{{ URL('/admin/master_user/do_edit_kantor') }}" method="POST">
                    @csrf
                    <input name="id_user" type="hidden" value="{{ $data->user->id }}" />
                    <div class="form-group">
                        <select name="id_kantor" class="form-control input-lg theSelect">
                            @foreach ($data_kantor as $dt)
                                <option value="{{ $dt->id }}"
                                    {{ $dt->id == $data->user->id_kantor ? 'selected' : '' }}>{{ $dt->alamat }}
                                </option>
                            @endforeach
                        </select>
                        <input type="submit" class="btn btn-primary" style="margin-top:25px;" value="Edit Lokasi">
                    </div>
                </form>
            </div>
        </div>

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Axie yang dimainkan</h3>
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Email Akun</th>
                            <th>Negara</th>
                            <th>Level</th>
                            <th>Inventory SLP</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->axie as $dt)
                            <tr>
                                <td>{{ $dt->email_akun }}</td>
                                <td>{{ $dt->negara }}</td>
                                <td>{{ $dt->level }}</td>
                                <td>{{ $dt->total_slp }}</td>
                                <td>
                                    <a href="{{ URL('admin/master_axie/') . '/' . $dt->id }}">
                                        <div class="btn btn-primary">
                                            Detail
                                        </div>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Kontak Pegawai</h3>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama Kontak</th>
                            <th>Nomor Kontak</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data->kontak as $dt)
                            <tr>
                                <td>{{ $dt->name }}</td>
                                <td>{{ $dt->telephone }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $(".theSelect").select2();
        $('#myTable').DataTable({});
        $('#myTable2').DataTable({});
    });

    $(document).on("click", "#browse1", function() {
        var file = $("#inputfile1");
        file.trigger("click");
    });

    $(document).on("click", "#browse2", function() {
        var file = $("#inputfile2");
        file.trigger("click");
    });

    $('#inputfile1').change(function(e) {
        console.log(e);
        var fileName = e.target.files[0].name;
        // $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview").src = e.target.result;
            $("#preview").css("width", "300px");
            $("#image-form").submit();
            // console.log(e.target.result);
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $('#inputfile2').change(function(e) {
        var fileName = e.target.files[0].name;
        // $("#file2").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
            // get loaded data and render thumbnail.
            document.getElementById("preview2").src = e.target.result;
            $("#image-form2").submit();
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });
</script>
