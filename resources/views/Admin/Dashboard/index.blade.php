@include('Admin.header',['activePage' => 'dashboard'])
<style>
    .p-number {
        color: #E05227;
        font-weight: bold;
        font-size: 18px;
    }

    .icon.ic2 {
        margin-top: 5px;
    }

</style>
<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/home.css') }}">
<!-- MAIN -->
<div class="main">
    <div class="loading-overlay" id="loading-overlay">
        <i class="fa fa-spinner fa-spin spin-big"></i>
        <h5>Please Wait ...</h5>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->

            @if ($dataUser->role == 2)
                <div class="panel panel-headline">
                    <div class="panel-heading">
                        <h3 class="panel-title" style="margin-bottom:25px;">Dashboard</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span class="number">{{ $dataDashboard->average_slp_2days }}</span>
                                        <span class="title">Avg SLP 2 days ago</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span
                                            class="number">{{ $dataDashboard->average_slp_yesterday }}</span>
                                        <span class="title">Avg SLP Yesterday</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span class="number">{{ $dataDashboard->average_slp_today }}</span>
                                        <span class="title">Avg SLP Today</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span class="number">{{ $dataDashboard->total_gain_slp_2days }}</span>
                                        <span class="title">Gain SLP 2 days ago</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span
                                            class="number">{{ $dataDashboard->total_gain_slp_yesterday }}</span>
                                        <span class="title">Gain SLP Yesterday</span>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="metric">
                                    <span class="icon"><i class="fa fa-book"></i></span>
                                    <p>
                                        <span class="number">{{ $dataDashboard->total_gain_slp_today }}</span>
                                        <span class="title">Gain SLP Today</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">Monthly Overview</h3>
                    <div class="form-group">
                        <label>Owner</label>
                        <select class="form-control input-lg" id="cbOwner" onchange="loadData()"
                            {{ $dataUser->role == 3 ? 'disabled' : '' }}>
                            @if ($dataUser->role == 3)
                                <option value="{{ $dataUser->id }}">{{ $dataUser->name }}</option>
                            @else
                                @foreach ($dataOwner as $dt)
                                    <option value="{{ $dt->id }}">{{ $dt->name }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Periode</label>
                        <input id="periode" type="month" class="form-control input-lg" onchange="loadData()" />
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-plus"></i></span>
                                <p>
                                    <span class="number" id="total_gained_slp">0</span>
                                    <span class="title">Gained SLP</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-minus"></i></span>
                                <p>
                                    <span class="number" id="total_claimed_slp">0</span>
                                    <span class="title">Claimed SLP</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-shopping-bag"></i></span>
                                <p>
                                    <span class="number" id="total_inventory">0</span>
                                    <span class="title">Inventory SLP</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="avg_slp_2days">0</span>
                                    <span class="title">Avg SLP 2 days ago</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="avg_slp_yesterday">0</span>
                                    <span class="title">Avg SLP Yesterday</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="avg_slp_today">0</span>
                                    <span class="title">Avg SLP Today</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="total_slp_2days">0</span>
                                    <span class="title">Gain SLP 2 days ago</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="total_slp_yesterday">0</span>
                                    <span class="title">Gain SLP Yesterday</span>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="metric">
                                <span class="icon"><i class="fa fa-book"></i></span>
                                <p>
                                    <span class="number" id="total_slp_today">0</span>
                                    <span class="title">Gain SLP Today</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <a id="btn-daily-report" href="#">
                        <div class="btn btn-primary">Lihat Laporan Daily Report</div>
                    </a>
                    <div class="row" style="margin-top:25px;">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="text-align: center;">Grafik Perolehan Rata" SLP</h3>
                            </div>
                            <div class="panel-body">
                                <canvas id="myChart"></canvas>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="panel-heading">
                                <h3 class="panel-title" style="text-align: center;">Grafik Pengeluaran</h3>
                            </div>
                            <div class="panel-body">
                                <canvas id="myChart2"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">Daftar Pegawai</h3>
                </div>
                <div class="panel-body">
                    <table id="myTable" class="table table-bordered display">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Total SLP Gain</th>
                                <th>Average PVP</th>
                                <th>Last Update</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-headline">

                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">Daftar Akun</h3>
                </div>
                <div class="panel-body">
                    <table id="myTable2" class="table table-bordered display">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>Inventory SLP</th>
                                <th>Total SLP Gain</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">Leaderboard</h3>
                </div>
                <div class="panel-body">
                    <table id="myTable3" class="table table-bordered display">
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Player</th>
                                <th>Account Email</th>
                                <th>Average SLP</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">TOP 10 LEADERBOARD</h3>
                </div>
                <div class="panel-body">
                    <table id="myTable4" class="table table-bordered display">
                        <thead>
                            <tr>
                                <th>Rank</th>
                                <th>Player</th>
                                <th>Account Email</th>
                                <th>Average SLP</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.3.0/Chart.js"></script>
<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var ctx2 = document.getElementById('myChart2').getContext('2d');
    $(function() {
        $("#myTable").DataTable({
            "order": [
                [2, "desc"]
            ]
        });
        $("#myTable2").DataTable({
            "order": [
                [2, "desc"]
            ]
        });
        $("#myTable3").DataTable({
            "order": [
                [0, "asc"]
            ]
        });
        $("#myTable4").DataTable({
            "order": [
                [0, "asc"]
            ]
        });

        var d = new Date();
        var month = d.getMonth() + 1;
        if (month <= 9) {
            month = "0" + month;
        }
        var year = d.getFullYear();
        var value = year + "-" + month;
        $("#periode").val(value);

        loadData();
    });

    function loadData() {
        $("#loading-overlay").css("visibility", "visible");
        var id_owner = $("#cbOwner").val();
        var periode = $("#periode").val();
        var temp = periode.split("-");
        var year = temp[0];
        var month = temp[1];
        console.log(periode);

        document.getElementById("btn-daily-report").href = "/admin/laporan_daily_report?month=" + month + "&year=" +
            year + "&id_owner=" + id_owner;

        $.ajax({
            type: "GET",
            url: "/api/get_dashboard?id_owner=" + id_owner,
            cache: false,
            success: function(data) {
                $("#avg_slp_today").html(data.payload.average_slp_today);
                $("#avg_slp_yesterday").html(data.payload.average_slp_yesterday);
                $("#avg_slp_2days").html(data.payload.average_slp_2days);

                $("#total_slp_today").html(data.payload.total_gain_slp_today);
                $("#total_slp_yesterday").html(data.payload.total_gain_slp_yesterday);
                $("#total_slp_2days").html(data.payload.total_gain_slp_2days);
            }
        });

        $.ajax({
            type: "GET",
            url: "/api/laporan/slp_by_owner?id_owner=" + id_owner + "&month=" + month + "&year=" + year,
            cache: false,
            success: function(data) {
                console.log(data);
                $("#total_gained_slp").html(data.payload.total_slp_gained);
                $("#total_claimed_slp").html(data.payload.total_slp_claimed);
                $("#total_inventory").html(data.payload.total_slp_unclaimed);
                // console.log(data.jadwal_cuti);
                // data_detail_cuti = data.jadwal_cuti;
                var t = $("#myTable").DataTable();
                t.clear().draw(false);
                data.payload.pegawai.forEach(function(item, index) {
                    t.row.add([
                        item.name,
                        item.email,
                        item.total_gain,
                        item.data_pvp.average_pvp,
                        item.last_update,
                        "<a href='/admin/dashboard/detail?id=" + item.id + "&month=" +
                        month + "&year=" + year + "&id_owner=" + id_owner +
                        "'><div class='btn btn-primary'><i class='fa fa-eye'></i></div></a>"
                    ]).draw(false);
                });

                var t2 = $("#myTable2").DataTable();
                t2.clear().draw(false);
                data.payload.axie.forEach(function(item, index) {
                    if (!item.total_gain_slp) {
                        item.total_gain_slp = 0;
                    }
                    t2.row.add([
                        item.email_akun,
                        item.total_slp,
                        item.total_gain_slp,
                    ]).draw(false);
                });

                $("#loading-overlay").css("visibility", "hidden");

            }
        });

        $.ajax({
            type: "GET",
            url: "/api/get_leaderboard?month=" + month + "&year=" + year,
            cache: false,
            success: function(data) {
                console.log(data);
                var t = $("#myTable3").DataTable();
                t.clear().draw(false);
                data.payload.data.forEach(function(item, index) {
                    t.row.add([
                        item.rank,
                        item.name,
                        item.axie,
                        item.average_pvp
                    ]).draw(false);
                });

                var t2 = $("#myTable4").DataTable();
                t2.clear().draw(false);
                data.payload.top_10.forEach(function(item, index) {
                    t2.row.add([
                        item.rank,
                        item.name,
                        item.axie,
                        item.average_pvp,
                    ]).draw(false);
                });


            }
        });

        $.ajax({
            type: "GET",
            url: "/api/laporan/perolehan_slp?year=" + year + "&id_owner=" + id_owner,
            cache: false,
            success: function(data) {
                console.log(data);
                var label_line = [];
                var data_line = [];
                data.payload.forEach((dt) => {
                    label_line.push(dt.month);
                    data_line.push((dt.total_slp / dt.total_axie).toFixed());
                });

                var myChart = new Chart(ctx, {
                    type: 'line',
                    data: {
                        labels: label_line,
                        datasets: [{
                            label: 'Average SLP',
                            data: data_line,
                            fill: false,
                            borderColor: 'rgb(75, 192, 192)'
                        }]
                    },
                    options: {
                        elements: {
                            line: {
                                tension: 0
                            }
                        },
                        maintainAspectRatio: true,
                        scales: {
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    display: true
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 500,
                                }
                            }]
                        },
                    }
                })
            }
        });


        $.ajax({
            type: "GET",
            url: "/api/laporan/pengeluaran?year=" + year + "&id_owner=" + id_owner,
            cache: false,
            success: function(data) {
                console.log(data);
                var label_line = [];
                var data_line = [];
                data.payload.forEach((dt) => {
                    label_line.push(dt.month);
                    data_line.push(dt.total_pengeluaran);
                });

                var myChart2 = new Chart(ctx2, {
                    type: 'line',
                    data: {
                        labels: label_line,
                        datasets: [{
                            label: 'Total Pengeluaran',
                            data: data_line,
                            fill: false,
                            borderColor: 'rgb(75, 192, 192)'
                        }]
                    },
                    options: {
                        elements: {
                            line: {
                                tension: 0
                            }
                        },
                        maintainAspectRatio: true,
                        scales: {
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    display: true,
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    stepSize: 10000000,
                                }
                            }]
                        },
                        tooltips: {
                            callbacks: {
                                label: function(tooltipItem, data) {
                                    return "Rp. " + numberWithCommas(tooltipItem.yLabel);
                                }
                            }
                        }
                    }
                })
            }
        });

    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }
</script>
