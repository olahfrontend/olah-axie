@include('Admin.header',['activePage' => 'dashboard'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    table {
        width: 100%;
        overflow-x: auto;
        overflow-y: auto;
        border-collapse: collapse;
        border-spacing: 0;
    }

    table th {
        vertical-align: middle;
        text-align: center;
        min-width: 80px;
    }

    .panel-body {
        width: 100%;
        height: 100%;
        overflow: auto;
    }

    table {
        border-collapse: separate;
        page-break-inside: auto;
        /* Don't collapse */
    }

    thead {
        position: sticky;
        top: -20px;
        background-color: white;
        border: 1px solid black;
        z-index: 15;
        display: table-header-group;
    }

    .first-td {
        z-index: 10;
        position: sticky;
        left: -28px;
        background-color: white;
        /* width: 300px; */
        border: 1px solid black;
    }

    .acc {
        z-index: 15;
    }

    body {
        overflow: hidden;
    }

    tr {
        page-break-inside: avoid;
        page-break-after: auto;
    }


    @media print {
        @page {
            size: landscape
        }

        table {
            page-break-after: auto;
            overflow: visible !important;
        }

        tr {
            page-break-inside: avoid;
            page-break-after: auto
        }

        td {
            page-break-inside: avoid;
            page-break-after: auto
        }

        thead {
            display: table-header-group
        }

        tfoot {
            display: table-footer-group
        }
    }

</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Laporan Daily Report Monthly</h3>
                <div class="btn btn-primary" onclick="fnExcelReport()">
                    Export as Excel
                </div>
            </div>
            <div class="panel-body" style="height:700px;">
                <table class="table table-bordered display" id="myTable">
                    <thead>
                        <tr>
                            <th rowspan="2" class="first-td acc">Account Name</th>
                            @foreach ($data->array_tanggal as $dt)
                                <th colspan="6">{{ $dt }}</th>
                            @endforeach
                        </tr>
                        <tr>
                            @foreach ($data->array_tanggal as $dt)
                                <th>PVP</th>
                                <th>Adv</th>
                                <th>Quest</th>
                                <th>Inv</th>
                                <th>Lvl</th>
                                <th>Player</th>
                            @endforeach
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($data)) {
                            $length = count($data->data);
                            for ($i = 0; $i < $length; $i++) {
                                echo '<tr>';
                                for ($j = 0; $j < count($data->data[$i]->daily); $j++) { ?>
                        @if (isset($data->data[$i]->daily[$j]->data_daily))
                            @if ($j == 0)
                                <td class="first-td">{{ $data->data[$i]->nama_akun }}</td>
                            @endif
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->slp_pvp }}</td>
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->slp_adventure }}</td>
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->slp_quest }}</td>
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->last_inventory }}</td>
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->level_before }}</td>
                            <td>{{ $data->data[$i]->daily[$j]->data_daily->name }}</td>
                        @else
                            @if ($j == 0)
                                <td class="first-td">{{ $data->data[$i]->nama_akun }}</td>
                            @endif
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                            <td>-</td>
                        @endif

                        <?php }
                                echo '</tr>';
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<iframe id="txtArea1" style="display:none"></iframe>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
    function printPDF() {
        if (!$('body').hasClass('layout-fullwidth')) {
            $('body').addClass('layout-fullwidth');
        }

        window.print()
    }

    function fnExcelReport() {
        var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
        var textRange;
        var j = 0;
        tab = document.getElementById('myTable'); // id of table

        for (j = 0; j < tab.rows.length; j++) {
            tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
            //tab_text=tab_text+"</tr>";
        }

        tab_text = tab_text + "</table>";
        tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
        tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
        tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
        {
            txtArea1.document.open("txt/html", "replace");
            txtArea1.document.write(tab_text);
            txtArea1.document.close();
            txtArea1.focus();
            sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
        } else //other browser not tested on IE 11
            sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

        return (sa);
    }
    $(document).ready(function() {});
</script>
