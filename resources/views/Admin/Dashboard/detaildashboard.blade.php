@include('Admin.header',['activePage' => 'dashboard'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Detail Information</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label>Nama Pegawai</label>
                    <input value="{{$data->name_pegawai}}" type="text" class="form-control input-lg" readonly />
                </div>
                <div class="form-group">
                    <label>Periode</label>
                    <input value="{{$data->periode}}" type="text" class="form-control input-lg" readonly />
                </div>
                <table class="table table-bordered display" id="myTable">
                    <thead>
                        <tr>
                            <th>Email Akun</th>
                            <th>Average Daily</th>
                            <th>Inventory</th>
                            <th>Withdraw</th>
                            <th>Total Gained</th>
                            <th>Last Claim on</th>
                            <th>Average PVP</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data->data_axie as $dt)
                        <tr>
                            <td>{{$dt->email_akun}}</td>
                            <td>{{$dt->avg_slp}}</td>
                            <td>{{$dt->unclaimed}}</td>
                            <td>{{$dt->total_withdraw}}</td>
                            <td>{{$dt->gain_slp}}</td>
                            <td>
                                @if(isset($dt->last_withdraw))
                                {{$dt->last_withdraw}}
                                @else
                                -
                                @endif
                            </td>
                            <td>{{$dt->avg_pvp}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({});
        $('#myTable2').DataTable({});
    });
</script>