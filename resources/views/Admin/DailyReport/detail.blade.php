@include('Admin.header',['activePage' => 'daily_report'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 33%;
    }

	.form-group{
		text-align:left;
	}

    .form-group label {
        margin: 0px;
    }

    .btn {
        border-radius: 25px;
        font-weight: bold;
    }
	
	.form-control{
		width:100% !important;
	}
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Detail Information</h3>
                <div class="form-container">
                    <div class="form-group">
                        <label>Name</label>
                        <p>{{$data->name}}</p>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <p>{{$data->email_akun}}</p>
                    </div>
                    <div class="form-group">
                        <label>Submit Date</label>
                        <p>{{$data->created_at}}</p>
                    </div>
                </div>
                <div class="form-container">
                    <div class="form-group">
                        <label>SLP Adventure</label>
                        <p>{{$data->slp_adventure}}</p>
                    </div>
                    <div class="form-group">
                        <label>SLP PVP</label>
                        <p>{{$data->slp_pvp}}</p>
                    </div>
                    <div class="form-group">
                        <label>SLP Quest</label>
                        <p>{{$data->slp_quest}}</p>
                    </div>
                </div>
                <div class="form-container">
                    <div class="form-group">
                        <label>Total SLP Gain</label>
                        <p>{{($data->slp_pvp + $data->slp_quest + $data->slp_adventure)}}</p>
                    </div>
                    <div class="form-group">
                        <label>Last Inventory</label>
                        <p>{{$data->last_inventory}}</p>
                    </div>
                </div>
                <div class="form-container">
                    <div class="form-group" style="flex-basis:100%;">
                        <label>Keterangan</label>
                        <p>{{$data->keterangan}}</p>
                    </div>
                </div>
                <h5 style="text-align:center;font-weight:bold;font-size:16px;">SCREENSHOT BUKTI</h5>
                <div class="form-container">
                    <div class="form-group" style="margin:auto;">
                        <img src="{{$data->url_screenshot}}" style="max-width:600px;" />
                    </div>
                </div>
                <div class="btn btn-primary" onclick="adjustment()">Report Adjustment</div>
                <!-- <div class="form-container">
                    <div class="form-group" style="margin:25px auto;">
                        <form action="{{URL('/admin/do_approve')}}" id="formApprove">
                            <input type="hidden" name="id_daily_report" value="{{$data->id}}">
                            <input type="hidden" name="status" id="id_approve">
                        </form>
                        <div class="btn btn-primary" onclick="approve(1)">Approve</div>
                        <div class="btn btn-danger" onclick="approve(-1)">Reject</div>
                    </div>
                </div> -->
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>

<div id="modalAdjustment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Report Adjustment</h4>
            </div>
            <div class="modal-body">
                <div class="modal-content">
               
                        <form action="{{URL('/admin/do_adjustment')}}" id="formApprove" method="POST">
							@csrf
                            <input type="hidden" name="id_daily_report" value="{{$data->id}}" />
                            <input type="hidden" name="id_axie" value="{{$data->id_axie}}" />
                            <div class="form-group">
								<label>Gaji</label>
								<input type="number" name="gaji" value="8000" class="form-control input-lg"/>
							</div>
                            <div class="form-group">
								<label>SLP Adventure</label>
								<input type="number" name="slp_adventure" value="50" class="form-control input-lg" />
							</div>
                            <div class="form-group">
								<label>SLP Quest</label>
								<input type="number" name="slp_quest" value="25" class="form-control input-lg"/>
							</div>
                            <div class="form-group">
								<label>SLP PVP</label>
								<input type="number" name="slp_pvp" class="form-control input-lg" />
							</div>
                            <div class="form-group">
								<label>Last Inventory</label>
								<input type="number" name="last_inventory" class="form-control input-lg"/>
							</div>
							<input style="margin-top:15px;" type="submit" class="btn btn-primary" value="SUBMIT" />
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')
<script>
    // function approve(status) {
    //     $("#id_approve").val(status);
    //     $("#formApprove").submit();
    // }
	
	function adjustment(){
        $("#modalAdjustment").modal();
	}

</script>