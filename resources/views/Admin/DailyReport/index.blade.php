@include('Admin.header',['activePage' => 'daily_report'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<style>
    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 150px;
        padding-left: 5%;
        /* Location of the box */
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.9);
        /* Black w/ opacity */
    }

    /* Modal Content (image) */
    .modal-content {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 500px;
    }

    /* Caption of Modal Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }

    /* Add Animation */
    .modal-content,
    #caption {
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }

    @-webkit-keyframes zoom {
        from {
            -webkit-transform: scale(0)
        }

        to {
            -webkit-transform: scale(1)
        }
    }

    @keyframes zoom {
        from {
            transform: scale(0)
        }

        to {
            transform: scale(1)
        }
    }

    /* The Close Button */
    .close {
        position: absolute;
        top: 150px;
        right: 10%;
        color: white;
        z-index: 2;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }

    .close:hover,
    .close:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }

    .spin-big {
        font-size: 50px;
        height: 50px;
        width: 50px;
        color: #74B7BD;
    }

    .loading-overlay {
        visibility: hidden;
        width: 100%;
        height: 100%;
        padding-left: calc(50vw - 100px);
        padding-top: calc(50vh - 100px);
        position: fixed;
        color: white;
        background-color: rgba(0, 0, 0, 0.5);
        z-index: 99;
    }

    .loading-overlay h5 {
        font-weight: bold;
        font-size: 18px;
        margin-left: -20px;
    }
</style>
<!-- MAIN -->
<div class="main">
    <div class="loading-overlay" id="loading-overlay">
        <i class="fa fa-spinner fa-spin spin-big"></i>
        <h5>Please Wait ...</h5>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Daily Report</h3>
                <!-- <a href="/admin/master_axie/add">
            <div class="btn btn-primary" style="margin-top:25px;">Tambah Axie</div>
        </a> -->
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email Akun</th>
                            <th>SLP yang didapat</th>
                            <th>Inventory</th>
                            <th>Screenshot</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $dt)
                        <tr>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->email_akun }}</td>
                            <td>
                                ADVENTURE : {{$dt->slp_adventure}}<br>
                                PVP : {{$dt->slp_pvp}}<br>
                                QUEST : {{$dt->slp_quest}}<br>
                                TOTAL SLP : {{($dt->slp_adventure+$dt->slp_pvp+$dt->slp_quest) }}<br><br>
                                LEVEL SEKARANG : {{$dt->level}}<br>
                                LEVEL BEFORE : {{$dt->level_before}}
                            </td>
                            <td>{{ $dt->last_inventory }}</td>
                            <td>
                                <img src="{{$dt->url_screenshot}}" onclick="imageclick('<?php echo $dt->url_screenshot; ?>')" style="cursor:pointer;width:250px; height:250px;object-fit:contain;" />
                            </td>
                            <form action="{{URL('/admin/do_approve')}}" id="formApprove">
                                <input type="hidden" id="id_daily_report" name="id_daily_report" value="{{$dt->id}}">
                                <input type="hidden" name="status" id="id_approve">
                            </form>
                            <td style="display:flex; flex-direction:column;justify-content:center;height:250px;">
                                <div class="btn btn-primary" style="margin-bottom:5px;" onclick="approve(<?php echo $dt->id; ?>,1,this)">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="btn btn-danger" style="border-radius:15px;" onclick="approve(<?php echo $dt->id; ?>,-1,this)">
                                    <i class="fa fa-close"></i>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>

    <div class="main-content">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Pegawai belum report</h3>
                <!-- <a href="/admin/master_axie/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Axie</div>
                </a> -->
            </div>

            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email Akun</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_belum_report as $dt)
                        <tr>
                            <td>{{ $dt[0]->name }}</td>
                            <td>{{ $dt[0]->email_akun }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>

    <div class="main-content">
        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Pegawai sudah report</h3>
                <!-- <a href="/admin/master_axie/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Axie</div>
                </a> -->
            </div>

            <div class="panel-body">
                <table id="myTable3" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email Akun</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_sudah_report as $dt)
                        <tr>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->email_akun }}</td>
                            <td>
                                <a href="/admin/daily_report/{{$dt->id_daily_report}}">
                                    <div class="btn btn-primary"><i class="fa fa-eye"></i></div>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>

<div id="myModal" class="modal">
    <span class="close">&times;</span>
    <img class="modal-content" id="img01">
    <div id="caption"></div>
</div>
<!-- END MAIN -->
@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
        $('#myTable2').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
        $('#myTable3').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });

    function approve(id, status, comp) {
        if (status == -1) {
            swal({
                    title: "Reject this daily report?",
                    text: "Are you sure to reject this daily report?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        confirmApprove(id, status, comp);
                        // $("#formApprove").submit();
                    }
                });
        } else {
            // $("#formApprove").submit();
            confirmApprove(id, status, comp);
        }
    }

    function confirmApprove(id, status, comp) {
        // console.log(comp);

        $("#loading-overlay").css("visibility", "visible");

        var formData = new FormData();
        formData.append("_token", '<?php echo csrf_token(); ?>');
        formData.append("id_daily_report", id);
        formData.append("status", status);
        $.ajax({
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            url: "/api/report/approve",
            cache: false,
            success: function(data) {
                $(comp).closest('tr').remove();
                // console.log(data);
                $("#loading-overlay").css("visibility", "hidden");
                swal("Sukses", "Update Report Berhasil", "success");
            },
            fail: function(data) {
                $("#loading-overlay").css("visibility", "hidden");
                swal("Failed", "Oops Something Wrong", "error");
            }
        });
    }

    function imageclick(img_src) {
        console.log("Asd");
        modal.style.display = "block";
        modalImg.src = img_src;
        captionText.innerHTML = img_src;
    }

    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.getElementById("myImg");
    var modalImg = document.getElementById("img01");
    var captionText = document.getElementById("caption");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }
</script>