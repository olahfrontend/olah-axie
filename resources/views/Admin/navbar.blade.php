<!-- NAVBAR -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="brand">
        <a href="/"><img src="{{URL::asset('assets/img/logo-dark.png')}}" alt="Klorofil Logo" class="img-responsive logo"></a>
    </div>
    <div class="container-fluid">
        <div class="navbar-btn">
            <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
        </div>
        <div class="navbar-btn navbar-btn-right">
            <form action="{{URL('logout')}}" method="POST" id="form-logout">
                @csrf
                <a class="btn btn-primary btn-login" onclick="Logout()"><span>SIGN OUT</span></a>
            </form>
        </div>
    </div>
</nav>
<!-- END NAVBAR -->
<!-- LEFT SIDEBAR -->
<div id="sidebar-nav" class="sidebar">
    <h5 style="text-align:center;color:white;font-weight:bold;margin-top:25px;">ADMIN PANEL</h5>
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <?php $index_sub = 0; ?>
                @foreach($dataUser->data_hakakses as $dt)
                @if($dt->have_submenu == 0)
                <li><a href="{{URL($dt->url)}}" class="{{$activePage == $dt->active_page ? 'active' :''}}"><i class="{{$dt->icon_fa}}"></i> <span>{{$dt->nama_menu}}</span></a></li>
                @else
                <li>
                    <?php
                    $selected = str_contains($dt->active_page, $activePage);
                    ?>
                    <a href="#subPages<?php echo $index_sub; ?>" data-toggle="collapse" class="<?php echo $selected ? 'active' : 'collapsed'; ?>"><i class="{{$dt->icon_fa}}"></i> <span>{{$dt->nama_menu}}</span> <i class="icon-submenu lnr lnr-chevron-left"></i></a>
                    <div id="subPages<?php echo $index_sub; ?>" class="<?php echo $selected ? 'collapse in' : 'collapse'; ?>">
                        <ul class="nav">
                            @foreach($dt->submenu as $d)
                            <li><a href="{{URL($d->url)}}" class="{{$activePage == $d->active_page ? 'active' :''}}">{{$d->nama_menu}}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </li>
                <?php $index_sub++; ?>
                @endif
                @endforeach

            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->