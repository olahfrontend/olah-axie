@include('Admin.header',['activePage' => 'master_kantor'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .form-container {
        display: flex;
    }

    .form-container .form-group {
        flex-basis: 50%;
    }

    .form-group label {
        margin: 0px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title" style="margin-bottom:25px;">Informasi Kantor</h3>
                <form action="{{URL('/admin/master_kantor/do_add')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Owner</label><br>
                        <select class="form-control input-lg" name="id_owner" {{$dataUser->role != 2 ? 'readonly' : ''}}>
                            @if($dataUser->role == 2)
                            @foreach($data_owner as $dt)
                            <option value='{{$dt->id}}'>{{$dt->name}}</option>
                            @endforeach
                            @else
                            <option value='{{$dataUser->id}}' selected>{{$dataUser->name}}</option>
                            @endif
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label><br>
                        <input type="text" name="alamat" class="form-control input-lg" />
                    </div>
                    <input type="submit" class="btn btn-primary" value="Add">
                </form>
            </div>
        </div>

    </div>
    <!-- END BORDERED TABLE -->
</div>
<!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@include('Admin.footer')