@include('Admin.header',['activePage' => 'master_kantor'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .btn {
        border-radius: 15px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Kantor</h3>
                <a href="/admin/master_kantor/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Kantor</div>
                </a>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Alamat Kantor</th>
                            <th>Owner</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_kantor as $dt)
                        <tr>
                            <td>{{ $dt->alamat }}</td>
                            <td>{{ $dt->name }}</td>
                            <td>{{ $dt->created_at }}</td>
                            <td>
                                <a href="{{ URL('admin/master_kantor/') . '/' . $dt->id }}">
                                    <div class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        }); 
    });
</script>