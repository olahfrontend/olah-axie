@include('Admin.header',['activePage' => 'all_axie_tracker'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<link rel="stylesheet" href="{{ URL::asset('css/detail_tracker.css') }}?v=1.3">
<style>
    /* The snackbar - position it at the bottom and in the middle of the screen */
    #snackbar {
        visibility: hidden;
        /* Hidden by default. Visible on click */
        min-width: 250px;
        /* Set a default minimum width */
        margin-left: -125px;
        /* Divide value of min-width by 2 */
        background-color: #333;
        /* Black background color */
        color: #fff;
        /* White text color */
        text-align: center;
        /* Centered text */
        border-radius: 2px;
        /* Rounded borders */
        padding: 16px;
        /* Padding */
        position: fixed;
        /* Sit on top of the screen */
        z-index: 1;
        /* Add a z-index if needed */
        left: 50%;
        /* Center the snackbar */
        bottom: 30px;
        /* 30px from the bottom */
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible;
        /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
  However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }

        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }

        to {
            bottom: 0;
            opacity: 0;
        }
    }
</style>
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->
            <div class="header-1">
                <div class="card-name">
                    <h5 id="name_profile">-</h5>
                    <p>Ronin Address</p>
                    <div style="display:flex;color:#74B7BD;">
                        <p id="ronin_address">{{$data_axie->address_ronin}}</p>
                        <i class="fa fa-copy" style="cursor:pointer;margin-left:5px;" onclick="copyToClipboard()"></i>
                    </div>
                </div>
            </div>

            <div class="content1">
                <div class="card-item">
                    <h5>Email</h5>
                    <div class="detail">
                        <div class="information">{{$data_axie->email_akun}}</div>
                        <div class="icon"><img src="{{URL::asset('image/email.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Pegawai</h5>
                    <div class="detail">
                        <div class="information">{{$data_axie->name}}</div>
                        <div class="icon"><img src="{{URL::asset('image/user.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Last Claim</h5>
                    <div class="detail">
                        <div class="information" id="last_claim">-</div>
                        <div class="icon"><img src="{{URL::asset('image/SLP.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Unclaimed SLP</h5>
                    <div class="detail">
                        <div class="information" id="unclaimed_slp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/SLP.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Claimed SLP</h5>
                    <div class="detail">
                        <div class="information" id="claimed_slp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/SLP.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Total SLP</h5>
                    <div class="detail">
                        <div class="information" id="total_slp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/SLP.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Average Gain SLP</h5>
                    <div class="detail">
                        <div class="information">{{$average_gain_slp}}</div>
                        <div class="icon"><img src="{{URL::asset('image/SLP.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Win Rate</h5>
                    <div class="detail">
                        <div class="information" id="win_rate">0</div>
                        <div class="icon"><img src="{{URL::asset('image/pvp.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Total Play PVP Today</h5>
                    <div class="detail">
                        <div class="information" id="total_pvp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/pvp.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>ELO</h5>
                    <div class="detail">
                        <div class="information" id="elo_pvp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/trophy.png')}}" /></div>
                    </div>
                </div>
                <div class="card-item">
                    <h5>Rank</h5>
                    <div class="detail">
                        <div class="information" id="rank_pvp">0</div>
                        <div class="icon"><img src="{{URL::asset('image/pvp.png')}}" /></div>
                    </div>
                </div>
            </div>

            <div class="content2">
                <div class="header">
                    Daily SLP Tracker & MMR Tracker
                </div>
                <div class="content">
                    <div class="left-side">
                        <canvas id="myChart1" width="100%" height="auto"></canvas>
                    </div>
                    <div class="right-side">
                        <canvas id="myChart3" width="100%" height="auto"></canvas>
                    </div>
                </div>
                <div class="header" style="margin-top:15px;">
                    Log 50 Battle PVP
                </div>
                <div class="content">
                    <div class="center-side">
                        <canvas id="myChart2" width="auto" height="auto"></canvas>
                    </div>
                </div>
            </div>

            <div class="content3">
                <div class="header">
                    Inventory Axie's
                </div>
                <div class="content" id="content_axie">
                    <!-- <div class="item">
                        <div class="header-item">
                            <img src="https://axie.zone/func/axiegenerator.php?color=ff9ab8&class=bird&pattern=circles&tail=granmas_fan&back=ronin&ears=bubblemaker&horn=pliers&eyes=chubby&mouth=goda" />
                            <h5>Axie #2202361</h5>
                            <p>#2202361</p>
                        </div>
                        <div class="desc-item">
                            <table>
                                <thead>
                                    <tr>
                                        <th>D</th>
                                        <th>R1</th>
                                        <th>R2</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="color:#FF9BB9;">Chubby</td>
                                        <td style="color:#5C9F11;">Confused</td>
                                        <td style="color:#FF9BB9;">Mavis</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#FF9BB9;">Early Bird</td>
                                        <td style="color:#009B93;">Bubblemaker</td>
                                        <td style="color:#B771C4">Curved Spine</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#FFAB38;">Ronin</td>
                                        <td style="color:#FF9BB9;">Cupid</td>
                                        <td style="color:#FF9BB9;">Pigeon Post</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#FFAB38;">Nut Cracker</td>
                                        <td style="color:#FF9BB9;">Peace Maker</td>
                                        <td style="color:#FF5341;">Mosquito</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#FF5341;">Pliers</td>
                                        <td style="color:#FF9BB9;">Eggshell</td>
                                        <td style="color:#FF9BB9;">Kestrel</td>
                                    </tr>
                                    <tr>
                                        <td style="color:#FFAB38;">Nutcracker</td>
                                        <td style="color:#FF5341;">Gravel Ant</td>
                                        <td style="color:#FF5341;">Fish Snack</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="footer-item">
                            <p>Stats</p>
                            <div class="status">
                                <div class="stats">
                                    <img src="{{URL::asset('image/stat_health.svg')}}" />
                                    <p>28</p>
                                </div>
                                <div class="stats">
                                    <img src="{{URL::asset('image/stat_speed.svg')}}" />
                                    <p>64</p>
                                </div>
                                <div class="stats">
                                    <img src="{{URL::asset('image/stat_skill.svg')}}" />
                                    <p>54</p>
                                </div>
                                <div class="stats">
                                    <img src="{{URL::asset('image/stat_morale.svg')}}" />
                                    <p>56</p>
                                </div>
                                <div class="stats">
                                    <p>🍆 0</p>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="content4">
                <div class="header">
                    Log 50 Battles
                </div>
                <div class="content">
                    <table id="myTable">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Opponent</th>
                                <th>Result</th>
                                <th>Replay</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

            <!-- END OVERVIEW -->
        </div>
    </div>
    <div id="snackbar">Copied to clipboard</div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.5.1/chart.min.js" integrity="sha512-Wt1bJGtlnMtGP0dqNFH1xlkLBNpEodaiQ8ZN5JLA5wpc1sUlk/O5uuOMNgvzddzkpvZ9GLyYNa8w2s7rqiTk5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script src="{{ URL::asset('js/plugin-label-datatable.js') }}?v=1.0"></script>
<script>
    var ctx = document.getElementById('myChart1').getContext('2d');
    var ctx2 = document.getElementById('myChart2').getContext('2d');
    var ctx3 = document.getElementById('myChart3').getContext('2d');

    function copyToClipboard() {
        var range = document.createRange();
        range.selectNode(document.getElementById("ronin_address"));
        window.getSelection().removeAllRanges(); // clear current selection
        window.getSelection().addRange(range); // to select text
        document.execCommand("copy");
        window.getSelection().removeAllRanges(); // to deselect

        // Get the snackbar DIV
        var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function() {
            x.className = x.className.replace("show", "");
        }, 3000);
    }

    function loadData() {
        var data_daily = <?php echo $data_daily; ?>;
        var label_bar = [];
        var data_bar = [];
        data_daily.forEach((data) => {
            console.log(data.slp_adventure);
            var total_slp = data.slp_adventure + data.slp_pvp + data.slp_quest;
            label_bar.push(data.date);
            data_bar.push(total_slp);
        });
        console.log(data_bar);
        label_bar.reverse();
        data_bar.reverse();
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: label_bar,
                datasets: [{
                    label: 'Daily Income SLP',
                    data: data_bar,
                    backgroundColor: 'rgba(116,183,189, 0.5)',
                    borderColor: 'rgba(116,183,189, 0.5)',
                    borderWidth: 1,
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                    }
                },
                plugins: {
                    labels: {
                        render: 'value',
                    }
                }
            }
        });

        var log_mmr = <?php echo $data_log_mmr; ?>;
        var label_line = [];
        var data_line = [];
        // for (var i = log_mmr.length - 1; i >= 0; i--) {
        //     console.log("DATE");
        //     console.log(log_mmr[i].date);
        //     console.log("MMR");
        //     console.log(log_mmr[i].mmr);
        //     label_line.push(log_mmr[i].date);
        //     data_line.push(log_mmr[i].mmr);
        // }
        log_mmr.forEach((data) => {
            console.log(data.mmr);
            label_line.push(data.date);
            data_line.push(data.mmr);
        });
        console.log(data_line);
        data_line.reverse();
        label_line.reverse();

        var myChart3 = new Chart(ctx3, {
            type: 'line',
            data: {
                labels: label_line,
                datasets: [{
                    label: 'MMR Tracker',
                    data: data_line,
                    fill: false,
                    borderColor: 'rgb(75, 192, 192)'
                }]
            }
        });
    }

    $(function() {
        loadData();
        loadDashboard();
        loadBattleLog();
        loadAxie();

    });

    var log_pvp;
    var list_axies;
    var type_class;
    var purity = 100;

    function getColor(text, type, gen) {
        var color;
        if (type_class != type) {
            if (gen == 1) {
                purity -= 13;
            } else if (gen == 2) {
                purity -= 3;
            } else {
                purity -= 1;
            }
        }
        if (purity < 0) {
            purity = 0;
        }
        if (type == "beast") {
            color = "#FFAB38";
        } else if (type == "bird") {
            color = "#FF9BB9";
        } else if (type == "plant") {
            color = "#5C9F11";
        } else if (type == "bug") {
            color = "#FF5341";
        } else if (type == "aquatic") {
            color = "#009B93";
        } else if (type == "reptile") {
            color = "#DC8BE4";
        } else {
            color = "gray";
        }

        return '<td style="color:' + color + ';">' + text + '</td>';
    }

    function getColorType(text, type) {
        var color;
        type_class = type.toLowerCase();
        purity = 100;
        if (type == "Aquatic") {
            color = "#009B93";
        } else if (type == "Bird") {
            color = "#FF9BB9";
        } else if (type == "Dawn") {
            color = "#BECEFF";
        } else if (type == "Plant") {
            color = "#5C9F11";
        } else if (type == "Reptile") {
            color = "#DC8BE4";
        } else if (type == "Dusk") {
            color = "#129092";
        } else if (type == "Beast") {
            color = "#FFAB38";
        } else if (type == "Bug") {
            color = "#FF5341";
        } else if (type == "Mecha") {
            color = "#C6BDD4";
        } else {
            color = "#74B7BD";
        }

        return '<p class="tag" style="background-color:' + color + ';">#' + text + '</p>';
    }

    function loadAxie() {
        console.log("load axie");
        var ronin_address = "<?php echo $data_axie->address_ronin; ?>";
        var address = ronin_address.replace("ronin:", "0x");
        var query = '"query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {\\n  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {\\n    total\\n    results {\\n      ...AxieBrief\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\\nfragment AxieBrief on Axie {\\n  id\\n  name\\n  stage\\n  class\\n  breedCount\\n  image\\n  title\\n  battleInfo {\\n    banned\\n    __typename\\n  }\\n  auction {\\n    currentPrice\\n    currentPriceUSD\\n    __typename\\n  }\\n  parts {\\n    id\\n    name\\n    class\\n    type\\n    specialGenes\\n    __typename\\n  }\\n  __typename\\n}\\n"';
        var parameter = '{' +
            '"operationName": "GetAxieBriefList",' +
            '"variables": {' +
            '    "from": 0,' +
            '   "size": 24,' +
            '  "owner": "' + address + '",' +
            ' "criteria": {}' +
            '},' +
            '"query": ' + query + '}';
        $.ajax({
            type: "POST",
            url: "https://graphql-gateway.axieinfinity.com/graphql",
            data: parameter,
            contentType: "application/json",
            success: function(dt) {
                var url_asset = "<?php echo URL::asset(''); ?>";
                list_axies = dt.data.axies.results;
                list_axies.forEach((axies) => {
                    $.ajax({
                        type: "GET",
                        url: "https://api.axie.technology/getgenes/" + axies.id + "/all",
                        success: function(d) {
                            if ($("#name_profile").html() == "-") {
                                $("#name_profile").html(d.ownerProfile.name);
                            }
                            var new_card = ' <div class="item">' +
                                '<div class="header-item">' +
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + axies.id + '/axie/axie-full-transparent.png" />' +
                                '<a target="_blank" href="https://marketplace.axieinfinity.com/axie/' + axies.id + '"><h5>' + d.name + '</h5></a>' +
                                getColorType(axies.id, d.class) +
                                '</div>' +
                                '<div class="desc-item">' +
                                '<table>' +
                                '<thead>' +
                                '<tr>' +
                                '<th>D</th>' +
                                '<th>R1</th>' +
                                '<th>R2</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody>' +
                                '<tr>' +
                                getColor(d.traits.eyes.d.name, d.traits.eyes.d.class, 1) +
                                getColor(d.traits.eyes.r1.name, d.traits.eyes.r1.class, 2) +
                                getColor(d.traits.eyes.r2.name, d.traits.eyes.r2.class, 3) +
                                '</tr>' +
                                '<tr>' +
                                getColor(d.traits.ears.d.name, d.traits.ears.d.class, 1) +
                                getColor(d.traits.ears.r1.name, d.traits.ears.r1.class, 2) +
                                getColor(d.traits.ears.r2.name, d.traits.ears.r2.class, 3) +
                                '</tr>' +
                                '<tr>' +
                                getColor(d.traits.mouth.d.name, d.traits.mouth.d.class, 1) +
                                getColor(d.traits.mouth.r1.name, d.traits.mouth.r1.class, 2) +
                                getColor(d.traits.mouth.r2.name, d.traits.mouth.r2.class, 3) +
                                '</tr>' +
                                '<tr>' +
                                getColor(d.traits.back.d.name, d.traits.back.d.class, 1) +
                                getColor(d.traits.back.r1.name, d.traits.back.r1.class, 2) +
                                getColor(d.traits.back.r2.name, d.traits.back.r2.class, 3) +
                                '</tr>' +
                                '<tr>' +
                                getColor(d.traits.horn.d.name, d.traits.horn.d.class, 1) +
                                getColor(d.traits.horn.r1.name, d.traits.horn.r1.class, 2) +
                                getColor(d.traits.horn.r2.name, d.traits.horn.r2.class, 3) +
                                '</tr>' +
                                '<tr>' +
                                getColor(d.traits.tail.d.name, d.traits.tail.d.class, 1) +
                                getColor(d.traits.tail.r1.name, d.traits.tail.r1.class, 2) +
                                getColor(d.traits.tail.r2.name, d.traits.tail.r2.class, 3) +
                                '</tr>' +
                                '</tbody>' +
                                '</table>' +
                                '</div>' +
                                '<div class="footer-item">' +
                                '<p class="sub">Gene Quality</p>' +
                                '<p>' + purity + '%</p>' +
                                '<p class="sub">Stats</p>' +
                                '<div class="status">' +
                                '<div class="stats">' +
                                '<img src="' + url_asset + 'image/stat_health.svg" />' +
                                '<p>' + d.stats.hp + '</p>' +
                                '</div>' +
                                '<div class="stats">' +
                                '<img src="' + url_asset + 'image/stat_speed.svg" />' +
                                '<p>' + d.stats.speed + '</p>' +
                                '</div>' +
                                '<div class="stats">' +
                                '<img src="' + url_asset + 'image/stat_skill.svg" />' +
                                '<p>' + d.stats.skill + '</p>' +
                                '</div>' +
                                '<div class="stats">' +
                                '<img src="' + url_asset + 'image/stat_morale.svg" />' +
                                '<p>' + d.stats.morale + '</p>' +
                                '</div>' +
                                '<div class="stats">' +
                                '<p>🍆 ' + d.breedCount + '</p>' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            $("#content_axie").append(new_card);
                        },
                        error: function(err) {
                            console.log(err);
                        }
                    });
                });
            },
            error: function(err) {
                //COBA API KE 2
                console.log(err);
            }
        });
    }

    function loadBattleLog() {
        var ronin_address = "<?php echo $data_axie->address_ronin; ?>";
        var address = ronin_address.replace("ronin:", "0x");
        var win = 0;
        var lose = 0;
        var draw = 0;
        var total_play_today = 0;
        $.ajax({
            type: "GET",
            url: "https://olahproxy.herokuapp.com/https://api.lunaciaproxy.cloud/_battles/" + address + "/50/0",
            // url: "https://api.allorigins.win/raw?url=https://api.lunaciaproxy.cloud/_battles/" + address + "/50/0",
            success: function(dt) {
                log_pvp = dt.battle_logs.pvp;
                var t = $("#myTable").DataTable({
                    "order": [
                        [1, "desc"]
                    ]
                });
                t.clear().draw(false);
                log_pvp.forEach((log) => {
                    var first_player = log.first_client_id;
                    var second_player = log.second_client_id;
                    var winner = log.winner;

                    var now = Date.now();
                    var time = new Date(log.created_at);
                    var totalSeconds = (now - time) / 1000;

                    hours = Math.floor(totalSeconds / 3600)
                    // minutes = Math.floor((totalSeconds % 3600) / 60)
                    // seconds = Math.floor((totalSeconds % 3600) % 60)
                    // console.log(hours, minutes, seconds)

                    log.created_at = hours + ' hours ago';

                    if (hours >= 24) {
                        days = 0;
                        while (hours >= 24) {
                            days++;
                            hours -= 24;
                        }
                        log.created_at = days + " days " + hours + " hours ago";
                    } else {
                        total_play_today++;
                    }

                    if (first_player == address) {
                        //PLAYER 1 adalah Player
                        if (winner == 0) {
                            win++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[3].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[4].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[5].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:green;font-weight:bold;'>WIN</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        } else if (winner == 1) {
                            lose++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[3].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[4].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[5].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:red;font-weight:bold;'>LOSE</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        } else {
                            draw++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[3].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[4].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[5].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:gray;font-weight:bold;'>DRAW</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        }
                    } else {
                        if (winner == 1) {
                            win++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[0].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[1].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[2].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:green;font-weight:bold;'>WIN</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        } else if (winner == 0) {
                            lose++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[0].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[1].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[2].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:red;font-weight:bold;'>LOSE</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        } else {
                            draw++;
                            t.row.add([
                                log.created_at,
                                '<img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[0].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[1].fighter_id + '/axie/axie-full-transparent.png" /><img src="https://storage.googleapis.com/assets.axieinfinity.com/axies/' + log.fighters[2].fighter_id + '/axie/axie-full-transparent.png" />',
                                "<div style='color:gray;font-weight:bold;'>DRAW</div>",
                                "<a href='axie://?f=rpl&q=" + log.battle_uuid + "'><div class='btn btn-primary'>Replay</div></a>"
                            ]).draw(false);
                        }
                    }
                });

                $("#total_pvp").html(total_play_today);

                win_rate = (win / (win + lose + draw) * 100).toFixed(0);
                $("#win_rate").html(win_rate + "%");

                var myChart2 = new Chart(ctx2, {
                    type: 'pie',
                    data: {
                        labels: [
                            'PVP Win',
                            'PVP Lose',
                            'PVP Draw'
                        ],
                        datasets: [{
                            data: [win, lose, draw],
                            backgroundColor: [
                                'rgb(116,183,189)',
                                'rgb(25,19,38)',
                                'rgb(255,178,55)'
                            ],
                            hoverOffset: 4
                        }]
                    },
                    options: {
                        plugins: {
                            labels: {
                                render: 'percentage',
                                fontColor: '#fff',
                            }
                        }
                    }
                });
            },
            error: function(err) {
                //COBA API KE 2
                console.log(err);
            }
        });
    }

    function loadDashboard() {
        var ronin_address = "<?php echo $data_axie->address_ronin; ?>";
        var address = ronin_address.replace("ronin:", "0x");
        $.ajax({
            type: "GET",
            url: "https://olahproxy.herokuapp.com/https://axie-scho-tracker-server.herokuapp.com/api/account/" + ronin_address,
            // url: "https://api.allorigins.win/raw?url=https://axie-scho-tracker-server.herokuapp.com/api/account/" + ronin_address,
            success: function(dt) {
                var date = new Date(dt.slpData.lastClaim * 1000);
                date = moment(dt.slpData.lastClaim * 1000).format("lll");
                $("#last_claim").html(date);
                $("#unclaimed_slp").html(dt.slpData.gameSlp);
                $("#claimed_slp").html(dt.slpData.roninSlp);
                $("#total_slp").html(dt.slpData.totalSlp);
                $("#elo_pvp").html(dt.leaderboardData.elo);
                $("#rank_pvp").html(dt.leaderboardData.rank);
            },
            error: function(err) {
                //COBA API KE 2
                console.log("coba api 2");
                address = address.replace("ronin:", "0x");
                $.ajax({
                    type: "GET",
                    // crossDomain: true,
                    // headers: {
                    //     'Access-Control-Allow-Origin': 'http://207.148.74.112/'
                    // },
                    // url: "https://axiesworld.firebaseapp.com/updateSpecific?wallet=" + address,
                    // url: "https://api.allorigins.win/raw?url=https://axiesworld.firebaseapp.com/updateSpecific?wallet=" + address,
                    url: "https://olahproxy.herokuapp.com/https://game-api.axie.technology/api/v1/" + address,
                    success: function(dt) {
                        var date = new Date(dt.last_claim * 1000);
                        date = moment(dt.last_claim * 1000).format("lll");
                        $("#last_claim").html(date);
                        $("#unclaimed_slp").html(dt.in_game_slp);
                        $("#claimed_slp").html(dt.ronin_slp);
                        $("#total_slp").html(dt.total_slp);
                        $("#elo_pvp").html(dt.mmr);
                        $("#rank_pvp").html(dt.rank);
                    },
                    error: function(err) {
                        console.log("api 2 failed");
                    }
                });
            }
        });
    }
</script>