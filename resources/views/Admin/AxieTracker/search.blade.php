@include('Admin.header',['activePage' => 'search_axie_tracker'])
<style>
    .p-number {
        color: #E05227;
        font-weight: bold;
        font-size: 18px;
    }

    .icon.ic2 {
        margin-top: 5px;
    }

    #myTable4 {
        overflow: auto;
    }

    /* The snackbar - position it at the bottom and in the middle of the screen */
    #snackbar {
        visibility: hidden;
        /* Hidden by default. Visible on click */
        min-width: 250px;
        /* Set a default minimum width */
        margin-left: -125px;
        /* Divide value of min-width by 2 */
        background-color: #333;
        /* Black background color */
        color: #fff;
        /* White text color */
        text-align: center;
        /* Centered text */
        border-radius: 2px;
        /* Rounded borders */
        padding: 16px;
        /* Padding */
        position: fixed;
        /* Sit on top of the screen */
        z-index: 1;
        /* Add a z-index if needed */
        left: 50%;
        /* Center the snackbar */
        bottom: 30px;
        /* 30px from the bottom */
    }

    /* Show the snackbar when clicking on a button (class added with JavaScript) */
    #snackbar.show {
        visibility: visible;
        /* Show the snackbar */
        /* Add animation: Take 0.5 seconds to fade in and out the snackbar.
However, delay the fade out process for 2.5 seconds */
        -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
        animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    /* Animations to fade the snackbar in and out */
    @-webkit-keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @keyframes fadein {
        from {
            bottom: 0;
            opacity: 0;
        }

        to {
            bottom: 30px;
            opacity: 1;
        }
    }

    @-webkit-keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }

        to {
            bottom: 0;
            opacity: 0;
        }
    }

    @keyframes fadeout {
        from {
            bottom: 30px;
            opacity: 1;
        }

        to {
            bottom: 0;
            opacity: 0;
        }
    }

</style>
<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<link rel="stylesheet" href="{{ URL::asset('css/home.css') }}">
<!-- MAIN -->
<div class="main">
    <div class="loading-overlay" id="loading-overlay">
        <i class="fa fa-spinner fa-spin spin-big"></i>
        <h5>Please Wait ...</h5>
    </div>
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <!-- OVERVIEW -->

            <div class="panel panel-headline">
                <div class="panel-heading">
                    <h3 class="panel-title" style="margin-bottom:25px;">Axie Tracker</h3>
                </div>
                <div class="panel-body">

                    <div class="row" style="overflow:auto;">
                        <h3 class="panel-title" style="text-align:center;margin-bottom:25px;font-size:20px;">Search
                            Axie Tracker</h3>
                        <div class="form-group" style="display:{{ $dataUser->role != 2 ? 'none' : 'block' }}">
                            <label>Filter by owner</label>
                            <select id="selectOwner" class="form-control input-lg" onchange="search()">
                                <option value="-1">ALL</option>
                                @foreach ($dataOwner as $dt)
                                    <option value="{{ $dt->id }}">
                                        {{ $dt->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Search by Player / Email Axie</label>
                            <input placeholder="Search here" type="text" id="searchbar" class="form-control input-lg"
                                onkeyup="searchChange()">
                        </div>
                        <table id="myTable4" class="table table-bordered display">
                            <thead>
                                <tr>
                                    <th>Email Akun</th>
                                    <th>Player</th>
                                    <th>Claimed SLP</th>
                                    <th>Unclaimed SLP</th>
                                    <th>Total SLP</th>
                                    <th>Today SLP</th>
                                    <th>Inventory</th>
                                    <th>MMR</th>
                                    <th>Ronin Address</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- END OVERVIEW -->
        </div>
    </div>
    <!-- END MAIN CONTENT -->
    <div id="snackbar">Copied to clipboard</div>
</div>
<!-- END MAIN -->

@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script>
    function debounce(func, timeout = 1000) {
        let timer;
        return (...args) => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                func.apply(this, args);
            }, timeout);
        };
    }

    const searchChange = debounce(() => search());

    $(function() {
        $("#myTable4").DataTable({
            "order": [
                [0, "asc"]
            ],
            "searching": false
        });
        // loadAxieTracker();
    });


    function copyToClipboard(id) {
        var range = document.createRange();
        range.selectNode(document.getElementById("ronin_address" + id));
        window.getSelection().removeAllRanges(); // clear current selection
        window.getSelection().addRange(range); // to select text
        document.execCommand("copy");
        window.getSelection().removeAllRanges(); // to deselect

        // Get the snackbar DIV
        var x = document.getElementById("snackbar");

        // Add the "show" class to DIV
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function() {
            x.className = x.className.replace("show", "");
        }, 3000);
    }

    function search() {
        $("#loading-overlay").css("visibility", "visible");
        var searchVal = $("#searchbar").val();
        var id_owner = $("#selectOwner").val();
        // console.log("Search " + searchVal);

        $.ajax({
            type: "GET",
            url: "/api/axie/search?search=" + searchVal + "&owner=" + id_owner,
            success: function(d) {
                console.log(d);
                console.log(d.payload);
                var t = $("#myTable4").DataTable();
                t.clear().draw(false);
                var index = 1;
                console.log(d.payload.length);
                d.payload.forEach(data => {
                    if (data.address_ronin != "" && data.address_ronin != null) {
                        var address = data.address_ronin;
                        console.log(data.address_ronin);
                        $.ajax({
                            type: "GET",
                            url: "https://olahproxy.herokuapp.com/https://axie-scho-tracker-server.herokuapp.com/api/account/" +
                                address,
                            // url: "https://api.allorigins.win/raw?url=https://axie-scho-tracker-server.herokuapp.com/api/account/" + address,
                            // url: "https://api.lunaciarover.com/stats/" + address,
                            success: function(dt) {
                                console.log(index);
                                if (index >= d.payload.length) {
                                    $("#loading-overlay").css("visibility", "hidden");
                                } else {
                                    index++;
                                }
                                // console.log(dt);
                                // var today_slp = dt.in_game_slp - data.yesterday_slp;
                                // var date = new Date(dt.last_claim_timestamp * 1000);
                                // t.row.add([
                                //     data.email_akun,
                                //     data.name,
                                //     dt.ronin_slp,
                                //     dt.in_game_slp,
                                //     dt.total_slp,
                                //     today_slp,
                                //     data.total_slp,
                                //     dt.mmr,
                                //     data.address_ronin,
                                //     date,
                                // ]).draw(false);
                                var today_slp = dt.slpData.gameSlp - data.yesterday_slp;
                                var date = new Date(dt.slpData.lastClaim * 1000);
                                t.row.add([
                                    data.email_akun,
                                    data.name,
                                    dt.slpData.roninSlp,
                                    dt.slpData.gameSlp,
                                    dt.slpData.totalSlp,
                                    today_slp,
                                    data.total_slp,
                                    dt.leaderboardData.elo,
                                    '<div style="display:flex"><div id="ronin_address' +
                                    data.id + '">' + data.address_ronin +
                                    '</div><i class="fa fa-copy" style="cursor:pointer;margin-left:5px;" onclick="copyToClipboard(' +
                                    data.id + ')"></i></div>',
                                    "<a href='/admin/detail_tracker/" + data
                                    .id +
                                    "'><div class='btn btn-primary'>Detail</div></a>",
                                ]).draw(false);
                            },
                            error: function(err) {
                                //COBA API KE 2
                                console.log("coba api 2");
                                address = address.replace("ronin:", "0x");
                                $.ajax({
                                    type: "GET",
                                    // crossDomain: true,
                                    // headers: {
                                    //     'Access-Control-Allow-Origin': 'http://207.148.74.112/'
                                    // },
                                    // url: "https://axiesworld.firebaseapp.com/updateSpecific?wallet=" + address,
                                    // url: "https://api.allorigins.win/raw?url=https://axiesworld.firebaseapp.com/updateSpecific?wallet=" + address,
                                    url: "https://olahproxy.herokuapp.com/https://game-api.axie.technology/api/v1/" +
                                        address,
                                    success: function(dt) {
                                        console.log(dt);
                                        var today_slp = dt.total_slp - data
                                            .yesterday_slp;
                                        var date = new Date(dt.last_claim *
                                            1000);
                                        t.row.add([
                                            data.email_akun,
                                            data.name,
                                            dt.ronin_slp,
                                            dt.in_game_slp,
                                            dt.total_slp,
                                            today_slp,
                                            data.total_slp,
                                            dt.mmr,
                                            data.address_ronin,
                                            "<a href='/admin/detail_tracker/" +
                                            data.id +
                                            "'><div class='btn btn-primary'>Detail</div></a>",
                                        ]).draw(false);
                                    },
                                    error: function(err) {
                                        console.log("api 2 failed");
                                    }
                                });
                            }
                        });
                    }
                });
            },
        });
        $("#loading-overlay").css("visibility", "hidden");

    }

    // function searchChange() {
    //     console.log("sear");
    //     debounce(search, 1000);
    // }

    function loadAxieTracker() {

    }
</script>
