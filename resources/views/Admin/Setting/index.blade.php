@include('Admin.header',['activePage' => 'master_setting'])
<style>
    #ready {
        display: none;
    }

    #group_text {
        display: none;
    }

    #group_alasan {
        display: none;
    }
</style>
<link rel="stylesheet" href="{{URL::asset('css/home.css')}}">
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->

        @if($dataUser->role == 2)
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting Superadmin</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/setting/update_general')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Setting Name</label>
                        <select class="form-control input-lg" name="id_setting" id="id_setting_superadmin" onchange="settingSuperadminChange()">
                            @foreach($data_setting_superadmin as $dt)
                            <option value="{{$dt->id}}">{{$dt->nama_setting}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group" id="group_text">
                        <label>Value</label>
                        <input type="number" id="input_value_superadmin" name="value" class="form-control input-lg" />
                    </div>
                    <div class="form-group" id="group_select">
                        <label style="font-size:10px;font-weight:bold;color:red;">1 : STATUS SERVER NORMAL, 0 : STATUS SERVER ERROR</label>
                    </div>
                    <div class="form-group" id="group_alasan">
                        <label>ALASAN ERROR</label>
                        <input type="text" class="form-control input-lg" name="alasan" />
                    </div>
                    <div style="text-align:center">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
        @endif

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting General</h3>
            </div>
            <div class="panel-body">
                <form action="{{URL('/admin/setting/update_general')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>User</label>
                        <select class="form-control input-lg" name="id_user" id="id_user" onchange="userChange()">
                            @foreach($data_user as $dt)
                            <option value="{{$dt->id}}">{{$dt->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div id="ready">
                        <div class="form-group">
                            <label>Setting Name</label>
                            <select class="form-control input-lg" name="id_setting" id="id_setting" onchange="settingGeneralChange()">

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Value</label>
                            <input class="form-control input-lg" placeholder="Value" step="any" type="number" name="value" id="value_general">
                        </div>
                        <div style="text-align:center">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </div>
                    <div id="loading" style="text-align:center;">
                        <div class="lds-roller" style="margin:auto;">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>


        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting Minimal SLP Adventure</h3>
                <br>
                <div class="btn btn-primary" onclick="openModalAdd()">Add</div>
            </div>
            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Min Level</th>
                            <th>Max Level</th>
                            <th>Min SLP</th>
                            <th>Gaji</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
            </div>
        </div>



        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Setting Minimal SLP PVP</h3>
                <br>
                <div class="btn btn-primary" onclick="openModalPVP()">Add</div>
            </div>
            <div class="panel-body">
                <table id="myTable2" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>Min SLP</th>
                            <th>Max SLP</th>
                            <th>Gaji</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="table-body">
                    </tbody>
                </table>
            </div>
        </div>

        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>

<div id="modalAdd" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Setting Level</h4>
            </div>
            <div class="modal-body">
                <form action="{{URL('/admin/master_setting/do_add_setting')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id_user" id="add_id_user" />
                    <div class="form-group">
                        <label>Min Level</label>
                        <input type="number" class="form-control input-lg" name="min_level" />
                    </div>
                    <div class="form-group">
                        <label>Max Level</label>
                        <input type="number" class="form-control input-lg" name="max_level" />
                    </div>
                    <div class="form-group">
                        <label>Min SLP</label>
                        <input type="number" class="form-control input-lg" name="min_slp" />
                    </div>
                    <div class="form-group">
                        <label>Gaji</label>
                        <input type="number" class="form-control input-lg" name="value" />
                    </div>

                    <input type="submit" class="btn btn-primary" value="Add">
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalEdit" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Setting Level</h4>
            </div>
            <div class="modal-body">
                <form action="{{URL('/admin/master_setting/do_edit_setting')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="edit_id" />
                    <div class="form-group">
                        <label>Min Level</label>
                        <input type="number" class="form-control input-lg" name="min_level" id="edit_min_level" />
                    </div>
                    <div class="form-group">
                        <label>Max Level</label>
                        <input type="number" class="form-control input-lg" name="max_level" id="edit_max_level" />
                    </div>
                    <div class="form-group">
                        <label>Min SLP</label>
                        <input type="number" class="form-control input-lg" name="min_slp" id="edit_min_slp" />
                    </div>
                    <div class="form-group">
                        <label>Gaji</label>
                        <input type="number" class="form-control input-lg" name="value" id="edit_gaji" />
                    </div>

                    <input type="submit" class="btn btn-primary" value="Edit">
                </form>
            </div>
        </div>

    </div>
</div>


<div id="modalAddPVP" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Setting PVP</h4>
            </div>
            <div class="modal-body">
                <form action="{{URL('/admin/master_setting/do_add_setting_pvp')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id_user" id="add_id_user_pvp" />
                    <div class="form-group">
                        <label>Min SLP</label>
                        <input type="number" class="form-control input-lg" name="min_slp" />
                    </div>
                    <div class="form-group">
                        <label>Max SLP</label>
                        <input type="number" class="form-control input-lg" name="max_slp" />
                    </div>
                    <div class="form-group">
                        <label>Gaji</label>
                        <input type="number" class="form-control input-lg" name="value" />
                    </div>

                    <input type="submit" class="btn btn-primary" value="Add">
                </form>
            </div>
        </div>
    </div>
</div>

<div id="modalEditPVP" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Setting PVP</h4>
            </div>
            <div class="modal-body">
                <form action="{{URL('/admin/master_setting/do_edit_setting_pvp')}}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="edit_id_pvp" />
                    <div class="form-group">
                        <label>Min SLP</label>
                        <input type="number" class="form-control input-lg" name="min_slp" id="edit_min_slp_pvp" />
                    </div>
                    <div class="form-group">
                        <label>Max SLP</label>
                        <input type="number" class="form-control input-lg" name="max_slp" id="edit_max_slp_pvp" />
                    </div>
                    <div class="form-group">
                        <label>Gaji</label>
                        <input type="number" class="form-control input-lg" name="value" id="edit_value_pvp" />
                    </div>

                    <input type="submit" class="btn btn-primary" value="Edit">
                </form>
            </div>
        </div>

    </div>
</div>
<!-- END MAIN -->
@include('Admin.footer')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    var data_setting;
    var data_setting_superadmin;

    function settingSuperadminChange() {
        var id_setting = $("#id_setting_superadmin").val();
        // console.log(data_setting_superadmin[$("#id_setting_superadmin")[0].selectedIndex].value);
        var val = data_setting_superadmin[$("#id_setting_superadmin")[0].selectedIndex].value;
        // console.log(val);
        console.log($("#id_setting_superadmin option:selected").text());

        if ($("#id_setting_superadmin option:selected").text() == "SETTING_SERVER_AXIE") {
            $("#group_text").css("display", "block");
            $("#group_select").css("display", "block");
            $("#group_alasan").css("display", "block");
        } else {
            $("#group_text").css("display", "block");
            $("#group_select").css("display", "none");
            $("#group_alasan").css("display", "none");
        }
        var input_value = $("#input_value_superadmin");
        input_value.val(parseInt(val));
    }

    function openModalAdd() {
        $("#modalAdd").modal();
    }

    function openModalPVP() {
        $("#modalAddPVP").modal();
    }

    function openModalEdit(index) {
        $("#edit_id").val(data_setting_level[index].id);
        $("#edit_min_level").val(data_setting_level[index].min_level);
        $("#edit_max_level").val(data_setting_level[index].max_level);
        $("#edit_min_slp").val(data_setting_level[index].min_slp);
        $("#edit_gaji").val(data_setting_level[index].value);
        $("#modalEdit").modal();
    }

    function openModalEditPVP(index) {
        $("#edit_id_pvp").val(data_setting_pvp[index].id);
        $("#edit_min_slp_pvp").val(data_setting_pvp[index].min_slp);
        $("#edit_max_slp_pvp").val(data_setting_pvp[index].max_slp);
        $("#edit_value_pvp").val(data_setting_pvp[index].value);
        $("#modalEditPVP").modal();
    }

    function settingGeneralChange() {
        // console.log(data_setting[$("#id_setting")[0].selectedIndex].value);
        $("#value_general").val(data_setting[$("#id_setting")[0].selectedIndex].value);
    }

    var data_setting_level;

    function loadSetttingLevel() {
        var id_user = $("#id_user").val();

        $.ajax({
            type: "GET",
            url: "/api/get_level_setting?id_user=" + id_user,
            cache: false,
            success: function(data) {
                console.log(data.payload);
                data_setting_level = data.payload;
                var t = $("#myTable").DataTable();
                t.clear();
                data.payload.forEach(function(item, index) {
                    t.row.add([
                        item.min_level,
                        item.max_level,
                        item.min_slp,
                        item.value,
                        "<td><div class='btn btn-primary' onclick='openModalEdit(" + index + ")'><i class='fa fa-edit'></i></div> <div style='border-radius:15px;' class='btn btn-danger' onclick='ondelete(" + index + ")'><i class='fa fa-trash'></i></div><td>"
                    ]).draw(false);
                });
            }
        });
    }

    function ondelete(index) {
        var myTable = $('#myTable').DataTable();

        swal({
                title: "Are you sure?",
                text: "Data yang telah dihapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    console.log(data_setting_level[index].id);
                    var formData = new FormData();
                    formData.append("_token", '<?php echo csrf_token(); ?>');
                    formData.append("id", data_setting_level[index].id);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        url: "/api/delete_setting_level",
                        cache: false,
                        success: function(data) {
                            loadSetttingLevel();
                            swal("Sukses", "Data berhasil dihapus", "success");
                        },
                        fail: function(data) {
                            swal("Failed", "Oops Something Wrong", "error");
                        }
                    });
                }
            });
    }

    var data_setting_pvp;

    function loadSettingPVP() {
        var id_user = $("#id_user").val();

        $.ajax({
            type: "GET",
            url: "/api/get_pvp_setting?id_user=" + id_user,
            cache: false,
            success: function(data) {
                console.log(data.payload);
                data_setting_pvp = data.payload;
                var t = $("#myTable2").DataTable();
                t.clear();
                data.payload.forEach(function(item, index) {
                    t.row.add([
                        item.min_slp,
                        item.max_slp,
                        item.value,
                        "<td><div class='btn btn-primary' onclick='openModalEditPVP(" + index + ")'><i class='fa fa-edit'></i></div> <div style='border-radius:15px;' class='btn btn-danger' onclick='ondeletePVP(" + index + ")'><i class='fa fa-trash'></i></div><td>"
                    ]).draw(false);
                });
            }
        });
    }

    function ondeletePVP(index) {
        var myTable = $('#myTable2').DataTable();

        swal({
                title: "Are you sure?",
                text: "Data yang telah dihapus tidak dapat dikembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((willDelete) => {
                if (willDelete) {
                    console.log(data_setting_pvp[index].id);
                    var formData = new FormData();
                    formData.append("_token", '<?php echo csrf_token(); ?>');
                    formData.append("id", data_setting_pvp[index].id);
                    $.ajax({
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        url: "/api/delete_setting_pvp",
                        cache: false,
                        success: function(data) {
                            loadSettingPVP();
                            swal("Sukses", "Data berhasil dihapus", "success");
                        },
                        fail: function(data) {
                            swal("Failed", "Oops Something Wrong", "error");
                        }
                    });
                }
            });
    }

    function userChange() {
        $("#ready").css("display", "none");
        var id_user = $("#id_user").val();
        $("#add_id_user").val(id_user);
        $("#add_id_user_pvp").val(id_user);
        console.log(id_user);
        $.ajax({
            type: "GET",
            url: "/api/get_general_setting?id_user=" + id_user,
            cache: false,
            success: function(data) {
                $("#id_setting").find('option').remove();
                console.log(data.payload);
                data_setting = data.payload;
                data_setting.forEach(function(item, index) {
                    $("#id_setting").append(new Option(item.nama_setting, item.id));
                });
                settingGeneralChange();
                loadSetttingLevel();
                loadSettingPVP();
                $("#ready").css("display", "block");
                $("#loading").css("display", "none");
            }
        });
    }

    $(document).ready(function() {
        data_setting_superadmin = <?php echo json_encode($data_setting_superadmin); ?>;
        userChange();
        settingSuperadminChange();
        $('#myTable').DataTable({
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<style>
    .lds-roller {
        display: inline-block;
        position: relative;
        width: 80px;
        height: 80px;
    }

    .lds-roller div {
        animation: lds-roller 1.2s cubic-bezier(0.5, 0, 0.5, 1) infinite;
        transform-origin: 40px 40px;
    }

    .lds-roller div:after {
        content: " ";
        display: block;
        position: absolute;
        width: 7px;
        height: 7px;
        border-radius: 50%;
        background: #8DD7DC;
        margin: -4px 0 0 -4px;
    }

    .lds-roller div:nth-child(1) {
        animation-delay: -0.036s;
    }

    .lds-roller div:nth-child(1):after {
        top: 63px;
        left: 63px;
    }

    .lds-roller div:nth-child(2) {
        animation-delay: -0.072s;
    }

    .lds-roller div:nth-child(2):after {
        top: 68px;
        left: 56px;
    }

    .lds-roller div:nth-child(3) {
        animation-delay: -0.108s;
    }

    .lds-roller div:nth-child(3):after {
        top: 71px;
        left: 48px;
    }

    .lds-roller div:nth-child(4) {
        animation-delay: -0.144s;
    }

    .lds-roller div:nth-child(4):after {
        top: 72px;
        left: 40px;
    }

    .lds-roller div:nth-child(5) {
        animation-delay: -0.18s;
    }

    .lds-roller div:nth-child(5):after {
        top: 71px;
        left: 32px;
    }

    .lds-roller div:nth-child(6) {
        animation-delay: -0.216s;
    }

    .lds-roller div:nth-child(6):after {
        top: 68px;
        left: 24px;
    }

    .lds-roller div:nth-child(7) {
        animation-delay: -0.252s;
    }

    .lds-roller div:nth-child(7):after {
        top: 63px;
        left: 17px;
    }

    .lds-roller div:nth-child(8) {
        animation-delay: -0.288s;
    }

    .lds-roller div:nth-child(8):after {
        top: 56px;
        left: 12px;
    }

    @keyframes lds-roller {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>