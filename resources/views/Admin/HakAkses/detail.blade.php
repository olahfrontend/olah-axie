@include('Admin.header',['activePage' => 'master_role'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .btn {
        border-radius: 15px;
    }

    .form-check-container {
        margin: 15px 0px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Role</h3>
            </div>

            <div class="panel-body">

                <form action="{{URL('/admin/hak_akses/do_edit')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Nama Role</label><br>
                        <input value="{{$data_role->nama_role}}" type="text" name="nama_role" class="form-control input-lg" />
                    </div>
                    <div class="form-group">
                        <label>Jabatan</label>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="jabatan" id="flexRadioDefault1" value="1" {{$data_role->jabatan == 1 ? 'checked' : ''}}>
                            <label class="form-check-label" for="flexRadioDefault1">
                                PEGAWAI
                            </label>
                            <input class="form-check-input" style="margin-left:15px;" type="radio" name="jabatan" id="flexRadioDefault2" value="2" {{$data_role->jabatan == 2 ? 'checked' : ''}}>
                            <label class="form-check-label" for="flexRadioDefault2">
                                OWNER
                            </label>
                        </div>
                    </div>
                    <h5 style="font-weight:bold; color:#74B7BD;font-size:16px;margin-bottom:0px;">Hak Akses</h5>
                    <input type="hidden" name="id_role" value="{{$data_role->id}}" />
                    <div class="row form-check-container">
                        @foreach($data_menu as $index => $dt)
                        @if($dt->have_submenu == 0)
                        <div class="col-md-4">
                            <?php $exists = false; ?>
                            @foreach($data_role->data_hak_akses as $d)
                            @if($d->id == $dt->id)
                            <?php $exists = true; ?>
                            <input class="form-check-input" type="checkbox" value="{{$dt->id}}" name="checkbox[]" checked>
                            @endif
                            @endforeach
                            @if(!$exists)
                            <input class="form-check-input" type="checkbox" value="{{$dt->id}}" name="checkbox[]">
                            @endif
                            <label class="form-check-label" for="{{$dt->id}}">
                                {{$dt->nama_menu}}
                            </label>
                        </div>
                        @endif
                        @endforeach
                    </div>
            </div>
            <input type="submit" class="btn btn-primary" style="margin:15px 25px;" value="Edit" />
            </form>

        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "asc"]
            ]
        });
    });

    function submitForm() {

    }
</script>