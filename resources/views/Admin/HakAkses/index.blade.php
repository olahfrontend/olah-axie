@include('Admin.header',['activePage' => 'master_role'])

<link rel="stylesheet" href="{{ URL::asset('css/wallet.css') }}">
<!-- MAIN -->
<style>
    .btn {
        border-radius: 15px;
    }
</style>
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">

        <!-- BORDERED TABLE -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">List Role</h3>
                <a href="/admin/hak_akses/add">
                    <div class="btn btn-primary" style="margin-top:25px;">Tambah Role</div>
                </a>
            </div>

            <div class="panel-body">
                <table id="myTable" class="table table-bordered display">
                    <thead>
                        <tr>
                            <th>ID Role</th>
                            <th>Nama Role</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_role as $dt)
                        <tr>
                            <td>{{ $dt->id }}</td>
                            <td>{{ $dt->nama_role }}</td>
                            <td>
                                <a href="{{ URL('admin/hak_akses/') . '/' . $dt->id }}">
                                    <div class="btn btn-primary">
                                        <i class="fa fa-edit"></i>
                                    </div>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END BORDERED TABLE -->
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->

@include('Admin.footer')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function() {
        $('#myTable').DataTable({
            "order": [
                [0, "asc"]
            ]
        });
    });
</script>