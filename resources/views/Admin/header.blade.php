<!doctype html>
<html lang="en">

<head>
    <title>Olah Axie</title>
    <link rel="icon" href="{{URL::asset('image/logo.png')}}" type="image/x-icon">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/linearicons/style.css')}}">
    <link rel="stylesheet" href="{{URL::asset('assets/vendor/chartist/css/chartist-custom.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>

    <link href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" rel="stylesheet">



    <!-- MAIN CSS -->
    <link rel="stylesheet" href="{{URL::asset('assets/css/main.css')}}">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="{{URL::asset('assets/css/demo.css')}}">
    <link rel="stylesheet" href="{{URL::asset('css/navbar.css')}}">
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <!-- ICONS -->
    <link rel="apple-touch-icon" sizes="76x76" href="{{URL::asset('assets/img/apple-icon.png')}}">
    <style>
        .select2-container--default .select2-selection--single {
            height: 46px;
            padding: 9px 12px;
            font-size: 18px;
            line-height: 1.3333333;
        }

        .select2-selection__arrow {
            top: 10px !important;
            right: 10px !important;

        }

        .btn {
            border-radius: 15px;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        @include("Admin.navbar")