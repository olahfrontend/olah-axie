<?php

namespace App\Console\Commands;

use App\Helpers\CommonHelper;
use App\Models\DetailJadwalCuti;
use App\Models\JadwalCuti;
use App\Models\LogMMR;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\UserAxie;
use Error;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class AxieSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'AxieSyncCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Sync Axie';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //CREATE LOG SERVER
        $log = new LogServer();
        $log->description = "AXIE_SYNC_COMMAND_START";
        $log->save();

        $today = Carbon::now()->subDays(1);
        $now = Carbon::now()->subHour();
        //CRONJOB UNTUK SYNC INVENTORY AXIE DARI API AXIE LUAR
        $data_axie = MasterAxie::where("address_ronin", "<>", "")->get();
        foreach ($data_axie as $dt) {
            error_log($dt->address_ronin);
            if ($dt->address_ronin != "") {
                // error_log($dt->address_ronin);
                $ronin_address = $dt->address_ronin;
                $address = str_replace("ronin:", "0x", $ronin_address);
                $data = CommonHelper::getAPIExternal("https://olahproxy.herokuapp.com/https://axie-scho-tracker-server.herokuapp.com/api/account/" . $ronin_address);
                // error_log("https://api.lunaciarover.com/stats/" . $address);
                // error_log(json_encode($data));
                $data = json_encode($data);
                error_log($data);
                $data = json_decode($data);
                if (isset($data->slpData)) {
                    error_log($address . " " . $data->slpData->gameSlp);
                    $dt->yesterday_slp = $data->slpData->gameSlp;
                    //TAMBAH LOG MMR
                    // $log_mmr = LogMMR::whereBetween("created_at", [$today, $now])->where("id_axie", $dt->id)->first();
                    // if (!isset($log_mmr)) {
                    //KALO BELOM ADA BUAT BARU -> HARI BARU
                    $log_mmr = new LogMMR();
                    $log_mmr->id_axie = $dt->id;
                    $log_mmr->mmr = $data->leaderboardData->elo;
                    $log_mmr->save();
                    // } else {
                    //     //KALO SUDAH ADA UPDATE
                    //     $log_mmr->mmr = $data->leaderboardData->elo;
                    //     $log_mmr->save();
                    // }
                } else {
                    $log = new LogServer();
                    $log->description = "AXIE_SYNC_COMMAND_FAILED";
                    $log->save();
                    break;
                }
                // error_log($address);
                $dt->save();
            }
        }

        //CREATE LOG SERVER
        $log = new LogServer();
        $log->description = "AXIE_SYNC_COMMAND_DONE";
        $log->save();
        error_log("SYNC AXIE BERHASIL");
    }
}
