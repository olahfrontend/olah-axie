<?php

namespace App\Console\Commands;

use App\Models\DetailJadwalCuti;
use App\Models\JadwalCuti;
use App\Models\LogServer;
use App\Models\UserAxie;
use Error;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CutiCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CutiCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Cuti';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //CRONJOB UNTUK CUTI -> CEK APAKAH BESOK ADA YANG CUTI KALO ADA PINDAHKAN AKUN KE PENGGANTI
        $data = JadwalCuti::where("tanggal_cuti_mulai", Carbon::today())->get();
        // error_log($data);
        foreach ($data as $dt) {
            if ($dt->status == 0) {
                //PINDAHKAN AKUN USER YANG CUTI KE PENGGANTI
                $data_pengganti = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();

                foreach ($data_pengganti as $d) {
                    $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                    $useraxie->id_user = $d->id_pengganti;
                    $useraxie->save();
                    error_log("axie " . $useraxie->id_axie . " diberikan ke " . $useraxie->id_user);

                    $log = new LogServer();
                    $log->description = "AXIE " . $useraxie->id_axie . " DIBERIKAN KE " . $useraxie->id_user;
                    $log->save();
                }
                $dt->status = 1;
                $dt->save();
            }
        }

        $data = JadwalCuti::where("tanggal_cuti_selesai", Carbon::today())->get();
        foreach ($data as $dt) {
            if ($dt->status == 1) {
                //PINDAHKAN AKUN PENGGANTI KE USER KEMBALI
                $data_user = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();
                foreach ($data_user as $d) {
                    $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                    $useraxie->id_user = $dt->id_user;
                    $useraxie->save();

                    error_log("axie " . $useraxie->id_axie . " dikembalikan ke " . $useraxie->id_user);
                    $log = new LogServer();
                    $log->description = "AXIE " . $useraxie->id_axie . " DIKEMBALIKAN KE " . $useraxie->id_user;
                    $log->save();
                }
                $dt->status = 2;
                $dt->save();
            }
        }

        //CREATE LOG SERVER
        $log = new LogServer();
        $log->description = "MANUAL_CUTI_COMMAND_FINISHED";
        $log->save();
    }
}
