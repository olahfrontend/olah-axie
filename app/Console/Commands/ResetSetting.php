<?php

namespace App\Console\Commands;

use App\Models\LogServer;
use App\Models\SettingPVP;
use App\Models\Users;
use Illuminate\Console\Command;

class ResetSetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ResetSettingCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Cuti';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = Users::where("role", 3)->where("status", 1)->get();

        foreach ($data as $dt) {
            $setting = SettingPVP::where("id_user", $dt->id)->get();
            if (count($setting) <= 0) {
                //GENERATE SETTING DEFAULT
                $min_slp = [51, 66, 81, 110];
                $max_slp = [65, 80, 110, 999999];
                $value = [1000, 1500, 2000, 2500];
                for ($i = 0; $i < count($min_slp); $i++) {
                    $new = new SettingPVP();
                    $new->id_user = $dt->id;
                    $new->min_slp = $min_slp[$i];
                    $new->max_slp = $max_slp[$i];
                    $new->value = $value[$i];
                    $new->save();
                }
            }

            $log = new LogServer();
            $log->description = "GENERATE_SETTING_DEFAULT_PVP UNTUK " . strtoupper($dt->email);
            $log->save();
        }

        //CREATE LOG SERVER
        $log = new LogServer();
        $log->description = "GENERATE_DEFAULT_SETTING_COMMAND_SUCCESS";
        $log->save();
    }
}
