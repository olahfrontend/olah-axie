<?php

namespace App\Console\Commands;

use App\Models\DailyReport;
use App\Models\Gaji;
use App\Models\JadwalCuti;
use App\Models\LogGaji;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\Setting;
use App\Models\SettingPVP;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use stdClass;

class HitungBonusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'HitungBonusCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command Bonus';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        // $today = Carbon::today()->subDays(1);
        //YEAR-MONTH-DAY HOUR
        $dateFormat = '2021-11-08 09';
        $format = 'Y-m-d H';
        // $today = Carbon::createFromFormat($format, $dateFormat);
        $start_week = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->startOfWeek();
        $end_week = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->endOfWeek();

        $start_week_ymd = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->startOfWeek()->format("Y-m-d");
        $end_week_ymd = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->endOfWeek()->format("Y-m-d");

        $day = Carbon::createFromFormat($format, $dateFormat)->format("l");
        $created_at = Carbon::createFromFormat($format, $dateFormat)->subdays(1);

        $start_month = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->startOfMonth();
        $end_month = Carbon::createFromFormat($format, $dateFormat)->subDays(1)->endOfMonth();

        $day_number = Carbon::createFromFormat($format, $dateFormat)->subdays(1)->format('d');
        $end = Carbon::createFromFormat($format, $dateFormat)->subDays(1)->endOfMonth()->format('d');

        $month = Carbon::createFromFormat($format, $dateFormat)->format("m");
        $year = Carbon::createFromFormat($format, $dateFormat)->format("Y");

        $week_awal = Carbon::createFromFormat($format, $dateFormat)->modify('first day of this month')->format("W");
        $week = (Carbon::createFromFormat($format, $dateFormat)->format("W") - $week_awal);


        if ($day == "Monday") {
            //HARI SENIN HITUNG BONUS GAJI WEEKEND
            $data = Gaji::whereBetween('created_at', [$start_week, $end_week])
                ->where("type", "GAJI_HARIAN")
                ->get();

            $data_user_bonus = new stdClass();

            foreach ($data as $dt) {
                if (!isset($data_user_bonus->{$dt->id_user})) {
                    $data_user_bonus->{$dt->id_user} = new stdClass();
                    $data_user_bonus->{$dt->id_user}->id_user = $dt->id_user;
                    $data_user_bonus->{$dt->id_user}->gaji = 0;
                    $data_user_bonus->{$dt->id_user}->bonus = true;
                }
                if ($dt->gaji <= 0) {

                    $log_gaji = LogGaji::where("description", "GAJI BONUS MINGGUAN MINGGU KE-" . $week . " TIDAK CAIR KARENA TIDAK LAPORAN PADA TANGGAL " . $dt->created_at)
                        ->first();
                    if (!isset($log_gaji)) {
                        $log_gaji = new LogGaji();
                        $log_gaji->id_user = $dt->id_user;
                        $log_gaji->description = "GAJI BONUS MINGGUAN MINGGU KE-" . $week . " TIDAK CAIR KARENA TIDAK LAPORAN PADA TANGGAL " . $dt->created_at;
                        $log_gaji->save();
                    }

                    $data_user_bonus->{$dt->id_user}->bonus = false;
                }
                if ($data_user_bonus->{$dt->id_user}->bonus && $dt->id_daily_report != null) {
                    //HITUNG BONUS MINGGUAN
                    $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                    $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                    $bonus_mingguan = Setting::where("nama_setting", "BONUS_FULL_MINGGU")->where("id_user", $axie->id_owner)->first();
                    $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * ($bonus_mingguan->value / 100));
                    // error_log($data_user_bonus->{$dt->id_user}->gaji);
                }
            }

            // error_log(json_encode($data_user_bonus));
            foreach ($data_user_bonus as $dt) {
                if ($dt->bonus) {
                    //CEK ABSEN
                    $absen = JadwalCuti::where("id_user", $dt->id_user)
                        ->whereBetween('created_at', [$start_week, $end_week])
                        ->where("status_absen", 1)
                        ->get();
                    $exist_absen = false;
                    foreach ($absen as $abs) {
                        if ($abs->status_absen == 1 && !$exist_absen) {
                            $log_gaji = new LogGaji();
                            $log_gaji->id_user = $dt->id_user;
                            $log_gaji->description = "ABSEN TANGGAL " . $abs->tanggal_cuti_mulai . " BONUS MINGGUAN KE-" . $week . " TIDAK CAIR";
                            $log_gaji->save();
                            $exist_absen = true;
                        }
                    }

                    $data_daily = DB::select("
                    SELECT DATE(daily_report.created_at) as tanggal, AVG(daily_report.slp_pvp) as average 
                    FROM daily_report,master_axie 
                    WHERE master_axie.id = daily_report.id_axie 
                    AND 
                    daily_report.status_server = 1
                    AND
                    master_axie.id_player = daily_report.id_user 
                    AND 
                    daily_report.id_user = " . $dt->id_user . " 
                    AND DATE(daily_report.created_at) 
                    BETWEEN '" . $start_week_ymd . "' 
                    AND '" . $end_week_ymd . "'
                    GROUP BY DATE(daily_report.created_at)");

                    $exist_pvp_daily = false;
                    $user = Users::where("id", $dt->id_user)->first();
                    if ($user->role == 1) {
                        $setting_min_pvp_daily = Setting::where("nama_setting", "MIN_PVP_DAILY")->first();
                        foreach ($data_daily as $daily) {
                            //CEK DAILY AVERAGE PVP
                            if ($dt->id_user == 66) {
                                $jam = Carbon::parse($daily->created_at)->format("H");
                                error_log("id : " . $daily->id);
                                error_log("date : " . $daily->tanggal);
                                error_log("jam : " . $jam);
                                if ($jam < 9) {
                                    $yesterday = Carbon::createFromFormat("Y-m-d", $daily->tanggal)->subdays(1)->format("Y-m-d");
                                    error_log("tanggal sebelum : " . $yesterday);
                                }
                            }

                            $daily->average = ceil($daily->average);
                            if ($daily->average < $setting_min_pvp_daily->value) {
                                $log_gaji = new LogGaji();
                                $log_gaji->id_user = $dt->id_user;
                                $log_gaji->description = "AVERAGE PVP TANGGAL " . $daily->tanggal . " SEBANYAK " . $daily->average . " TIDAK MENCAPAI SYARAT MINIMAL (" . $setting_min_pvp_daily->value . ")";
                                $log_gaji->save();

                                $exist_pvp_daily = true;
                            }
                        }
                    }

                    if ($exist_pvp_daily) {
                        $log_gaji = new LogGaji();
                        $log_gaji->id_user = $dt->id_user;
                        $log_gaji->description = "BONUS MINGGUAN KE-" . $week . " TIDAK CAIR KARENA PVP DAILY TIDAK TERPENUHI";
                        $log_gaji->save();
                    }

                    if (!$exist_absen && !$exist_pvp_daily) {
                        //CAIRKAN BONUS WEEKEND
                        error_log("user id : " . $dt->id_user . " dapat bonus weekend : " . $dt->gaji);
                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->gaji = $dt->gaji;
                        $data->created_at = $created_at;
                        $data->type = "GAJI_BONUS_MINGGUAN";
                        $data->save();
                    } else {
                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->gaji = 0;
                        $data->created_at = $created_at;
                        $data->type = "GAJI_BONUS_MINGGUAN";
                        $data->save();
                    }
                }
            }
        }

        //HITUNGAN BONUS AKHIR BULAN
        if ($day_number == $end) {
            //AKHIR BULAN HITUNG BONUS AKHIR BULAN DAN BONUS PVP
            $data = Gaji::whereBetween('created_at', [$start_month, $end_month])
                ->where("type", "GAJI_HARIAN")
                ->orWhere("type", "GAJI_BONUS_MINGGUAN")
                ->whereBetween('created_at', [$start_month, $end_month])
                // ->whereRaw("WEEKDAY(created_at) <> 6")
                ->get();

            $data_user_bonus = new stdClass();
            foreach ($data as $dt) {
                // error_log($dt->id);
                if (!isset($data_user_bonus->{$dt->id_user})) {
                    $data_user_bonus->{$dt->id_user} = new stdClass();
                    $data_user_bonus->{$dt->id_user}->id_user = $dt->id_user;
                    $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                    $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                    $bonus_bulanan = Setting::where("nama_setting", "BONUS_FULL_BULAN")->where("id_user", $axie->id_owner)->first();
                    $data_user_bonus->{$dt->id_user}->gaji = ($dt->gaji * ($bonus_bulanan->value / 100));
                    $data_user_bonus->{$dt->id_user}->gaji_daily = $dt->gaji;
                    $data_user_bonus->{$dt->id_user}->gaji_mingguan = 0;
                    $data_user_bonus->{$dt->id_user}->bonus = true;
                } else if ($data_user_bonus->{$dt->id_user}->bonus) {
                    if ($dt->gaji <= 0) {
                        $data_user_bonus->{$dt->id_user}->bonus = false;

                        $log_gaji = new LogGaji();
                        $log_gaji->id_user = $dt->id_user;
                        if ($dt->type == "GAJI_HARIAN") {
                            $log_gaji->description = "BONUS BULANAN TIDAK CAIR KARENA TIDAK LAPORAN PADA HARI " . $dt->created_at;
                        } else if ($dt->type == "GAJI_BONUS_MINGGUAN") {
                            $log_gaji->description = "BONUS BULANAN TIDAK CAIR KARENA BONUS MINGGUAN TIDAK TERPENUHI";
                        }
                        $log_gaji->save();
                    } else if ($data_user_bonus->{$dt->id_user}->bonus && $dt->type == "GAJI_HARIAN") {
                        //HITUNG BONUS BULANAN
                        $data_user_bonus->{$dt->id_user}->gaji_daily += $dt->gaji;
                        $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                        $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                        $bonus_bulanan = Setting::where("nama_setting", "BONUS_FULL_BULAN")->where("id_user", $axie->id_owner)->first();
                        $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * ($bonus_bulanan->value / 100));

                        // error_log($data_user_bonus->{$dt->id_user}->gaji);
                    } else if ($data_user_bonus->{$dt->id_user}->bonus && $dt->type == "GAJI_BONUS_MINGGUAN") {
                        // error_log("mingguan bonus : " . $dt->gaji);
                        $data_user_bonus->{$dt->id_user}->gaji_mingguan += $dt->gaji;
                        //BONUS MINGGUAN JUGA DIHITUNG -> 20%
                        $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * (20 / 100));
                    }
                }
            }

            foreach ($data_user_bonus as $dt) {
                if ($dt->bonus) {
                    //CAIRKAN BONUS BULANAN
                    // error_log("user id : " . $dt->id_user . " gaji daily : " . $dt->gaji_daily);
                    // error_log("user id : " . $dt->id_user . " gaji minggu : " . $dt->gaji_mingguan);
                    // error_log("user id : " . $dt->id_user . " dapat bonus monthly : " . $dt->gaji);

                    $gaji = Gaji::where("id_user", $dt->id_user)
                        ->whereBetween('created_at', [$start_month, $end_month])
                        ->get();
                    $daily_report = DailyReport::where("id_user", $dt->id_user)
                        ->whereBetween('created_at', [$start_month, $end_month])
                        ->orderBy("created_at", "asc")
                        ->get();

                    // error_log(json_encode($daily_report));
                    $total_bonus_pvp = 0;
                    $data_pvp = Gaji::hitungBonusPVP($daily_report, $gaji, $month, $year);
                    $total_bonus_pvp = $data_pvp->est_bonus_pvp;
                    if ($total_bonus_pvp > 0) {
                        //CAIRKAN BONUS PVP
                        error_log("user id : " . $dt->id_user . " dapat bonus pvp : " . $total_bonus_pvp);
                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->created_at = $created_at;
                        $data->gaji = $total_bonus_pvp;
                        $data->type = "GAJI_BONUS_PVP";
                        $data->save();

                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->gaji = $dt->gaji;
                        $data->created_at = $created_at;
                        $data->type = "GAJI_BONUS_BULANAN";
                        $data->save();
                    } else {
                        error_log("user id : " . $dt->id_user . " tidak memenuhi pvp");
                        $log_gaji = new LogGaji();
                        $log_gaji->id_user = $dt->id_user;
                        $log_gaji->description = "SYARAT AVERAGE MINIMUM PVP BULANAN TIDAK TERPENUHI BONUS BULANAN, MINGGUAN, DAN PVP TIDAK CAIR";
                        $log_gaji->save();

                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->created_at = $created_at;
                        $data->gaji = 0;
                        $data->type = "GAJI_BONUS_PVP";
                        $data->save();

                        $data = new Gaji();
                        $data->id_user = $dt->id_user;
                        $data->id_daily_report = null;
                        $data->gaji = 0;
                        $data->created_at = $created_at;
                        $data->type = "GAJI_BONUS_BULANAN";
                        $data->save();
                    }
                }
            }
        }
    }
}
