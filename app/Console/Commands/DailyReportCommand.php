<?php

namespace App\Console\Commands;

use App\Models\DailyReport;
use App\Models\DendaInventori;
use App\Models\DetailJadwalCuti;
use App\Models\Gaji;
use App\Models\JadwalCuti;
use App\Models\LogGaji;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\Setting;
use App\Models\SettingLevel;
use App\Models\UserAxie;
use App\Models\Users;
use DateTime;
use Error;
use Hamcrest\Core\Set;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Mockery\Undefined;
use stdClass;

class DailyReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DailyReportCommand';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hitung gaji bonus (weekend, monthly) dan cek user yang tidak lapor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Carbon::setWeekStartsAt(Carbon::MONDAY);
        Carbon::setWeekEndsAt(Carbon::SATURDAY);
        //KARENA DIJALANKAN JAM 9 MAKA CEKNYA HARI KMARENNYA
        $today = Carbon::now()->subDays(1);
        $actual_today = Carbon::now();
        $now = Carbon::now();
        $start_week = Carbon::today()->subDays(1)->startOfWeek();
        $end_week = Carbon::today()->subDays(1)->endOfWeek();
        $start_month = Carbon::today()->subdays(1)->startOfMonth();
        $end_month = Carbon::today()->subDays(1)->endOfMonth();
        $firstday = Carbon::now()->subDays(1)->modify('first day of this month');
        $week_awal = Carbon::now()->subDays(1)->modify('first day of this month')->format("W");

        $week = (Carbon::now()->subDays(1)->format("W") - $week_awal);
        $created_at = Carbon::today()->subDays(1);

        $start_week_ymd = Carbon::today()->subDays(1)->startOfWeek()->format("Y-m-d");
        $end_week_ymd = Carbon::today()->subDays(1)->endOfWeek()->format("Y-m-d");
        // error_log($today);
        // error_log($actual_today);

        // error_log($start_week);
        // error_log($end_week);
        // error_log($start_month);
        // error_log($end_month);
        // $obj = date('Y-m-d', strtotime('-1 day', strtotime(Date('D'))));
        // $obj = date('D', $today);

        $day = Carbon::now()->format("l");
        $day_number = Carbon::now()->subDays(1)->subdays(1)->format('d');
        $end = Carbon::today()->subDays(1)->endOfMonth()->format('d');


        $month = Carbon::now()->subDays(1)->format("m");
        $year = Carbon::now()->subDays(1)->format("Y");

        //CEK APAKAH CRONJOB SUDAH JALAN HARI INI
        $checkLog = LogServer::where("description", "CRONJOB_DAILY_SUCCESS")
            ->whereDate("created_at", $actual_today)
            ->first();

        // error_log($today);


        if (!isset($checkLog)) {
            //KALO BELUM JALAN JALANKAN
            $daily_report = DailyReport::whereBetween('created_at', [$today, $now])->get();
            $user_axie = UserAxie::get();

            foreach ($user_axie as $dt) {
                //CEK APAKAH USER HARI ITU LAPOR
                $daily_report = DailyReport::where("id_user", $dt->id_user)
                    ->where("id_axie", $dt->id_axie)
                    ->whereBetween('created_at', [$today, $now])
                    ->first();
                if ($daily_report == null) {
                    error_log("user axie id yang tidak report : " . $dt->id);

                    $axie = MasterAxie::where("id", $dt->id_axie)->first();

                    $data_daily = new DailyReport();
                    $data_daily->id_user = $dt->id_user;
                    $data_daily->id_axie = $dt->id_axie;
                    $data_daily->slp_adventure = 0;
                    $data_daily->slp_pvp = 0;
                    $data_daily->slp_quest = 0;
                    $data_daily->created_at = $today;
                    $data_daily->last_inventory = $axie->total_slp;
                    $data_daily->level_before = $axie->level;
                    $data_daily->status = 1;
                    $data_daily->save();

                    $data = new Gaji();
                    $data->id_user = $dt->id_user;
                    $data->id_daily_report = $data_daily->id;
                    $data->id_axie = $dt->id_axie;
                    $data->gaji = 0;
                    $data->created_at = $today;
                    $data->type = "GAJI_HARIAN";
                    $data->save();

                    $log_gaji = new LogGaji();
                    $log_gaji->id_user = $dt->id_user;
                    $log_gaji->description = "AKUN " . strtoupper($axie->email_akun) . " TIDAK LAPORAN PADA TANGGAL " . $today;
                    $log_gaji->save();

                    $log = new LogServer();
                    $data_user = Users::where('id', $dt->id_user)->first();
                    $log->description = "USER " . strtoupper($data_user->email) . " DENGAN AXIE " . strtoupper($axie->email_akun) . " TIDAK LAPORAN UNTUK TANGGAL " . $today;
                    $log->save();
                } else {
                    if ($daily_report->status == 0) {
                        error_log("report tidak diapprove : " . $daily_report->id);
                        $daily_report->status = 1;
                        $daily_report->save();

                        $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                        if ($daily_report->level_before != null) {
                            $axie->total_slp += ($daily_report->slp_adventure + $daily_report->slp_pvp + $daily_report->slp_quest);
                            $axie->save();
                        }

                        $setting_level = SettingLevel::getMinimalSLP($axie->id_owner, $axie->level);
                        // $day = date('D');

                        if ($day == "Monday") {
                            //KALO HARI MINGGU TAMBAH BONUS
                            $bonus_minggu = Setting::where("nama_setting", "BONUS_HARI_MINGGU")->first();
                            $setting_level->value = $setting_level->value + $bonus_minggu->value;
                        }

                        if ($daily_report->slp_adventure >= $setting_level->min_slp) {
                            $data = new Gaji();
                            $data->id_user = $daily_report->id_user;
                            $data->id_daily_report = $daily_report->id;
                            $data->id_axie = $daily_report->id_axie;
                            $data->gaji = $setting_level->value;
                            $data->created_at = $today;
                            $data->type = "GAJI_HARIAN";
                            $data->save();
                        } else {
                            $data = new Gaji();
                            $data->id_user = $daily_report->id_user;
                            $data->id_daily_report = $daily_report->id;
                            $data->id_axie = $daily_report->id_axie;
                            $data->created_at = $today;
                            $data->gaji = 0;
                            $data->type = "GAJI_HARIAN";
                            $data->save();
                        }

                        $log = new LogServer();
                        $log->description = "ID REPORT " . $daily_report->id . " TIDAK DIAPPROVE (AUTO APPROVE)";
                        $log->save();
                    }
                }
            }

            //HITUNGAN AKHIR WEEKEND
            if ($day == "Monday") {
                //HARI SENIN HITUNG BONUS GAJI WEEKEND
                $data = Gaji::whereBetween('created_at', [$start_week, $end_week])
                    ->where("type", "GAJI_HARIAN")
                    ->get();

                $data_user_bonus = new stdClass();
                foreach ($data as $dt) {
                    if (!isset($data_user_bonus->{$dt->id_user})) {
                        $data_user_bonus->{$dt->id_user} = new stdClass();
                        $data_user_bonus->{$dt->id_user}->id_user = $dt->id_user;
                        $data_user_bonus->{$dt->id_user}->gaji = 0;
                        $data_user_bonus->{$dt->id_user}->bonus = true;
                    }
                    if ($dt->gaji <= 0) {

                        $log_gaji = LogGaji::where("description", "GAJI BONUS MINGGUAN MINGGU KE-" . $week . " TIDAK CAIR KARENA TIDAK LAPORAN PADA TANGGAL " . $dt->created_at)
                            ->first();
                        if (!isset($log_gaji)) {
                            $log_gaji = new LogGaji();
                            $log_gaji->id_user = $dt->id_user;
                            $log_gaji->description = "GAJI BONUS MINGGUAN MINGGU KE-" . $week . " TIDAK CAIR KARENA TIDAK LAPORAN PADA TANGGAL " . $dt->created_at;
                            $log_gaji->save();
                        }

                        $data_user_bonus->{$dt->id_user}->bonus = false;
                    }
                    if ($data_user_bonus->{$dt->id_user}->bonus && $dt->id_daily_report != null) {
                        //HITUNG BONUS MINGGUAN
                        $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                        $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                        $bonus_mingguan = Setting::where("nama_setting", "BONUS_FULL_MINGGU")->where("id_user", $axie->id_owner)->first();
                        $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * ($bonus_mingguan->value / 100));
                        // error_log($data_user_bonus->{$dt->id_user}->gaji);
                    }
                }
                // error_log(json_encode($data_user_bonus));
                foreach ($data_user_bonus as $dt) {
                    if ($dt->bonus) {
                        //CEK ABSEN
                        $absen = JadwalCuti::where("id_user", $dt->id_user)
                            ->whereBetween('created_at', [$start_week, $end_week])
                            ->where("status_absen", 1)
                            ->get();
                        $exist_absen = false;
                        foreach ($absen as $abs) {
                            if ($abs->status_absen == 1 && !$exist_absen) {
                                $log_gaji = new LogGaji();
                                $log_gaji->id_user = $dt->id_user;
                                $log_gaji->description = "ABSEN TANGGAL " . $abs->tanggal_cuti_mulai . " BONUS MINGGUAN KE-" . $week . " TIDAK CAIR";
                                $log_gaji->save();
                                $exist_absen = true;
                            }
                        }

                        $data_daily = DB::select("
                        SELECT DATE(daily_report.created_at) as tanggal, AVG(daily_report.slp_pvp) as average 
                        FROM daily_report,master_axie 
                        WHERE master_axie.id = daily_report.id_axie 
                        AND 
                        daily_report.status_server = 1
                        AND
                        master_axie.id_player = daily_report.id_user 
                        AND 
                        daily_report.id_user = " . $dt->id_user . " 
                        AND DATE(daily_report.created_at) 
                        BETWEEN '" . $start_week_ymd . "' 
                        AND '" . $end_week_ymd . "'
                        GROUP BY DATE(daily_report.created_at)");

                        $exist_pvp_daily = false;
                        $user = Users::where("id", $dt->id_user)->first();
                        if ($user->role == 1) {
                            $setting_min_pvp_daily = Setting::where("nama_setting", "MIN_PVP_DAILY")->first();
                            foreach ($data_daily as $daily) {
                                $daily->average = ceil($daily->average);
                                if ($daily->average < $setting_min_pvp_daily->value) {
                                    $log_gaji = new LogGaji();
                                    $log_gaji->id_user = $dt->id_user;
                                    $log_gaji->description = "AVERAGE PVP TANGGAL " . $daily->tanggal . " SEBANYAK " . $daily->average . " TIDAK MENCAPAI SYARAT MINIMAL (" . $setting_min_pvp_daily->value . ")";
                                    $log_gaji->save();

                                    $exist_pvp_daily = true;
                                }
                            }
                        }

                        if ($exist_pvp_daily) {
                            $log_gaji = new LogGaji();
                            $log_gaji->id_user = $dt->id_user;
                            $log_gaji->description = "BONUS MINGGUAN KE-" . $week . " TIDAK CAIR KARENA PVP DAILY TIDAK TERPENUHI";
                            $log_gaji->save();
                        }

                        if (!$exist_absen && !$exist_pvp_daily) {
                            //CAIRKAN BONUS WEEKEND
                            error_log("user id : " . $dt->id_user . " dapat bonus weekend : " . $dt->gaji);
                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->gaji = $dt->gaji;
                            $data->created_at = $created_at;
                            $data->type = "GAJI_BONUS_MINGGUAN";
                            $data->save();
                        } else {
                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->gaji = 0;
                            $data->created_at = $created_at;
                            $data->type = "GAJI_BONUS_MINGGUAN";
                            $data->save();
                        }
                    }
                }
            }


            //HITUNGAN BONUS AKHIR BULAN
            if ($day_number == $end) {
                //AKHIR BULAN HITUNG BONUS AKHIR BULAN DAN BONUS PVP
                $data = Gaji::whereBetween('created_at', [$start_month, $end_month])
                    ->where("type", "GAJI_HARIAN")
                    ->orWhere("type", "GAJI_BONUS_MINGGUAN")
                    ->whereBetween('created_at', [$start_month, $end_month])
                    // ->whereRaw("WEEKDAY(created_at) <> 6")
                    ->get();

                $data_user_bonus = new stdClass();
                foreach ($data as $dt) {
                    // error_log($dt->id);
                    if (!isset($data_user_bonus->{$dt->id_user})) {
                        $data_user_bonus->{$dt->id_user} = new stdClass();
                        $data_user_bonus->{$dt->id_user}->id_user = $dt->id_user;
                        $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                        $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                        $bonus_bulanan = Setting::where("nama_setting", "BONUS_FULL_BULAN")->where("id_user", $axie->id_owner)->first();
                        $data_user_bonus->{$dt->id_user}->gaji = ($dt->gaji * ($bonus_bulanan->value / 100));
                        $data_user_bonus->{$dt->id_user}->gaji_daily = $dt->gaji;
                        $data_user_bonus->{$dt->id_user}->gaji_mingguan = 0;
                        $data_user_bonus->{$dt->id_user}->bonus = true;
                    } else if ($data_user_bonus->{$dt->id_user}->bonus) {
                        if ($dt->gaji <= 0) {
                            $data_user_bonus->{$dt->id_user}->bonus = false;

                            $log_gaji = new LogGaji();
                            $log_gaji->id_user = $dt->id_user;
                            if ($dt->type == "GAJI_HARIAN") {
                                $log_gaji->description = "BONUS BULANAN TIDAK CAIR KARENA TIDAK LAPORAN PADA HARI " . $dt->created_at;
                            } else if ($dt->type == "GAJI_BONUS_MINGGUAN") {
                                $log_gaji->description = "BONUS BULANAN TIDAK CAIR KARENA BONUS MINGGUAN TIDAK TERPENUHI";
                            }
                            $log_gaji->save();
                        } else if ($data_user_bonus->{$dt->id_user}->bonus && $dt->type == "GAJI_HARIAN") {
                            //HITUNG BONUS BULANAN
                            $data_user_bonus->{$dt->id_user}->gaji_daily += $dt->gaji;
                            $daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                            $axie = MasterAxie::where("id", $daily_report->id_axie)->first();
                            $bonus_bulanan = Setting::where("nama_setting", "BONUS_FULL_BULAN")->where("id_user", $axie->id_owner)->first();
                            $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * ($bonus_bulanan->value / 100));

                            // error_log($data_user_bonus->{$dt->id_user}->gaji);
                        } else if ($data_user_bonus->{$dt->id_user}->bonus && $dt->type == "GAJI_BONUS_MINGGUAN") {
                            // error_log("mingguan bonus : " . $dt->gaji);
                            $data_user_bonus->{$dt->id_user}->gaji_mingguan += $dt->gaji;
                            //BONUS MINGGUAN JUGA DIHITUNG -> 20%
                            $data_user_bonus->{$dt->id_user}->gaji += ($dt->gaji * (20 / 100));
                        }
                    }
                }

                foreach ($data_user_bonus as $dt) {
                    if ($dt->bonus) {
                        //CAIRKAN BONUS BULANAN
                        // error_log("user id : " . $dt->id_user . " gaji daily : " . $dt->gaji_daily);
                        // error_log("user id : " . $dt->id_user . " gaji minggu : " . $dt->gaji_mingguan);
                        // error_log("user id : " . $dt->id_user . " dapat bonus monthly : " . $dt->gaji);

                        $gaji = Gaji::where("id_user", $dt->id_user)
                            ->whereBetween('created_at', [$start_month, $end_month])
                            ->get();
                        $daily_report = DailyReport::where("id_user", $dt->id_user)
                            ->whereBetween('created_at', [$start_month, $end_month])
                            ->orderBy("created_at", "asc")
                            ->get();

                        // error_log(json_encode($daily_report));
                        $total_bonus_pvp = 0;
                        $data_pvp = Gaji::hitungBonusPVP($daily_report, $gaji, $month, $year);
                        $total_bonus_pvp = $data_pvp->est_bonus_pvp;
                        if ($total_bonus_pvp > 0) {
                            //CAIRKAN BONUS PVP
                            error_log("user id : " . $dt->id_user . " dapat bonus pvp : " . $total_bonus_pvp);
                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->created_at = $today;
                            $data->gaji = $total_bonus_pvp;
                            $data->type = "GAJI_BONUS_PVP";
                            $data->save();

                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->gaji = $dt->gaji;
                            $data->created_at = $today;
                            $data->type = "GAJI_BONUS_BULANAN";
                            $data->save();
                        } else {
                            error_log("user id : " . $dt->id_user . " tidak memenuhi pvp");
                            $log_gaji = new LogGaji();
                            $log_gaji->id_user = $dt->id_user;
                            $log_gaji->description = "SYARAT AVERAGE MINIMUM PVP BULANAN TIDAK TERPENUHI BONUS BULANAN, MINGGUAN, DAN PVP TIDAK CAIR";
                            $log_gaji->save();

                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->created_at = $today;
                            $data->gaji = 0;
                            $data->type = "GAJI_BONUS_PVP";
                            $data->save();

                            $data = new Gaji();
                            $data->id_user = $dt->id_user;
                            $data->id_daily_report = null;
                            $data->gaji = 0;
                            $data->created_at = $today;
                            $data->type = "GAJI_BONUS_BULANAN";
                            $data->save();
                        }
                    }
                }
            }

            //CRONJOB UNTUK CUTI -> CEK APAKAH BESOK ADA YANG CUTI KALO ADA PINDAHKAN AKUN KE PENGGANTI
            $data = JadwalCuti::where("tanggal_cuti_mulai", Carbon::today())->get();
            foreach ($data as $dt) {
                if ($dt->status == 0) {
                    //PINDAHKAN AKUN USER YANG CUTI KE PENGGANTI
                    $data_pengganti = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();

                    foreach ($data_pengganti as $d) {
                        $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                        $useraxie->id_user = $d->id_pengganti;
                        $useraxie->save();
                        // error_log("axie " . $useraxie->id_axie . " diberikan ke " . $useraxie->id_user);

                        $log = new LogServer();
                        $log->description = "AXIE " . $useraxie->id_axie . " DIBERIKAN KE " . $useraxie->id_user;
                        $log->save();
                    }
                    $dt->status = 1;
                    $dt->save();
                }
            }

            $data = JadwalCuti::where("tanggal_cuti_selesai", Carbon::today())->orderBy("created_at", "desc")->get();
            foreach ($data as $dt) {
                if ($dt->status == 1) {
                    //PINDAHKAN AKUN PENGGANTI KE USER KEMBALI
                    $data_user = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();
                    foreach ($data_user as $d) {
                        $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                        $useraxie->id_user = $dt->id_user;
                        $useraxie->save();

                        // error_log("axie " . $useraxie->id_axie . " dikembalikan ke " . $useraxie->id_user);
                        $log = new LogServer();
                        $log->description = "AXIE " . $useraxie->id_axie . " DIKEMBALIKAN KE " . $useraxie->id_user;
                        $log->save();
                    }
                    $dt->status = 2;
                    $dt->save();
                }
            }

            //CREATE LOG SERVER
            $log = new LogServer();
            $log->description = "CRONJOB_DAILY_SUCCESS";
            $log->save();
        } else {
            //CREATE LOG SERVER
            $log = new LogServer();
            $log->description = "CRONJOB_DAILY_FAILED";
            $log->save();
            error_log("Cronjob telah dijalankan untuk hari ini");
        }
    }
}
