<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DendaInventori;
use App\Models\DetailJadwalCuti;
use App\Models\JadwalCuti;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\UserAxie;
use App\Models\Users;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;
use stdClass;

class MasterUserController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data = CommonHelper::getAPI("/api/user/list");
        // $data_cuti = JadwalCuti::join("users", "users.id", "jadwal_cuti.id_user")->select("users.name", "users.email", "jadwal_cuti.*")->orderBy("id", "DESC")->get();
        // print_r($data->payload);
        return view(
            "Admin.User.index",
            [
                "dataUser" => $initialData->payload,
                "data" => $data->payload,
                // "data_cuti" => $data_cuti,
            ]
        );
    }

    public function listcuti()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //ROLE SUPERADMIN
            $data_cuti = JadwalCuti::join("users", "users.id", "jadwal_cuti.id_user")
                ->select("users.name", "users.email", "jadwal_cuti.*")
                ->orderBy("id", "DESC")
                ->get();
        } else if ($role->jabatan == 2) {
            //ROLE OWNER
            $data_cuti = JadwalCuti::join("users", "users.id", "jadwal_cuti.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->select("users.name", "users.email", "jadwal_cuti.*")
                ->orderBy("id", "DESC")
                ->get();
        } else if ($role->jabatan == 1) {
            //ROLE PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_cuti = JadwalCuti::join("users", "users.id", "jadwal_cuti.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("users.name", "users.email", "jadwal_cuti.*")
                ->orderBy("id", "DESC")
                ->get();
        }
        return view(
            "Admin.User.listcuti",
            [
                "dataUser" => $initialData->payload,
                "data_cuti" => $data_cuti
            ]
        );
    }

    public function listdenda()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //ROLE SUPERADMIN
            $data_denda = DendaInventori::join("users", "users.id", "denda_inventori.id_user")
                ->select("denda_inventori.*", "users.name")
                ->get();
        } else if ($role->jabatan == 2) {
            //ROLE OWNER
            $data_denda = DendaInventori::join("users", "users.id", "denda_inventori.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->select("denda_inventori.*", "users.name")
                ->get();
        } else if ($role->jabatan == 1) {
            //ROLE PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_denda = DendaInventori::join("users", "users.id", "denda_inventori.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("denda_inventori.*", "users.name")
                ->get();
        }

        return view(
            "Admin.User.listdenda",
            [
                "dataUser" => $initialData->payload,
                "data_denda" => $data_denda
            ]
        );
    }

    public function adddenda()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //ROLE SUPERADMIN
            $data_pegawai = Users::where("role", 1)
                ->orWhere("role", 5)
                ->where("status", 1)
                ->get();
        } else if ($role->jabatan == 2) {
            //ROLE OWNER
            $data_pegawai = Users::where([
                ['role', '<>', 2],
                ['role', '<>', 3],
                ['role', '<>', 4],
                ['role', '<>', 6],
                ['status', '=', 1]
            ])
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->get();
        } else if ($role->jabatan == 1) {
            //ROLE PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_pegawai = Users::where([
                ['role', '<>', 2],
                ['role', '<>', 3],
                ['role', '<>', 4],
                ['role', '<>', 6],
                ['status', '=', 1]
            ])
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->get();
        }
        return view("Admin.User.adddenda", [
            "dataUser" => $initialData->payload,
            "data_pegawai" => $data_pegawai
        ]);
    }

    public function do_add_denda(Request $request)
    {
        $newRequest = Request::create('/api/denda/add', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Add Denda Berhasil", "success", "/admin/list_denda");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function listreimburs()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data = CommonHelper::getAPI("/api/reimburs/list");
        return view(
            "Admin.User.listreimburs",
            [
                "dataUser" => $initialData->payload,
                "data" => $data->payload
            ]
        );
    }

    public function detail($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        Input::merge([
            "id" => $id
        ]);
        $data = CommonHelper::getAPI("/api/user/detail");


        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //ROLE SUPERADMIN
            $data_kantor = MasterKantor::get();
        } else if ($role->jabatan == 2) {
            //ROLE OWNER
            $data_kantor = MasterKantor::where("id_owner", $initialData->payload->id)
                ->get();
        } else if ($role->jabatan == 1) {
            //ROLE PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_kantor = MasterKantor::where("id_owner", $work_at->id_owner)
                ->get();
        }
        // echo json_encode($data);
        $data_role = MasterRole::get();
        return view("Admin.User.detail", [
            "data" => $data->payload,
            "dataUser" => $initialData->payload,
            "data_kantor" => $data_kantor,
            "data_role" => $data_role
        ]);
    }

    public function doadd(Request $request)
    {
        if ($request->password == $request->cpassword) {
            $newRequest = Request::create('/api/user/register', 'POST');
            $response = Route::dispatch($newRequest);
            $res = json_decode($response->getContent());
            if ($response->getStatusCode() == 200) {
                CommonHelper::showAlert("Sukses", "Add User Berhasil", "success", "/admin/master_user");
            } else {
                CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
            }
        } else {
            CommonHelper::showAlert("Gagal", "Konfirmasi password salah", "error", "back");
        }
    }

    public function add()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data_role = MasterRole::get();
        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            $data_kantor = MasterKantor::get();
        } else if ($role->jabatan == 3) {
            $data_kantor = MasterKantor::where("id_owner", $initialData->payload->id)->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_kantor = MasterKantor::where("id_owner", $work_at->id_owner)->get();
        }
        return view("Admin.User.register", [
            "dataUser" => $initialData->payload,
            "data_kantor" => $data_kantor,
            "data_role" => $data_role
        ]);
    }

    public function log()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // print_r($initialData->payload->user->role);
        // if ($initialData->payload->role != 2) {
        //     return redirect("/admin");
        // }

        $data = CommonHelper::getAPI("/api/log");

        $data_server_error = LogServer::where("description", "like", "%MENGUPDATE SETTING SETTING_SERVER_AXIE%")->get();

        foreach ($data_server_error as $dt) {
            $temp = substr($dt->description, -6);
            if ($temp == "NORMAL") {
                $dt->status = "NORMAL";
            } else {
                $dt->status = "ERROR";
            }
        }
        // print_r($data->payload);
        return view(
            "Admin.LogServer.index",
            [
                "data" => $data->payload,
                "dataUser" => $initialData->payload,
                "data_server_error" => $data_server_error
            ]
        );
    }

    public function addCuti()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //ROLE SUPERADMIN
            $data_user = Users::where("role", 1)
                ->orWhere("role", 5)
                ->get();
        } else if ($role->jabatan == 2) {
            //ROLE OWNER
            $data_user = Users::where("role", "<>", 2)
                ->where("role", "<>", 3)
                ->where("role", "<>", 4)
                ->where("role", "<>", 6)
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id_owner)
                ->select("users.id", "users.name")
                ->get();
        } else if ($role->jabatan == 1) {
            //ROLE PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_user = Users::where("role", "<>", 2)
                ->where("role", "<>", 3)
                ->where("role", "<>", 4)
                ->where("role", "<>", 6)
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("users.id", "users.name")
                ->get();
        }
        return view(
            "Admin.User.addcuti",
            [
                "data_user" => $data_user,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function doAddCuti(Request $request)
    {

        $date_start = $request->tanggal_cuti_mulai;
        $date_start = strtotime($date_start);
        $date_start = date('Y/m/d', $date_start);

        $date_end = $request->tanggal_cuti_selesai;
        $date_end = strtotime($date_end);
        $date_end = date('Y/m/d', $date_end);

        // echo $date_start;
        $data = new JadwalCuti();
        $data->id_user = $request->id_user;
        $data->tanggal_cuti_mulai = $date_start;
        $data->tanggal_cuti_selesai = $date_end;
        $data->alasan = $request->alasan;
        $data->status_absen = $request->status_absen;
        if (isset($request->status_absen)) {
            $data->status_absen = $request->status_absen;
        }
        $data->save();

        CommonHelper::showAlert("Sukses", "Sukses tambah jadwal cuti", "success", URL("/admin/master_user/detail_cuti/" . $data->id));
    }

    public function doeditcuti(Request $request)
    {
        $date_start = $request->tanggal_cuti_mulai;
        $date_start = strtotime($date_start);
        $date_start = date('Y/m/d', $date_start);

        $date_end = $request->tanggal_cuti_selesai;
        $date_end = strtotime($date_end);
        $date_end = date('Y/m/d', $date_end);

        $data = JadwalCuti::where('id', $request->id)->first();
        $data->tanggal_cuti_mulai = $date_start;
        $data->tanggal_cuti_selesai = $date_end;
        $data->alasan = $request->alasan;
        if ($request->status_absen == 1) {
            $data->status_absen = 1;
        } else {
            $data->status_absen = 0;
        }

        $data->save();

        CommonHelper::showAlert("Sukses", "Sukses edit jadwal cuti", "success", URL("/admin/master_user/detail_cuti/" . $data->id));
    }

    public function logout()
    {
        $newRequest = Request::create('/api/user/logout', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Logout Berhasil", "success", "/admin");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function detailCuti($id)
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data_user = JadwalCuti::where("jadwal_cuti.id", $id)
            ->join("users", "users.id", "jadwal_cuti.id_user")
            ->select("users.name", "jadwal_cuti.*")
            ->first();

        $data_user_pengganti = Users::where("role", 1)
            ->orWhere("role", 5)
            ->where("id", "<>", $data_user->id_user)
            ->get();

        $data_axie_user = UserAxie::where("user_axie.id_user", $data_user->id_user)
            ->join("master_axie", "master_axie.id", "user_axie.id_axie")
            ->select("user_axie.*", "master_axie.email_akun")
            ->get();

        // print_r($data_user_pengganti);

        $detail_jadwal_cuti = DetailJadwalCuti::where("detail_jadwal_cuti.id_jadwal_cuti", $data_user->id)
            ->join("users", "users.id", "detail_jadwal_cuti.id_pengganti")
            ->join("master_axie", "master_axie.id", "detail_jadwal_cuti.id_axie")
            ->select("users.name", "master_axie.email_akun", "detail_jadwal_cuti.*")
            ->get();
        $data_axie_available = [];
        // echo json_encode($data_axie_user);
        // echo json_encode($detail_jadwal_cuti);

        foreach ($data_axie_user as $dt) {
            $exist = false;
            foreach ($detail_jadwal_cuti as $d) {
                if ($d->id_axie == $dt->id_axie) {
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                $data_axie_available[] = $dt;
            }
        }

        // echo json_encode($data_axie_available);
        // print_r($data_axie_user);
        $data_user->tanggal_cuti_mulai = strtotime($data_user->tanggal_cuti_mulai);
        $data_user->tanggal_cuti_mulai = date('m/d/Y', $data_user->tanggal_cuti_mulai);

        $data_user->tanggal_cuti_selesai = strtotime($data_user->tanggal_cuti_selesai);
        $data_user->tanggal_cuti_selesai = date('m/d/Y', $data_user->tanggal_cuti_selesai);
        return view(
            "Admin.User.detailcuti",
            [
                "data_user" => $data_user,
                "data_user_pengganti" => $data_user_pengganti,
                "data_axie_user" => $data_axie_available,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function detailLaporan($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data_user = Users::where("id", $id)->first();
        return view(
            "Admin.User.detaillaporan",
            [
                "data_user" => $data_user,
                "dataUser" => $initialData->payload
            ]
        );
    }

    public function do_edit_kantor(Request $request)
    {
        $id_user = $request->id_user;
        $id_kantor = $request->id_kantor;

        $data = Users::where("id", $id_user)->first();
        $data->id_kantor = $id_kantor;
        $data->save();

        CommonHelper::showAlert("Sukses", "Edit Lokasi Berhasil", "success", "/admin/master_user/" . $id_user);
        // echo $id_user . " " . $id_kantor;
    }

    public function toggle_status_pegawai($id)
    {
        $data = Users::where('id', $id)->first();
        if ($data->status == 1) {
            $data->status = 0;
            $data->tanggal_berhenti = date('Y/m/d');
            $data->save();
            CommonHelper::showAlert("Sukses", "Pegawai telah dinonaktifkan", "success", "/admin/master_user/" . $id);
        } else {
            $data->status = 1;
            $data->tanggal_berhenti = null;
            $data->save();
            CommonHelper::showAlert("Sukses", "Pegawai telah diaktifkan", "success", "/admin/master_user/" . $id);
        }
    }

    public function startCommandCuti()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role != 2 && $initialData->payload->role != 3 && $initialData->payload->role != 6) {
        //     CommonHelper::showAlert("Failed", "User not Allowed", "success", "/admin/list_cuti");
        //     return redirect("/admin");
        // }

        $now = Carbon::now();
        $now_hour = Carbon::parse($now)->format("H");

        if ($now_hour < 9) {
            CommonHelper::showAlert("Failed", "Start Command hanya dapat dijalankan jam 9 ke atas", "error", "back");
            // return redirect("/admin/list_cuti");
        } else {

            //CRONJOB UNTUK CUTI -> CEK APAKAH BESOK ADA YANG CUTI KALO ADA PINDAHKAN AKUN KE PENGGANTI

            //CREATE LOG SERVER
            $log = new LogServer();
            $log->description = strtoupper($initialData->payload->email) . " START CUTI COMMAND";
            $log->save();
            $data = JadwalCuti::where("tanggal_cuti_mulai", Carbon::today())->get();
            // error_log($data);
            foreach ($data as $dt) {
                if ($dt->status == 0) {
                    //PINDAHKAN AKUN USER YANG CUTI KE PENGGANTI
                    $data_pengganti = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();

                    foreach ($data_pengganti as $d) {
                        $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                        $useraxie->id_user = $d->id_pengganti;
                        $useraxie->save();
                        error_log("axie " . $useraxie->id_axie . " diberikan ke " . $useraxie->id_user);

                        $log = new LogServer();
                        $log->description = "AXIE " . $useraxie->id_axie . " DIBERIKAN KE " . $useraxie->id_user;
                        $log->save();
                    }
                    $dt->status = 1;
                    $dt->save();
                }
            }

            $data = JadwalCuti::where("tanggal_cuti_selesai", Carbon::today())->orderBy("created_at", "desc")->get();
            foreach ($data as $dt) {
                if ($dt->status == 1) {
                    //PINDAHKAN AKUN PENGGANTI KE USER KEMBALI
                    $data_user = DetailJadwalCuti::where("id_jadwal_cuti", $dt->id)->get();
                    foreach ($data_user as $d) {
                        $useraxie = UserAxie::where("id_axie", $d->id_axie)->first();
                        $useraxie->id_user = $dt->id_user;
                        $useraxie->save();

                        error_log("axie " . $useraxie->id_axie . " dikembalikan ke " . $useraxie->id_user);
                        $log = new LogServer();
                        $log->description = "AXIE " . $useraxie->id_axie . " DIKEMBALIKAN KE " . $useraxie->id_user;
                        $log->save();
                    }
                    $dt->status = 2;
                    $dt->save();
                }
            }

            //CREATE LOG SERVER
            $log = new LogServer();
            $log->description = "START CUTI COMMAND FINISHED";
            $log->save();

            CommonHelper::showAlert("Sukses", "Cuti Command Telah Dijalankan", "success", "/admin/list_cuti");
        }
    }

    public function do_edit_detail(Request $request)
    {
        $data = Users::where("id", $request->id)->first();
        $data->name = $request->name;
        $data->role = $request->role;
        $data->email = $request->email;
        $data->alamat = $request->alamat;
        $data->no_ktp = $request->no_ktp;
        $data->no_telp = $request->no_telp;
        $data->no_telp_darurat_1 = $request->no_telp_darurat_1;
        $data->nama_darurat_1 = $request->nama_darurat_1;
        $data->hubungan_darurat_1 = $request->hubungan_darurat_1;
        $data->no_telp_darurat_2 = $request->no_telp_darurat_2;
        $data->nama_darurat_2 = $request->nama_darurat_2;
        $data->hubungan_darurat_2 = $request->hubungan_darurat_2;
        $data->save();

        CommonHelper::showAlert("Sukses", "Edit Pegawai Berhasil", "success", "/admin/master_user");
    }

    public function uploadPhoto(Request $request)
    {
        $file = $request->file;
        $id_user = $request->id_user;

        $category = "foto";
        $url_photo = $this->uploadImage($file, $category);

        if ($request->type == "ktp") {
            $data = Users::where("id", $id_user)->first();
            $data->foto_ktp = $url_photo;
            $data->save();
        } else {
            $data = Users::where('id', $id_user)->first();
            $data->foto_selfie = $url_photo;
            $data->save();
        }

        CommonHelper::showAlert("Sukses", "Upload Photo berhasil", "success", "/admin/master_user/" . $id_user);
    }

    public function detailCoach($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $data_user = Users::where("id", $id)->first();
        $roles = [1, 5];
        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //SUPERADMIN
            $data_pegawai = Users::whereIn("role", $roles)->get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_pegawai = Users::whereIn("users.role", $roles)
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->select("users.*")
                ->get();
        } else if ($role->jabatan == 1) {
            //PEGAWAI
            $work_at = MasterKantor::where("id_kantor", $initialData->payload->id_kantor)->first();

            $data_pegawai = Users::whereIn("users.role", $roles)
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("users.*")
                ->get();
        }
        return view(
            "Admin.User.detailcoach",
            [
                "data_user" => $data_user,
                "dataUser" => $initialData->payload,
                "dataPegawai" => $data_pegawai
            ]
        );
    }

    public function doBatalCuti(Request $request)
    {
        $id = $request->id;

        $data = JadwalCuti::where("id", $id)->first();
        $data->delete();

        CommonHelper::showAlert("Sukses", "Cuti Berhasil Dibatalkan", "success", "/admin/list_cuti");
    }

    public function laporan_daily($id)
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        $data_user = Users::where("id", $id)->first();
        return view(
            "Admin.User.detaillaporan",
            [
                "data_user" => $data_user,
                "dataUser" => $initialData->payload
            ]
        );
    }
}
