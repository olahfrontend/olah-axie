<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HakAkses;
use App\Models\MasterRole;
use Illuminate\Support\Facades\Route;

class LoginController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (isset($initialData)) {
            if ($initialData->payload->role == 2) {
                return redirect("/admin/dashboard");
            } else {
                return view("Admin.login");
            }
        }
        return view("Admin.login");
    }

    public function dologin()
    {
        $newRequest = Request::create('/api/user/login', 'POST');
        $response = Route::dispatch($newRequest);
        // print_r($response);
        $res = json_decode($response->getContent());
        // echo $response->getStatusCode();
        if ($response->getStatusCode() == 200) {
            $menu = HakAkses::where("hak_akses.id_role", $res->payload->role)->join("master_menu", "master_menu.id", "hak_akses.id_menu")->orderBy("hak_akses.id_menu")->first();
            $menu->url = str_replace("admin/", "", $menu->url);

            if ($res->payload->role == 1 || $res->payload->role == 5) {
                CommonHelper::showAlert("Sign In Failed", "User not allowed", "error", "/admin");
            } else {
                echo '
                <script>
                    window.location.href = "' . strtolower($menu->url) . '";
                </script>';
            }
        } else {
            CommonHelper::showAlert("Sign In Failed", $res->error_msg, "error", "/admin");
        }
    }
}
