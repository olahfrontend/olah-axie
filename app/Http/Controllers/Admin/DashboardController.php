<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\LogMMR;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Users;
use Carbon\Carbon;

class DashboardController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        if ($initialData->payload->role == 1) {
            return redirect("/admin");
        }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($role->id == 2) {
            $data_owner = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_owner = Users::where("id", $initialData->payload->id)->get();
        } else {
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_owner = Users::where("id", $work_at->id_owner)->get();
        }
        $data_dashboard = CommonHelper::getAPI("/api/get_dashboard");
        $data_axie = CommonHelper::getAPI("/api/axie/list");
        return view("Admin.Dashboard.index", [
            "dataUser" => $initialData->payload,
            "dataOwner" => $data_owner,
            "dataDashboard" => $data_dashboard->payload,
            "dataAxie" => $data_axie
        ]);
    }

    public function detail(Request $request)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        if ($initialData->payload->role == 1) {
            return redirect("/admin");
        }

        $data = CommonHelper::getAPI("api/laporan/laporan_pegawai");

        // print_r($data);
        return view("Admin.Dashboard.detaildashboard", [
            "dataUser" => $initialData->payload,
            "data" => $data->payload
        ]);
    }

    public function laporan_daily_report()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        $data = CommonHelper::getAPI("api/laporan/laporan_daily_monthly");
        return view("Admin.Dashboard.laporandailyreport", [
            "dataUser" => $initialData->payload,
            "data" => $data->payload
        ]);
    }

    public function axie_live()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        $data_axie = CommonHelper::getAPI("/api/axie/list");
        // $data_kantor = MasterKantor::get();
        $data_owner = Users::where("role", 3)->where("status", 1)->get();
        // print_r($data_axie);
        // $data_axie = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
        //     ->where('master_axie.id_owner', 59)
        //     ->leftJoin("users", "users.id", "user_axie.id_user")
        //     ->select("master_axie.*", "users.name")
        //     ->get();
        return view("Admin.AxieTracker.index", [
            "dataUser" => $initialData->payload,
            "dataAxie" => $data_axie,
            // "dataKantor" => $data_kantor,
            "dataOwner" => $data_owner
        ]);
    }

    public function search_axie_tracker()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        $data_owner = Users::where("role", 3)->where("status", 1)->get();
        return view("Admin.AxieTracker.search", [
            "dataUser" => $initialData->payload,
            "dataOwner" => $data_owner
        ]);
    }

    public function detail_axie_tracker($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        $data_axie = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
            ->leftJoin("users", "users.id", "user_axie.id_user")
            ->where("master_axie.id", $id)
            ->select("users.name", "master_axie.*", "users.id as id_user")
            ->first();

        $data_daily = DailyReport::where("id_user", $data_axie->id_user)
            ->where("id_axie", $id)
            ->whereNotNull("level_before")
            ->orderBy("created_at", "desc")
            ->limit(7)
            ->get();

        $data_log_mmr = LogMMR::where("id_axie", $id)
            ->orderBy("created_at", "desc")
            ->limit(7)
            ->get();

        $total_slp = 0;
        $total_play = 0;
        foreach ($data_daily as $dt) {
            $dt->date = Carbon::createFromFormat("Y-m-d H:i:s", $dt->created_at)->format("Y/m/d");
            $total_slp += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
            $total_play++;
        }

        $average_gain_slp = 0;
        if ($total_play > 0) {
            $average_gain_slp = number_format($total_slp / $total_play, 0);
        }

        foreach ($data_log_mmr as $dt) {
            $dt->date = Carbon::createFromFormat("Y-m-d H:i:s", $dt->created_at)->format("Y/m/d");
        }



        return view("Admin.AxieTracker.detail", [
            "dataUser" => $initialData->payload,
            "data_axie" => $data_axie,
            "data_daily" => $data_daily,
            "average_gain_slp" => $average_gain_slp,
            "data_log_mmr" => $data_log_mmr
        ]);
    }
}
