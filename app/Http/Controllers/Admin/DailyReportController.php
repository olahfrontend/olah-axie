<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\Gaji;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Setting;
use App\Models\UserAxie;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use stdClass;

class DailyReportController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5) {
        //     return redirect("/admin");
        // }

        $data = CommonHelper::getAPI("/api/report/daily_report_admin");
        $data_belum_report = [];
        $data_sudah_report = [];
        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //SUPERADMIN
            $data_user_axie = UserAxie::get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_user_axie = UserAxie::join("users", "users.id", "user_axie.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->select("user_axie.*")
                ->get();
        } else if ($role->jabatan == 1) {
            //ADMIN & PENGAWAS
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_user_axie = UserAxie::join("users", "users.id", "user_axie.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("user_axie.*")
                ->get();
        }
        $temp1 = 0;
        $temp2 = 0;
        foreach ($data_user_axie as $dt) {
            $temp = DailyReport::where("id_axie", $dt->id_axie)
                ->where("id_user", $dt->id_user)
                ->whereDate("created_at", Carbon::today())
                ->first();

            // echo Response::json($temp->payload);
            if (!isset($temp)) {
                $data_belum_report[$temp1] = new stdClass();
                $data_belum_report[$temp1] = UserAxie::where("user_axie.id", $dt->id)
                    ->join("master_axie", "master_axie.id", "user_axie.id_axie")
                    ->join("users", "users.id", "user_axie.id_user")
                    ->get();
                // print_r($temp);
                $temp1++;
            } else {
                if ($temp->status == 1) {
                    $data_sudah_report[$temp2] = new stdClass();
                    // echo "a ".$temp->id;
                    $data_sudah_report[$temp2] = UserAxie::where("user_axie.id", $dt->id)
                        ->join("master_axie", "master_axie.id", "user_axie.id_axie")
                        ->join("users", "users.id", "user_axie.id_user")
                        ->first();
                    $data_sudah_report[$temp2]->id_daily_report = $temp->id;
                    $temp2++;
                }
            }
        }


        // echo count($data_belum_report);
        // $databelumreport = Response::json($data_belum_report);
        // echo Response::json($data_belum_report);
        // print_r($data_belum_report[1][0]);
        // print("<pre>" . print_r($data_sudah_report, true) . "</pre>");
        return view("Admin.DailyReport.index", [
            "data" => $data->payload,
            "dataUser" => $initialData->payload,
            "data_belum_report" => $data_belum_report,
            "data_sudah_report" => $data_sudah_report
        ]);
    }

    public function detail($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1) {
        //     return redirect("/admin");
        // }

        Input::merge([
            "id" => $id
        ]);
        $data = CommonHelper::getAPI("/api/report/daily_report_admin");
        return view("Admin.DailyReport.detail", [
            "data" => $data->payload,
            "dataUser" => $initialData->payload
        ]);
    }

    public function approve()
    {
        $newRequest = Request::create('/api/report/approve', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Update Report Berhasil", "success", "/admin/daily_report");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }
    
	public function adjustment(Request $request){
		$id_daily_report = $request->id_daily_report;
		$id_axie = $request->id_axie;
		$gaji = $request->gaji;
		$pvp = $request->slp_pvp;
		$adv = $request->slp_adventure;
		$quest = $request->slp_quest;
		$last_inventory = $request->last_inventory;
		
		$daily_report = DailyReport::where("id",$id_daily_report)->first();
		$daily_report->slp_adventure = $adv;
		$daily_report->slp_quest = $quest;
		$daily_report->slp_pvp = $pvp;
		$daily_report->last_inventory = $last_inventory;
		$daily_report->save();
		
		$data_gaji = Gaji::where("id_daily_report",$id_daily_report)->first();
		$data_gaji->gaji = $gaji;
		$data_gaji->save();
		
		$axie = MasterAxie::where("id",$id_axie)->first();
		$axie->total_slp = $last_inventory;
		$axie->save();
		
        CommonHelper::showAlert("Sukses", "Update Report Berhasil", "success", "/admin/master_axie/".$id_axie);
		
	}
}
