<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Users;
use Illuminate\Support\Facades\Route;

class MasterKantorController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($role->id == 2) {
            //SUPERADMIN
            $data_kantor = MasterKantor::join("users", "users.id", "master_kantor.id_owner")
                ->select("users.name", "master_kantor.*")
                ->get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_kantor = MasterKantor::join("users", "users.id", "master_kantor.id_owner")
                ->where("master_kantor.id_owner", $initialData->payload->id)
                ->select("users.name", "master_kantor.*")
                ->get();
        } else {
            //PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_kantor = MasterKantor::join("users", "users.id", "master_kantor.id_owner")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("users.name", "master_kantor.*")
                ->get();
        }
        // else if ($initialData->payload->role == 3) {
        //     $data_kantor = MasterKantor::join("users", "users.id", "master_kantor.id_owner")
        //         ->where("master_kantor.id_owner", $initialData->payload->id)
        //         ->select("users.name", "master_kantor.*")
        //         ->get();
        // }
        return view("Admin.Kantor.index", [
            "data_kantor" => $data_kantor,
            "dataUser" => $initialData->payload,
        ]);
    }

    public function add()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1) {
        //     return redirect("/admin");
        // }


        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //SUPERADMIN
            $data_owner = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_owner = Users::where("id", $initialData->payload->id)->get();
        } else {
            //PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_owner = Users::where("id", $work_at->id_owner)->get();
        }
        return view("Admin.Kantor.add", [
            "data_owner" => $data_owner,
            "dataUser" => $initialData->payload
        ]);
    }

    public function doadd()
    {
        $newRequest = Request::create('/api/add_kantor', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Add Kantor Berhasil", "success", "/admin/master_kantor");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function detail($id)
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1) {
        //     return redirect("/admin");
        // }

        $data = MasterKantor::where("id", $id)
            ->first();

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            //SUPERADMIN
            $data_owner = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 2) {
            //OWNER
            $data_owner = Users::where("id", $initialData->payload->id)->get();
        } else {
            //PEGAWAI
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_owner = Users::where("id", $work_at->id_owner)->get();
        }
        // echo json_encode($data);
        return view("Admin.Kantor.detail", [
            "data" => $data,
            "data_owner" => $data_owner,
            "dataUser" => $initialData->payload
        ]);
    }

    public function doedit(Request $request)
    {
        $data = MasterKantor::where("id", $request->id)->first();
        $data->alamat = $request->alamat;
        $data->id_owner = $request->id_owner;
        $data->save();

        CommonHelper::showAlert("Sukses", "Sukses Edit Kantor", "success", "/admin/master_kantor");
    }
}
