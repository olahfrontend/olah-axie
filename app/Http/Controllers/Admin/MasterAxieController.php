<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Users;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class MasterAxieController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        if ($initialData->payload->role == 1) {
            return redirect("/admin");
        }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            $data = CommonHelper::getAPI("/api/axie/list");
            $data_history = DailyReport::where("daily_report.slp_adventure", null)
                ->where("daily_report.slp_pvp", null)
                ->where("daily_report.slp_quest", "<>", null)
                ->join("users", "users.id", "daily_report.id_user")
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->select("master_axie.email_akun", "users.name", "daily_report.*")
                ->get();
        } else if ($role->jabatan == 2) {

            $data = CommonHelper::getAPI("/api/axie/list");
            $data_history = DailyReport::where("daily_report.slp_adventure", null)
                ->where("daily_report.slp_pvp", null)
                ->where("daily_report.slp_quest", "<>", null)
                ->join("users", "users.id", "daily_report.id_user")
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->where("master_axie.id_owner", $initialData->payload->id)
                ->select("master_axie.email_akun", "users.name", "daily_report.*")
                ->get();
        } else if ($role->jabatan == 1) {
            $data = CommonHelper::getAPI("/api/axie/list");
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_history = DailyReport::where("daily_report.slp_adventure", null)
                ->where("daily_report.slp_pvp", null)
                ->where("daily_report.slp_quest", "<>", null)
                ->join("users", "users.id", "daily_report.id_user")
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->where("master_axie.id_owner", $work_at->id_owner)
                ->select("master_axie.email_akun", "users.name", "daily_report.*")
                ->get();
        }
        return view("Admin.Axie.index", [
            "data" => $data->payload,
            "dataUser" => $initialData->payload,
            "data_history" => $data_history
        ]);
    }

    public function add()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            $data_owner = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_owner = Users::where("id", $work_at->id_owner)->get();
        } else if ($role->jabatan == 2) {
            $data_owner = Users::where("id", $initialData->payload->id)->get();
        }
        return view("Admin.Axie.add", [
            "data_owner" => $data_owner,
            "dataUser" => $initialData->payload
        ]);
    }

    public function doadd()
    {
        $newRequest = Request::create('/api/axie/add', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Add Axie Berhasil", "success", "/admin/master_axie");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function detail($id)
    {

        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        if ($initialData->payload->role == 1) {
            return redirect("/admin");
        }

        Input::merge([
            "id" => $id,
            "id_axie" => $id
        ]);
        $data = CommonHelper::getAPI("/api/axie/detail");
        $dataPegawai = CommonHelper::getAPI("/api/user/list");
        $data_report = CommonHelper::getAPI("/api/report/history_slp_axie");
        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($role->id == 2) {
            $data_owner = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_owner = Users::where("id", $work_at->id_owner)->get();
        } else if ($role->jabatan == 2) {
            $data_owner = Users::where("id", $initialData->payload->id)->get();
        }
        // echo json_encode($data);
        return view("Admin.Axie.detail", [
            "data" => $data->payload,
            "dataPegawai" => $dataPegawai->payload,
            "dataReport" => $data_report->payload,
            "data_owner" => $data_owner,
            "dataUser" => $initialData->payload
        ]);
    }

    public function doedit(Request $request)
    {
        $newRequest = Request::create('/api/axie/update', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Edit Axie Berhasil", "success", "/admin/master_axie");
        } else {
            CommonHelper::showAlert("Error", $response->getStatusCode(), "error", "back");
        }
    }

    public function takeAxie(Request $request)
    {
        if ($request->id_user == -1) {
            Input::merge([
                "id_user" => $request->id_user_aktif,
                "type" => 2
            ]);
        } else {
            Input::merge([
                "type" => 1
            ]);
        }
        echo $request->id_axie . "\n";
        echo $request->id_user . "\n";
        echo $request->type;
        $newRequest = Request::create('/api/axie/take', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", $res->error_msg, "success", "/admin/master_axie");
        } else {
            CommonHelper::showAlert("Error", $res->error_msg, "error", "back");
        }
    }

    public function doTopupSLP(Request $request)
    {
        $newRequest = Request::create('/api/axie/topup_slp', 'POST');
        $response = Route::dispatch($newRequest);
        // $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Topup Berhasil", "success", "back");
        } else {
            CommonHelper::showAlert("Error", $response->getStatusCode(), "error", "back");
        }
    }

    public function doWithdrawSLP(Request $request)
    {
        $newRequest = Request::create('/api/axie/withdraw_slp', 'POST');
        $response = Route::dispatch($newRequest);
        // $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Withdraw Berhasil", "success", "back");
        } else {
            CommonHelper::showAlert("Error", $response->getStatusCode(), "error", "back");
        }
    }
}
