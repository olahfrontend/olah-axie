<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LogServer;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Setting;
use App\Models\Users;
use Illuminate\Support\Facades\Route;

class MasterSettingController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        // if ($initialData->payload->role == 1 || $initialData->payload->role == 5 || $initialData->payload->role == 4) {
        //     return redirect("/admin");
        // }

        $role = MasterRole::where("id", $initialData->payload->role)->first();
        if ($initialData->payload->role == 2) {
            $data_user = Users::where("role", 3)->where("status", 1)->get();
        } else if ($role->jabatan == 2) {
            $data_user = Users::where("id", $initialData->payload->id)
                ->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $initialData->payload->id_kantor)->first();
            $data_user = Users::where("id", $work_at->id_owner)->get();
        }

        // print_r($initialData);
        //ID USER SUPERADMIN = 2
        $data_setting_admin = Setting::where("id_user", 2)->get();
        // print_r($data_setting_admin);
        return view("Admin.Setting.index", [
            "data_user" => $data_user,
            "dataUser" => $initialData->payload,
            "data_setting_superadmin" => $data_setting_admin
        ]);
    }

    public function updateGeneral(Request $request)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        $id_setting = $request->id_setting;
        $value = $request->value;


        $data = Setting::where("id", $id_setting)->first();
        $data->value = $value;
        $data->save();

        $log = new LogServer();
        if ($data->nama_setting == "SETTING_SERVER_AXIE") {
            if ($data->value == 1) {
                $value = "NORMAL";
            } else {
                $value = "ERROR";
            }
        }
        $log->description = strtoupper($initialData->payload->email) . " MENGUPDATE SETTING " . strtoupper($data->nama_setting) . " MENJADI " . $value;

        if (isset($request->alasan) && $request->alasan != "" && $value == "ERROR") {
            $log->description = strtoupper($initialData->payload->email) . " MENGUPDATE SETTING " . strtoupper($data->nama_setting) . " MENJADI " . $value . " DENGAN ALASAN " . strtoupper($request->alasan);
        }
        $log->save();
        CommonHelper::showAlert("Sukses", "Update Setting Berhasil", "success", "/admin/master_setting");
    }

    public function do_add_setting()
    {
        $newRequest = Request::create('/api/add_setting_level', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Add Setting Level Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res, "error", "back");
        }
    }

    public function do_update_setting()
    {
        $newRequest = Request::create('/api/edit_setting_level', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Edit Setting Level Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function do_delete_setting()
    {
        $newRequest = Request::create('/api/delete_setting_level', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Delete Setting Level Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }


    public function do_add_setting_pvp()
    {
        $newRequest = Request::create('/api/add_setting_pvp', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Add Setting PVP Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res, "error", "back");
        }
    }

    public function do_update_setting_pvp()
    {
        $newRequest = Request::create('/api/edit_setting_pvp', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Edit Setting PVP Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }

    public function do_delete_setting_pvp()
    {
        $newRequest = Request::create('/api/delete_setting_pvp', 'POST');
        $response = Route::dispatch($newRequest);
        $res = json_decode($response->getContent());
        if ($response->getStatusCode() == 200) {
            CommonHelper::showAlert("Sukses", "Delete Setting PVP Berhasil", "success", "/admin/master_setting");
        } else {
            CommonHelper::showAlert("Gagal", $res->error_msg, "error", "back");
        }
    }
}
