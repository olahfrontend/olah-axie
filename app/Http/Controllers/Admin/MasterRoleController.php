<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\HakAkses;
use App\Models\MasterMenu;
use App\Models\MasterRole;
use stdClass;

class MasterRoleController extends Controller
{
    //
    public function index()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }

        $data_role = MasterRole::get();

        return view("Admin.HakAkses.index", [
            "data_role" => $data_role,
            "dataUser" => $initialData->payload,
        ]);
    }

    public function detail($id)
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        $detail_role = MasterRole::where("master_role.id", $id)->first();
        $data_role = MasterRole::where("master_role.id", $id)
            ->join("hak_akses", "hak_akses.id_role", "master_role.id")
            ->join("master_menu", "master_menu.id", "hak_akses.id_menu")
            ->select("master_role.nama_role", "master_menu.id", "master_menu.nama_menu")
            ->get();
        $data_menu = MasterMenu::get();

        // print_r($data_role);
        // echo json_encode($data_role);
        $data = new stdClass();
        $data->nama_role = $detail_role->nama_role;
        $data->jabatan = $detail_role->jabatan;
        $data->id = $detail_role->id;
        if (isset($data_role)) {
            $data->data_hak_akses = $data_role;
        }
        // echo json_encode($data);
        return view("Admin.HakAkses.detail", [
            "dataUser" => $initialData->payload,
            "data_role" => $data,
            "data_menu" => $data_menu
        ]);
    }

    public function viewadd()
    {
        $initialData = CommonHelper::checkSession();
        if (!isset($initialData)) {
            return redirect("/admin");
        }
        $data_menu = MasterMenu::get();

        // echo json_encode($data);
        return view("Admin.HakAkses.add", [
            "dataUser" => $initialData->payload,
            "data_menu" => $data_menu
        ]);
    }

    public function doadd(Request $request)
    {
        $selected_role = $request->checkbox;
        $nama_role = $request->nama_role;
        $jabatan = $request->jabatan;

        $role = new MasterRole();
        $role->nama_role = $nama_role;
        $role->jabatan = $jabatan;
        $role->save();

        $submenu = [];
        foreach ($selected_role as $sel) {
            $menu = MasterMenu::where("id", $sel)->first();
            if ($menu->id_parent_menu != null) {
                if (!in_array($menu->id_parent_menu, $submenu)) {
                    array_push($submenu, $menu->id_parent_menu);
                }
            }
            $data = new HakAkses();
            $data->id_menu = $sel;
            $data->id_role = $role->id;
            $data->save();
        }

        foreach ($submenu as $sub) {
            $data = new HakAkses();
            $data->id_menu = $sub;
            $data->id_role = $id_role;
            $data->save();
        }
        CommonHelper::showAlert("Sukses", "Add Role Berhasil", "success", "/admin/hak_akses");
    }

    public function doedit(Request $request)
    {
        $id_role = $request->id_role;
        $selected_role = $request->checkbox;
        $nama_role = $request->nama_role;
        $jabatan = $request->jabatan;

        $role = MasterRole::where("id", $id_role)->first();
        $role->nama_role = $nama_role;
        $role->jabatan = $jabatan;
        $role->save();

        $data = HakAkses::where("id_role", $id_role);
        $data->delete();

        $submenu = [];
        foreach ($selected_role as $sel) {
            $menu = MasterMenu::where("id", $sel)->first();
            if ($menu->id_parent_menu != null) {
                if (!in_array($menu->id_parent_menu, $submenu)) {
                    array_push($submenu, $menu->id_parent_menu);
                }
            }
            $data = new HakAkses();
            $data->id_menu = $sel;
            $data->id_role = $id_role;
            $data->save();
        }

        foreach ($submenu as $sub) {
            $data = new HakAkses();
            $data->id_menu = $sub;
            $data->id_role = $id_role;
            $data->save();
        }

        CommonHelper::showAlert("Sukses", "Update Hak Akses Berhasil", "success", "/admin/hak_akses");
    }
}
