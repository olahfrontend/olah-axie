<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DendaInventori;
use App\Models\Gaji;
use App\Models\LogServer;
use Illuminate\Support\Facades\Auth;

class DendaInventoriWebServices extends Controller
{
    //
    public function index()
    {
        $data = DendaInventori::get();
        return $this->createSuccessMessage($data);
    }

    public function addDenda(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        // if ($user->role != 2 || $user->role != 3 || $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        $data = new DendaInventori();
        $data->id_user = $request->id_user;
        $data->description = $request->description;
        $data->total_denda = $request->total_denda;
        $data->save();

        $gaji = new Gaji();
        $gaji->id_user = $request->id_user;
        $gaji->id_daily_report = null;
        $gaji->gaji = $request->total_denda * -1;
        $gaji->type = "DENDA_INVENTARIS";
        $gaji->id_axie = null;
        $gaji->id_denda = $data->id;
        $gaji->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " CREATED DENDA UNTUK ID_USER " . $data->id_user . " KETERANGAN " . $data->description . " DENGAN NOMINAL " . $data->nominal;
        $log->save();

        return $this->createSuccessMessage($gaji);
    }
}
