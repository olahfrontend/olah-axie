<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\Setting;
use App\Models\SettingLevel;
use App\Models\SettingPVP;
use App\Models\UserAxie;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SettingWebServices extends Controller
{
    //

    public function get_setting(Request $request)
    {
        $data = Setting::where("id_user", $request->id_user)->get();

        return $this->createSuccessMessage($data);
    }

    public function get_setting_level(Request $request)
    {
        $data = SettingLevel::where("id_user", $request->id_user)->get();
        return $this->createSuccessMessage($data);
    }

    public function add_setting_level(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $data = new SettingLevel();
        $data->id_user = $request->id_user;
        $data->min_level = $request->min_level;
        $data->max_level = $request->max_level;
        $data->min_slp = $request->min_slp;
        $data->value = $request->value;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENAMBAH SETTING LEVEL ";
        $log->save();
        return $this->createSuccessMessage($data);
    }

    public function edit_setting_level(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $data = SettingLevel::where("id", $request->id)->first();
        $data->min_level = $request->min_level;
        $data->max_level = $request->max_level;
        $data->min_slp = $request->min_slp;
        $data->value = $request->value;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENGUBAH SETTING LEVEL ";
        $log->save();

        return $this->createSuccessMessage($data);
    }

    public function delete_setting_level(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        $data = SettingLevel::where("id", $request->id)->delete();
        // $data->delete();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENGHAPUS SETTING LEVEL ";
        $log->save();

        if ($data) {
            return $this->createSuccessMessage("Success Delete Data");
        } else {
            return $this->createErrorMessage("Failed Delete Data", 400);
        }
    }

    public function get_setting_pvp(Request $request)
    {
        $data = SettingPVP::where("id_user", $request->id_user)->get();
        return $this->createSuccessMessage($data);
    }

    public function add_setting_pvp(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        $data = new SettingPVP();
        $data->id_user = $request->id_user;
        $data->min_slp = $request->min_slp;
        $data->max_slp = $request->max_slp;
        $data->value = $request->value;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENAMBAH SETTING PVP";
        $log->save();

        return $this->createSuccessMessage($data);
    }

    public function edit_setting_pvp(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        // echo $request->ip();
        // echo $request->min_slp;
        $data = SettingPVP::where("id", $request->id)->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Setting not found", 400);
        }
        $data->min_slp = $request->min_slp;
        $data->max_slp = $request->max_slp;
        $data->value = $request->value;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENGUBAH SETTING PVP";
        $log->save();

        return $this->createSuccessMessage($data);
    }

    public function delete_setting_pvp(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        $data = SettingPVP::where("id", $request->id)->delete();
        // $data->delete();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " MENGHAPUS SETTING PVP";
        $log->save();
        if ($data) {
            return $this->createSuccessMessage("Success Delete Data");
        } else {
            return $this->createErrorMessage("Failed Delete Data", 400);
        }
    }

    public function test(Request $request)
    {
        $master_axie = MasterAxie::get();

        foreach ($master_axie as $axie) {
            $user_axie = UserAxie::where("id_axie", $axie->id)->first();
            if (isset($user_axie)) {
                $axie->id_player = $user_axie->id_user;
                $axie->save();
            }
        }

        // $today = Carbon::now()->subDays(1);
        // $now = Carbon::now();
        // $daily_report = DailyReport::where("id_user", $request->id_user)
        //     ->where("id_axie", $request->id_axie)
        //     ->whereBetween('created_at', [$today, $now])
        //     ->first();

        // return $daily_report;
    }
}
