<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\UserAxie;
use App\Models\Users;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AxieWebServices extends Controller
{
    //
    public function addAxie(Request $request)
    {

        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $user = Auth::user();
        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        $axie = MasterAxie::where("email_akun", $request->email_akun)->first();
        if (isset($axie)) {
            return $this->createErrorMessage("Axie already exist", 400);
        }

        $data = new MasterAxie();
        $data->email_akun = $request->email_akun;
        $data->negara = $request->negara;
        $data->level = $request->level;
        $data->status_terpakai = 0;
        $data->id_owner = $request->id_owner;
        $data->address_ronin = $request->address_ronin;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " ADD NEW AXIE " . strtoupper($data->email_akun);
        $log->save();

        return $this->createSuccessMessage("Add Axie Sukses");
    }

    public function takeAxie(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $user = Auth::user();
        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        $pegawai = Users::where("id", $request->id_user)->first();
        if (!isset($pegawai)) {
            return $this->createErrorMessage("User tidak ditemukan", 400);
        }

        $data = MasterAxie::where("id", $request->id_axie)->first();
        $axie = $data;

        if (!isset($data)) {
            return $this->createErrorMessage("Axie tidak ditemukan", 400);
        }

        $now = Carbon::now();
        $now_hour = Carbon::parse($now)->format("H");

        if ($now_hour < 9) {
            $day = [Carbon::today()->subDays(1), Carbon::now()];
        } else {
            $day = [Carbon::today(), Carbon::now()];
        }

        $checkPlayed = DailyReport::whereBetween("created_at", $day)->where("id_axie", $axie->id)->where("slp_adventure", "<>", "null")->first();

        if (isset($checkPlayed)) {
            return $this->createErrorMessage("Axie telah dimainkan hari ini, Silahkan pindahkan axie besok", 400);
        }

        //CEK AXIE BEDA OWNER GA
        $kantor = MasterKantor::where("id", $pegawai->id_kantor)->first();
        if ($kantor->id_owner != $axie->id_owner) {
            return $this->createErrorMessage("Axie beda owner dengan pemain", 400);
        }

        if ($data->status_terpakai != 0 && $request->type == 1) {
            $user_axie = UserAxie::where("id_axie", $request->id_axie)->first();
            $pegawai_sebelum = Users::where("id", $user_axie->id_user)->first();
            $dt = DB::table('user_axie')->where("id_axie", $request->id_axie)->delete();
            $log = new LogServer();
            $log->description = strtoupper($user->email) . " TAKE AXIE " . strtoupper($axie->email_akun) . " DARI " . strtoupper($pegawai_sebelum->email);
            $log->save();
            // return $this->createErrorMessage("Axie sudah diambil", 400);
        }
        if ($request->type == 2 && $data->status_terpakai != 1) {
            return $this->createErrorMessage("Axie belum diambil", 400);
        }
        if ($request->type == 1) {
            //TAKE AXIE
            $data->status_terpakai = 1;
            $data->id_player = $request->id_user;
            $data->save();

            $data = new UserAxie();
            $data->id_axie = $request->id_axie;
            $data->id_user = $request->id_user;
            $data->save();

            $log = new LogServer();
            $log->description = strtoupper($user->email) . " GIVE AXIE " . strtoupper($axie->email_akun) . " KE " . strtoupper($pegawai->email);
            $log->save();

            return $this->createSuccessMessage($data, 200, "Take Axie Berhasil");
        } else if ($request->type == 2) {
            //UNTAKE AXIE
            $data->status_terpakai = 0;
            $data->id_player = null;
            $data->save();

            $data = UserAxie::where("id_user", $request->id_user)
                ->where("id_axie", $request->id_axie)
                ->delete();

            $log = new LogServer();
            $log->description = strtoupper($user->email) . " TAKE AXIE " . strtoupper($axie->email_akun) . " DARI " . strtoupper($pegawai->email);
            $log->save();

            return $this->createSuccessMessage(null, 200, "Untake Berhasil");
        } else {
            return $this->createErrorMessage("Type tidak exist", 400);
        }
    }

    public function updateLevel(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();


        $data = MasterAxie::where("id", $request->id_axie)->first();
        if (!isset($data)) {
            return $this->createErrorMessage("Axie tidak ditemukan", 400);
        }

        $check = UserAxie::where("id_user", $user->id)
            ->where("id_axie", $request->id_axie)
            ->first();

        if (!isset($check)) {
            return $this->createErrorMessage("Axie Bukan Milik User", 400);
        }

        $data->level = $request->level;
        $data->save();

        return $this->createSuccessMessage($data);
    }


    public function getListAxie(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $role = MasterRole::where("id", $user->role)->first();
        if ($user->role == 2) {
            if (isset($request->status)) {
                $data = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
                    ->leftJoin("users", "users.id", "user_axie.id_user")
                    ->where("master_axie.status_terpakai", $request->status)
                    ->select("master_axie.*", "users.name")
                    ->get();
            } else {
                $data = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
                    ->leftJoin("users", "users.id", "user_axie.id_user")
                    ->select("master_axie.*", "users.name")
                    ->get();
            }
        } else if ($role->jabatan == 2) {
            //OWNER
            $data = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
                ->leftJoin("users", "users.id", "user_axie.id_user")
                ->where("master_axie.id_owner", $user->id)
                ->select("master_axie.*", "users.name")
                ->get();
        } else if ($user->role == 7) {
            //COACH
            $data = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
                ->leftJoin("users", "users.id", "user_axie.id_user")
                ->leftJoin("couch_users", "couch_users.id_user", "users.id")
                ->where("couch_users.id_coach", $user->id)
                ->select("master_axie.*", "users.name")
                ->get();
        } else if ($role->jabatan == 1) {
            //ADMIN
            $work_at = MasterKantor::where("id", $user->id_kantor)->first();
            $data = MasterAxie::leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
                ->leftJoin("users", "users.id", "user_axie.id_user")
                ->where("master_axie.id_owner", $work_at->id_owner)
                ->select("master_axie.*", "users.name")
                ->get();
        }

        return $this->createSuccessMessage($data);
    }

    public function myAxie()
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $data = UserAxie::join("master_axie", "master_axie.id", "user_axie.id_axie")
            ->where("user_axie.id_user", $user->id)
            ->get();

        return $this->createSuccessMessage($data);
    }

    public function getDetailAxie(Request $request)
    {
        $data = MasterAxie::where("master_axie.id", $request->id)
            ->leftJoin("user_axie", "user_axie.id_axie", "master_axie.id")
            ->leftJoin("users", "users.id", "user_axie.id_user")
            ->select("master_axie.*", "users.name", "users.id as user_id")
            ->first();

        return $this->createSuccessMessage($data);
    }

    public function updateAxie(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        // if ($user->role !== 2) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }
        $data = MasterAxie::where("id", $request->id)->first();
        $data->email_akun = $request->email_akun;
        $data->negara = $request->negara;
        $data->level = $request->level;
        $data->id_owner = $request->id_owner;
        $data->address_ronin = $request->address_ronin;
        $data->status_boss = $request->status_boss;
        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " UPDATED AXIE " . strtoupper($data->email_akun);
        $log->save();
        return $this->createSuccessMessage($data);
    }

    public function topupSLP(Request $request)
    {

        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }
        $axie = MasterAxie::where("id", $request->id_axie)->first();
        if (!isset($axie)) {
            return $this->createErrorMessage("Axie not found", 400);
        }
        $data = new DailyReport();
        $data->id_user = $user->id;
        $data->id_axie = $request->id_axie;
        $data->slp_adventure = null;
        $data->slp_pvp = null;
        $data->slp_quest = $request->amount;
        $data->last_inventory = $axie->total_slp + $request->amount;
        $data->status = 1;
        $data->save();

        $axie->total_slp += $request->amount;
        $axie->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " TOPUP SLP SEBANYAK " . $request->amount . " DI AXIE " . strtoupper($axie->email_akun);
        $log->save();
        return $this->createSuccessMessage($data);
    }

    public function withdrawSLP(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }
        $axie = MasterAxie::where("id", $request->id_axie)->first();
        if (!isset($axie)) {
            return $this->createErrorMessage("Axie not found", 400);
        }

        // if ($request->amount > $axie->total_slp) {
        //     return $this->createErrorMessage("Insufficient Balance", 400);
        // }
        $data = new DailyReport();
        $data->id_user = $user->id;
        $data->id_axie = $request->id_axie;
        $data->slp_adventure = null;
        $data->slp_pvp = null;
        $data->slp_quest = $request->amount * -1;
        $data->last_inventory = $axie->total_slp - $request->amount;
        $data->status = 1;
        $data->save();

        $axie->total_slp -= $request->amount;
        $axie->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " WITHDRAW SLP SEBANYAK " . $request->amount . " DI AXIE " . strtoupper($axie->email_akun);
        $log->save();
        return $this->createSuccessMessage($data);
    }

    public function searchAxie(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $user = Auth::user();

        $searchVal = $request->search;

        $role = MasterRole::where("id", $user->role)->first();
        if ($user->role == 2) {
            //SUPERADMIN
            if (isset($request->owner) && $request->owner != -1) {
                $data = MasterAxie::join("user_axie", "user_axie.id_axie", "master_axie.id")
                    ->join("users", "users.id", "user_axie.id_user")
                    ->where("master_axie.email_akun", "like", "%" . $searchVal . "%")
                    ->where("master_axie.id_owner", $request->owner)
                    ->orWhere("users.name", "like", "%" . $searchVal . "%")
                    ->where("master_axie.id_owner", $request->owner)
                    ->select("master_axie.*", "users.name")
                    ->get();
            } else {
                $data = MasterAxie::join("user_axie", "user_axie.id_axie", "master_axie.id")
                    ->join("users", "users.id", "user_axie.id_user")
                    ->where("master_axie.email_akun", "like", "%" . $searchVal . "%")
                    ->orWhere("users.name", "like", "%" . $searchVal . "%")
                    ->select("master_axie.*", "users.name")
                    ->get();
            }
        } else if ($role->jabatan == 2) {
            //OWNER
            $data = MasterAxie::join("user_axie", "user_axie.id_axie", "master_axie.id")
                ->join("users", "users.id", "user_axie.id_user")
                ->where("master_axie.email_akun", "like", "%" . $searchVal . "%")
                ->where("master_axie.id_owner", $user->id)
                ->orWhere("users.name", "like", "%" . $searchVal . "%")
                ->where("master_axie.id_owner", $user->id)
                ->select("master_axie.*", "users.name")
                ->get();
        } else if ($user->role == 7) {
            //COACH
            $data = MasterAxie::join("user_axie", "user_axie.id_axie", "master_axie.id")
                ->join("users", "users.id", "user_axie.id_user")
                ->join("couch_users", "couch_users.id_user", "users.id")
                ->where("master_axie.email_akun", "like", "%" . $searchVal . "%")
                ->where("couch_users.id_coach", $user->id)
                ->orWhere("users.name", "like", "%" . $searchVal . "%")
                ->where("couch_users.id_coach", $user->id)
                ->get();
        } else if ($role->jabatan == 1) {
            //PENGAWAS & ADMIN
            $work_at = MasterKantor::where("id", $user->id_kantor)->first();
            $data = MasterAxie::join("user_axie", "user_axie.id_axie", "master_axie.id")
                ->join("users", "users.id", "user_axie.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_axie.email_akun", "like", "%" . $searchVal . "%")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->orWhere("users.name", "like", "%" . $searchVal . "%")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("master_axie.*", "users.name")
                ->get();
        }

        return $this->createSuccessMessage($data);
    }
}
