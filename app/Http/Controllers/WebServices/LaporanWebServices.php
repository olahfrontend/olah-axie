<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\Gaji;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\Users;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use stdClass;

class LaporanWebServices extends Controller
{
    //
    public function laporan_slp_by_owner(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $data_axie = MasterAxie::where("id_owner", $request->id_owner)->get();

        $total_slp_unclaimed = MasterAxie::where("id_owner", $request->id_owner)
            ->sum("total_slp");
        $data_daily_report = MasterAxie::where("master_axie.id_owner", $request->id_owner)
            ->join("daily_report", "daily_report.id_axie", "master_axie.id")
            ->whereMonth("daily_report.created_at", "=", $month)
            ->whereYear("daily_report.created_at", "=", $year)
            ->select("daily_report.*")
            ->get();

        $total_slp_gained = 0;
        $total_slp_claimed = 0;

        $laporan_pegawai = [];
        $temp = 0;

        foreach ($data_daily_report as $dt) {
            $total = $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
            // echo $dt->id . " ";
            if ($total < 0) {
                // echo "minus\n";
                $total_slp_claimed -= $total;
            } else if (isset($dt->slp_adventure) && isset($dt->slp_pvp) && isset($dt->slp_quest) && $dt->status == 1) {
                // echo $dt->slp_adventure . " " . $dt->slp_pvp . " " . $dt->slp_quest . "\n";
                foreach ($data_axie as $d) {
                    if ($d->id == $dt->id_axie) {
                        $d->total_gain_slp += $total;
                        $total_slp_gained += $total;
                        break;
                    }
                }

                $bool = false;
                foreach ($laporan_pegawai as $d) {
                    if ($d->id == $dt->id_user) {
                        $d->total_gain += $total;
                        $bool = true;
                        break;
                    }
                }
                if (!$bool) {
                    $laporan_pegawai[$temp] = Users::where("id", $dt->id_user)->select("id", "name", "email")->first();
                    $laporan_pegawai[$temp]->total_gain = $total;
                    $temp++;
                }
            } else {
                // echo "topup \n";
            }
        }

        foreach ($laporan_pegawai as $lap) {
            $daily_report = DailyReport::where("id_user", $lap->id)
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->orderBy("created_at", "asc")
                ->get();
            $gaji = Gaji::where("id_user", $lap->id)
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->get();
            $data_pvp = Gaji::hitungBonusPVP($daily_report, $gaji, $month, $year);
            $last_withdraw = DailyReport::where("id_user", $lap->id)
                ->where("slp_pvp", ">", 0)
                ->orderBy("created_at", "DESC")
                ->first();
            if (isset($last_withdraw)) {
                $timestamp = strtotime($last_withdraw->created_at);
                $lap->last_update = date("Y-m-d H:i:s", $timestamp);
            } else {
                $lap->last_update = "-";
            }
            $lap->data_pvp = $data_pvp;
        }

        $data = new stdClass();
        $data->total_slp_unclaimed = $total_slp_unclaimed;
        $data->total_slp_gained = $total_slp_gained;
        $data->total_slp_claimed = $total_slp_claimed;
        $data->pegawai = $laporan_pegawai;
        $data->axie = $data_axie;

        return $this->createSuccessMessage($data);
    }

    public function laporan_pegawai(Request $request)
    {
        $array_month = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $id_user = $request->id;
        $month = $request->month;
        $year = $request->year;
        $id_owner = $request->id_owner;

        $data = Users::where("users.id", $id_user)
            ->join("daily_report", "daily_report.id_user", "users.id")
            ->join("master_axie", "master_axie.id", "daily_report.id_axie")
            ->whereMonth("daily_report.created_at", "=", $month)
            ->whereYear("daily_report.created_at", "=", $year)
            ->where("master_axie.id_owner", $id_owner)
            ->select("master_axie.email_akun", "master_axie.total_slp", "daily_report.*", "users.name")
            ->get();

        $result = new stdClass();
        $data_axie = [];
        $index = 0;
        foreach ($data as $dt) {
            $gain_slp = $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
            $bool = true;
            foreach ($data_axie as $d) {
                if ($d->id_axie == $dt->id_axie) {
                    $d->gain_slp += $gain_slp;
                    $d->total_pvp += $dt->slp_pvp;
                    $d->total_play++;
                    $bool = false;
                    break;
                }
            }
            if ($bool) {
                $data_axie[$index] = new stdClass();
                $data_axie[$index]->email_akun = $dt->email_akun;
                $data_axie[$index]->gain_slp = $gain_slp;
                $data_axie[$index]->unclaimed = $dt->total_slp;
                $data_axie[$index]->total_pvp = $dt->slp_pvp;
                $data_axie[$index]->total_play = 1;
                $data_axie[$index]->id_axie = $dt->id_axie;
                $index++;
            }
        }

        foreach ($data_axie as $dt) {
            $dt->avg_pvp = $dt->total_pvp / $dt->total_play;
            $dt->avg_slp = $dt->gain_slp / $dt->total_play;
            $last_withdraw = DailyReport::where("id_axie", $dt->id_axie)
                ->where("slp_quest", "<", 0)
                ->orderBy("created_at", "DESC")
                ->first();
            $total_withdraw = DailyReport::where("id_axie", $dt->id_axie)
                ->where("slp_quest", "<", 0)
                ->sum("slp_quest");

            if (isset($last_withdraw)) {
                $dt->last_withdraw = date('d M Y', strtotime($last_withdraw["created_at"]));
            } else {
                $dt->last_withdraw = null;
            }
            $dt->total_withdraw = $total_withdraw * -1;
        }

        if (count($data) > 0) {
            $result->name_pegawai = $data[0]->name;
        }
        $result->periode = $array_month[$month - 1] . " " . $year;
        $result->data_axie = $data_axie;

        return $this->createSuccessMessage($result);
    }

    public function laporan_daily_monthly(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $dateFormat = $request->year . '-' . $request->month . '-01';
        $format = 'Y-m-d';

        $data_axie = MasterAxie::where("id_owner", $request->id_owner)->get();

        $start_month = Carbon::createFromFormat($format, $dateFormat)->startOfMonth()->format("d");
        $end_month = Carbon::createFromFormat($format, $dateFormat)->endOfMonth()->format("d");

        $day = [];
        for ($i = $start_month; $i < $end_month; $i++) {
            $hari = $i;
            $bulan = $month;
            if ($hari < 10 && strlen($hari) < 2) {
                $hari = "0" . $hari;
            }
            if ($bulan < 10) {
                $bulan = "0" . $bulan;
            }
            array_push($day, $hari . '/' . $bulan . '/' . $year);
        }

        $data = [];
        $data = new stdClass();
        $data->array_tanggal = $day;
        foreach ($data_axie as $index => $dt) {
            $data->data[$index] = new stdClass();
            $data->data[$index]->nama_akun = $dt->email_akun;
            // $data[$index]->axie = $dt;
            $data_daily = DailyReport::join("users", "daily_report.id_user", "users.id")
                ->whereMonth("daily_report.created_at", "=", $month)
                ->whereYear("daily_report.created_at", "=", $year)
                ->where("daily_report.slp_pvp", "<>", null)
                ->where("daily_report.slp_adventure", "<>", null)
                ->where("daily_report.id_axie", $dt->id)
                ->select("daily_report.*", "users.name")
                ->orderBy("daily_report.created_at", "asc")
                ->get();
            $temp = [];
            foreach ($data_daily as $d) {
                $d->tanggal = date("d/m/Y", strtotime($d->created_at));
                array_push($temp, $d->tanggal);
            }
            $data_final = [];
            foreach ($day as $ind => $d) {
                if (array_search($d, $temp, false)) {
                    $data_final[$ind] = new stdClass();
                    $data_final[$ind]->tanggal = $d;
                    $data_final[$ind]->data_daily = $data_daily[array_search($d, $temp, false)];
                } else {
                    $data_final[$ind] = new stdClass();
                    $data_final[$ind]->tanggal = $d;
                    $data_final[$ind]->data_daily = null;
                }
                if (count($temp) > 0) {
                    if ($d == $temp[0]) {
                        $data_final[$ind] = new stdClass();
                        $data_final[$ind]->tanggal = $d;
                        $data_final[$ind]->data_daily = $data_daily[0];
                    }
                }
            }
            $data->data[$index]->daily = $data_final;
        }

        return $this->createSuccessMessage($data);
    }

    public function get_laporan_monthly(Request $request)
    {

        $month = $request->month;
        $year = $request->year;
        $id_user = $request->id_user;

        $data_daily = DailyReport::join("master_axie", "master_axie.id", "daily_report.id_axie")
            ->join("users", "daily_report.id_user", "users.id")
            ->whereMonth("daily_report.created_at", "=", $month)
            ->whereYear("daily_report.created_at", "=", $year)
            ->where("daily_report.slp_pvp", "<>", null)
            ->where("daily_report.slp_adventure", "<>", null)
            ->where("users.id", $id_user)
            ->select("daily_report.*", "master_axie.email_akun", "users.name")
            ->orderBy("daily_report.created_at", "asc")
            ->get();

        $index = 0;
        $data = [];
        foreach ($data_daily as $dt) {
            $i = -1;
            foreach ($data as $index => $d) {
                if ($d->tanggal == date("d/m/Y", strtotime($dt->created_at))) {
                    $i = $index;
                    break;
                }
            }
            if ($i == -1) {
                $i = count($data);
                $data[$i] = new stdClass();
                $data[$i]->tanggal = date("d/m/Y", strtotime($dt->created_at));
                $data[$i]->data[0] = $dt;
            } else {
                $data[$i]->data[count($data[$i]->data)] = $dt;
            }
        }
        return $this->createSuccessMessage($data);
    }

    public function get_laporan_gain_slp(Request $request)
    {
        $month = $request->month;
        $year = $request->year;
        $type = $request->type;
        $id_axie = $request->id_axie;

        if ($type == "daily") {
            if (Carbon::now()->format("m") == $month) {
                $awal = Carbon::now()->modify('first day of this month');
                $akhir = Carbon::now();
            } else {
                $dateFormat = $year . '-' . $month . '-01';
                $format = 'Y-m-d';
                $awal = Carbon::createFromFormat($format, $dateFormat)->modify('first day of this month');
                $akhir = Carbon::createFromFormat($format, $dateFormat)->modify('last day of this month');
            }

            $data = DailyReport::where("id_axie", $id_axie)
                ->whereBetween('created_at', [$awal, $akhir])
                ->where('slp_adventure', "<>", NULL)
                ->get();

            $result = [];
            foreach ($data as $index => $dt) {
                $result[$index] = new stdClass();
                $result[$index]->date = Carbon::createFromFormat("Y-m-d H:i:s", $dt->created_at)->format("d");
                $result[$index]->total_gain = $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
            }

            return $this->createSuccessMessage($result);
        } else if ($type == "monthly") {
            if (Carbon::now()->format("m") == $month) {
                $awal = Carbon::now()->modify('first day of this month');
                $akhir = Carbon::now();
            } else {
                $dateFormat = $year . '-' . $month . '-01';
                $format = 'Y-m-d';
                $awal = Carbon::createFromFormat($format, $dateFormat)->modify('first day of this month');
                $akhir = Carbon::createFromFormat($format, $dateFormat)->modify('last day of this month');
            }

            $data = DailyReport::where("id_axie", $id_axie)
                ->whereBetween('created_at', [$awal, $akhir])
                ->where('slp_adventure', "<>", NULL)
                ->get();

            $result = [];
            $index = 0;
            $week_awal = $awal->format("W");
            foreach ($data as $dt) {
                $week = Carbon::createFromFormat("Y-m-d H:i:s", $dt->created_at)->format("W");
                $week -= $week_awal;
                $week += 1;
                $exists = false;

                foreach ($result as $res) {
                    if ($res->date == $week) {
                        $res->total_gain += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                        $exists = true;
                    }
                }

                if (!$exists) {
                    $result[$index] = new stdClass();
                    $result[$index]->date = $week;
                    $result[$index]->total_gain = $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                    $index++;
                }
            }

            return $this->createSuccessMessage($result);
        } else if ($type == "yearly") {
            if (Carbon::now()->format("Y") == $year) {
                $awal = Carbon::now()->startOfYear();
                $akhir = Carbon::now();
            } else {
                $dateFormat = $year . '-' . $month . '-01';
                $format = 'Y-m-d';
                $awal = Carbon::createFromFormat($format, $dateFormat)->startOfYear();
                $akhir = Carbon::createFromFormat($format, $dateFormat)->endOfYear();
            }

            $data = DailyReport::where("id_axie", $id_axie)
                ->whereBetween('created_at', [$awal, $akhir])
                ->where('slp_adventure', "<>", NULL)
                ->get();

            $result = [];
            $index = 0;
            foreach ($data as $dt) {
                $month = Carbon::createFromFormat("Y-m-d H:i:s", $dt->created_at)->format("M");
                $exists = false;

                foreach ($result as $res) {
                    if ($res->date == $month) {
                        $res->total_gain += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                        $exists = true;
                    }
                }

                if (!$exists) {
                    $result[$index] = new stdClass();
                    $result[$index]->date = $month;
                    $result[$index]->total_gain = $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                    $index++;
                }
            }

            return $this->createSuccessMessage($result);
        }
    }

    public function get_laporan_perolehan_slp(Request $request)
    {
        $year = $request->year;
        $id_owner = $request->id_owner;

        $data = MasterAxie::where("id_owner", $id_owner)
            ->join("daily_report", "daily_report.id_axie", "master_axie.id")
            ->whereYear("daily_report.created_at", "=", $year)
            ->select(DB::raw('daily_report.*, MONTH(DATE(daily_report.created_at)) as month'))
            ->get();

        $result = [];
        $month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"];

        for ($i = 0; $i < 12; $i++) {
            $result[$i] = new stdClass();
            $result[$i]->month = $month[$i];
            $result[$i]->total_slp = 0;
            $result[$i]->total_axie = 0;
            $result[$i]->axie = [];
        }

        foreach ($data as $dt) {
            if ($dt->slp_adventure > 0) {
                $result[$dt->month - 1]->total_slp += $dt->slp_adventure + $dt->slp_quest + $dt->slp_pvp;
                if (!array_search($dt->id_axie, $result[$dt->month - 1]->axie)) {
                    array_push($result[$dt->month - 1]->axie, $dt->id_axie);
                    $result[$dt->month - 1]->total_axie++;
                }
            }
        }

        return $this->createSuccessMessage($result);
    }

    public function getLaporanPengeluaran(Request $request)
    {
        $year = $request->year;
        $id_owner = $request->id_owner;

        $data = MasterKantor::where("id_owner", $id_owner)
            ->join("users", "users.id_kantor", "master_kantor.id")
            ->join("gaji", "gaji.id_user", "users.id")
            ->whereYear("gaji.created_at", "=", $year)
            ->select(DB::raw('gaji.*, MONTH(DATE(gaji.created_at)) as month'))
            ->get();

        $result = [];
        $month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"];

        for ($i = 0; $i < 12; $i++) {
            $result[$i] = new stdClass();
            $result[$i]->month = $month[$i];
            $result[$i]->total_pengeluaran = 0;
        }

        foreach ($data as $dt) {
            $result[$dt->month - 1]->total_pengeluaran += $dt->gaji;
        }

        return $this->createSuccessMessage($result);
    }
}
