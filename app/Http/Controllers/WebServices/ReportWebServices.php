<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DailyReport;
use App\Models\DendaInventori;
use App\Models\Gaji;
use App\Models\LogGaji;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Reimburs;
use App\Models\Setting;
use App\Models\SettingLevel;
use App\Models\SettingPVP;
use App\Models\UserAxie;
use App\Models\Users;
use DateTime;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use PDO;
use stdClass;

class ReportWebServices extends Controller
{
    //
    public function dailyReport(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        if (
            !isset($request->id_axie) || !isset($request->slp_adventure) || !isset($request->slp_pvp) ||
            !isset($request->slp_quest) || !isset($request->screenshot) || !isset($request->last_inventory)
        ) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }

        if (!UserAxie::isValid($user->id, $request->id_axie)) {
            return $this->createErrorMessage("Axie bukan milik user", 400);
        }

        // DailyReport::isComplete($user->id, $request->id_axie);

        if (DailyReport::isComplete($user->id, $request->id_axie)) {
            return $this->createErrorMessage("Report hari ini sudah terlapor", 400);
        }


        if ($request->slp_adventure < 0 || $request->slp_pvp < 0 || $request->slp_quest < 0) {
            return $this->createErrorMessage("SLP yang didapatkan tidak boleh kurang dari 0", 400);
        }

        $setting_slp_quest = Setting::where("nama_setting", "SETTING_SLP_QUEST")->first();
        $setting_slp_adventure = Setting::where("nama_setting", "SETTING_MAX_SLP_ADVENTURE")->first();

        if ($request->slp_adventure > $setting_slp_adventure->value || ($request->slp_quest != $setting_slp_quest->value && $request->slp_quest != 0)) {
            return $this->createErrorMessage("SLP adventure (" . $request->slp_adventure . ") / quest (" . $request->slp_quest . ") yang didapatkan melebihi batas silahkan cek kembali", 400);
        }


        $last_slp = MasterAxie::where("id", $request->id_axie)->first()->total_slp;

        $total_slp = $request->slp_adventure + $request->slp_pvp + $request->slp_quest;

        $server_status = Setting::where("nama_setting", "SETTING_SERVER_AXIE")->first();

        $axie = MasterAxie::where("id", $request->id_axie)->first();

        $level = $axie->level;
        // if (isset($request->level)) {
        //     $level = $request->level;
        // }
        $minimal_slp = SettingLevel::getMinimalSLP($axie->id_owner, $level);

        if ($request->slp_adventure < $minimal_slp->min_slp && intval($server_status->value) == 1) {
            return $this->createErrorMessage("Minimal SLP Adventure tidak memenuhi, minimal (" . $minimal_slp->min_slp . ")", 400);
        }

        $data = new DailyReport();
        $data->id_user = $user->id;
        $data->id_axie = $request->id_axie;
        $data->slp_adventure = $request->slp_adventure;
        if ($request->clear_boss != null) {
            if ($request->clear_boss) {
                if ($axie->status_boss == 1) {
                    return $this->createErrorMessage("Boss reward sudah pernah clear", 400);
                }
                //KALO CLEAR BOSS TAMBAH SLP 200
                $total_slp += 200;
                $data->slp_adventure = $request->slp_adventure + 200;
                $axie->status_boss = 1;
            }
        }

        if ($request->last_inventory != $total_slp + $last_slp) {
            return $this->createErrorMessage("Last Inventory tidak sama (" . ($total_slp + $last_slp) . ")", 400);
        }
        $data->slp_pvp = $request->slp_pvp;
        $data->slp_quest = $request->slp_quest;
        $data->level_before = $axie->level;
        if ($request->level != null) {
            if ($request->level >= $axie->level && $request->level <= 25) {
                $axie->level = $request->level;
            } else {
                return $this->createErrorMessage("Level tidak bisa lebih kecil dari level sekarang", 400);
            }
        }
        $data->last_inventory = $request->last_inventory;
        $data->url_screenshot = $request->screenshot;

        $data->status_server = intval($server_status->value);
        $data->keterangan = $request->keterangan;
        // if (isset($request->screenshot)) {
        //     $foto_ktp = $request->screenshot;
        //     $category = "screenshot";
        //     $url_screenshot = $this->uploadImage($foto_ktp, $category);
        //     $data->url_screenshot = $url_screenshot;
        // }
        $data->save();

        // $axie->total_slp = $total_slp + $last_slp;
        $axie->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " REPORT DAILY AXIE " . strtoupper($axie->email_akun) . " DENGAN ID DAILY REPORT = " . $data->id;
        if ($request->level != null) {
            $log->description = strtoupper($user->email) . " REPORT DAILY AXIE " . strtoupper($axie->email_akun) . " DENGAN ID DAILY REPORT = " . $data->id . " DAN UPDATE LEVEL MENJADI " . $request->level;
        }
        $log->save();

        return $this->createSuccessMessage($data);
    }

    public function getMyDailyReport()
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $dataUserAxie = new stdClass();
        $dataUserAxie->total_akun = 0;
        $dataUserAxie->total_selesai = 0;
        $dataUserAxie->data_daily = UserAxie::where("id_user", $user->id)->get();
        $dataUserAxie->total_akun = count($dataUserAxie->data_daily);

        $now = Carbon::now();
        $now_hour = Carbon::parse($now)->format("H");

        // echo Carbon::today()->subDays(1)->addHours(9);

        if ($now_hour < 9) {
            $day = [Carbon::today()->subDays(1)->addHours(9), Carbon::now()];
        } else {
            $day = [Carbon::today(), Carbon::now()];
        }

        foreach ($dataUserAxie->data_daily as $dt) {
            $dataReport = DailyReport::where("id_axie", $dt->id_axie)
                ->whereBetween('created_at', $day)
                ->where("slp_adventure", "<>", null)
                ->where("slp_pvp", "<>", null)
                ->where("slp_quest", "<>", null)
                ->orderBy("created_at", "desc")
                ->first();
            $averagePVP = DailyReport::where("id_axie", $dt->id_axie)
                ->where("id_user", $user->id)
                ->whereMonth("created_at", "=", date('m'))
                ->avg("slp_pvp");
            $axie = MasterAxie::where("id", $dt->id_axie)
                ->first();
            $dt->average_pvp = $averagePVP;

            $dt->daily_report = $dataReport;
            if (isset($dataReport)) {
                $jam = Carbon::parse($dataReport->created_at)->format("H");
                if ($jam < 9 && $now_hour >= 9) {
                    //CEK KALO DIA LAPORAN SEBELUM JAM 9 BRATI DIA LAPORAN UNTUK HARI SEBELUMNYA
                    $dt->daily_report = null;
                    $dataReport = null;
                } else if ($dataReport->status == 1) {
                    $dt->gaji = Gaji::where("id_daily_report", $dataReport->id)->first();
                }
            }
            $dt->axie = $axie;

            if (isset($dataReport)) {
                $dataUserAxie->total_selesai++;
            }
        }

        return $this->createSuccessMessage($dataUserAxie);
    }

    public function approveDailyReport(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        if ($user->role == 1) {
            return $this->createErrorMessage("User not allowed", 400);
        }

        if ($request->id_daily_report == null) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }

        $daily_report = DailyReport::where("id", $request->id_daily_report)->first();
        if ($daily_report == null) {
            return $this->createErrorMessage("Daily Report not found", 400);
        }

        if ($daily_report->status != 0) {
            return $this->createErrorMessage("Daily report sudah di approve", 400);
        }
        $axie = MasterAxie::where("id", $daily_report->id_axie)->first();

        if ($request->status == -1) {
            //REJECT DAILY REPORT
            $axie->level = $daily_report->level_before;
            if ($daily_report->slp_adventure > 200) {
                $axie->status_boss = 0;
            }
            $axie->save();

            $daily_report->delete();

            $log = new LogServer();
            $log->description = strtoupper($user->email) . " REJECT DAILY REPORT AXIE " . strtoupper($axie->email_akun);
            $log->save();

            return $this->createSuccessMessage($daily_report);
        }

        if ($daily_report->level_before != null) {
            $axie->total_slp += ($daily_report->slp_adventure + $daily_report->slp_pvp + $daily_report->slp_quest);
            $axie->save();
        }

        $setting_level = SettingLevel::getMinimalSLP($axie->id_owner, $axie->level);
        $day = date('D');

        if ($day == "Sun") {
            //KALO HARI MINGGU TAMBAH BONUS
            $bonus_minggu = Setting::where("nama_setting", "BONUS_HARI_MINGGU")->first();
            $setting_level->value = $setting_level->value + $bonus_minggu->value;
        }

        $now = Carbon::now();
        $now_hour = Carbon::parse($now)->format("H");
        $jam = Carbon::parse($daily_report->created_at)->format("H");
        $today = Carbon::today()->subDays(1);
        $today->hour = 10;
        $gaji = Gaji::where("id_daily_report", $daily_report->id)->first();

        if (isset($gaji)) {
            return $this->createErrorMessage("Report already approved", 400);
        }
        if ($daily_report->slp_adventure >= $setting_level->min_slp || $daily_report->status_server != 1) {
            $data = new Gaji();
            $data->id_user = $daily_report->id_user;
            $data->id_daily_report = $daily_report->id;
            $data->id_axie = $daily_report->id_axie;
            $data->gaji = $setting_level->value;
            if ($jam < 9 || $now_hour < 9) {
                //CEK KALO DIA LAPORAN PAGI SEBELUM JAM 9 IKUT HARI SEBELUMNYA
                $data->created_at = $today;
                $daily_report->created_at = $today;
                $daily_report->save();
            }
            $data->type = "GAJI_HARIAN";
            $data->save();
        } else {
            $data = new Gaji();
            $data->id_user = $daily_report->id_user;
            $data->id_daily_report = $daily_report->id;
            $data->id_axie = $daily_report->id_axie;
            $data->gaji = 0;
            if ($jam < 9 || $now_hour < 9) {
                //CEK KALO DIA LAPORAN PAGI SEBELUM JAM 9 IKUT HARI SEBELUMNYA
                $data->created_at = $today;
                $daily_report->created_at = $today;
                $daily_report->save();
            }
            $data->type = "GAJI_HARIAN";
            $data->save();
        }

        $daily_report->status = 1;
        $daily_report->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " APPROVE DAILY REPORT AXIE " . strtoupper($axie->email_akun);
        $log->save();
        if ($request->status == 1) {
            $message = "Approve Daily Report Berhasil";
        } else {
            $message = "Tolak Daily Report Berhasil";
        }
        return $this->createSuccessMessage($data, 200, $message);
    }

    public function getLaporanGajiWeekly(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        $role = MasterRole::where("id", $user->role)->first();
        if ($user->role == 5) {
            //JIKA USER TRAINEE TIDAK BOLEH LIHAT GAJI
            return $this->createErrorMessage("Pegawai Training tidak diperbolehkan melihat gaji", 400);
        } else if ($role->jabatan == 2) {
            //JIKA USER ADMIN MAKA AMBIL ID USER
            $user->id = $request->id_user;
        }
        $month = $request->month;
        $year = $request->year;

        $gaji = Gaji::where("id_user", $user->id)
            ->whereMonth("created_at", "=", $month)
            ->whereYear("created_at", "=", $year)
            ->get();

        $data = [];
        $data_denda = DendaInventori::where("id_user", $user->id)
            ->whereMonth("created_at", "=", $month)
            ->whereYear("created_at", "=", $year)
            ->get();

        $data_reimburs = Reimburs::where("id_user", $user->id)
            ->whereMonth("created_at", "=", $month)
            ->whereYear("created_at", "=", $year)
            ->where("status", 1)
            ->get();

        $temp = $year . '-' . $month . '-01';
        $d = new DateTime($temp);
        $firstday = $d->modify('first day of this month');
        $week_awal = $firstday->format("W");
        $total_bonus_mingguan = 0;
        $total_bonus_bulanan = 0;
        $total_bonus_pvp = 0;
        $total_denda = 0;
        $total_gaji = 0;
        $total_reimburs = 0;
        // echo $firstday;

        //HITUNG LAPORAN GAJI
        foreach ($gaji as $dt) {
            if ($dt->type == "GAJI_BONUS_MINGGUAN") {
                $total_bonus_mingguan += $dt->gaji;
            } else if ($dt->type == "GAJI_BONUS_BULANAN") {
                $total_bonus_bulanan += $dt->gaji;
            } else if ($dt->type == "GAJI_BONUS_PVP") {
                $total_bonus_pvp += $dt->gaji;
            } else if ($dt->type == "DENDA_INVENTARIS") {
                $total_denda += $dt->gaji * -1;
            } else if ($dt->type == "REIMBURS") {
                $total_reimburs += $dt->gaji;
            } else {
                if ($dt->type == "GAJI_BONUS_MINGGUAN" && $user->role != 1) {
                    $total_bonus_mingguan += $dt->gaji;
                }
                $d = new DateTime($dt->created_at);
                $dt->day = $d->format("l");
                $dt->week = ($d->format("W") - $week_awal) + 1;

                $data_daily_report = DailyReport::where("id", $dt->id_daily_report)->first();
                $dt->daily_report = $data_daily_report;
                $index = count($data);
                foreach ($data as $i => $dts) {
                    if ($dts->week == $dt->week) {
                        $index = $i;
                        break;
                    }
                }
                if (!isset($data[$index])) {
                    $data[$index] = new stdClass();
                    $data[$index]->week = $dt->week;
                    $data[$index]->data_axie = [];
                }

                $index2 = count($data[$index]->data_axie);
                foreach ($data[$index]->data_axie as $i => $dts) {
                    if ($dts->id_axie == $dt->id_axie) {
                        $index2 = $i;
                        break;
                    }
                }

                if (!isset($data[$index]->data_axie[$index2])) {
                    $data[$index]->data_axie[$index2] = new stdClass();
                    $data[$index]->data_axie[$index2]->id_axie = $dt->id_axie;
                    $data_axie = MasterAxie::where("id", $dt->id_axie)->first();
                    $data[$index]->data_axie[$index2]->detail_axie = $data_axie;
                }

                $data[$index]->data_axie[$index2]->data_gaji[] = $dt;

                //TAMBAH TOTAL PLAY AXIE
                if ($dt->type == "GAJI_HARIAN") {
                    $total_gaji += $dt->gaji;
                }
            }
        }

        //HITUNG BONUS PVP
        $daily_report = DailyReport::where("id_user", $user->id)
            ->whereMonth("created_at", "=", $month)
            ->whereYear("created_at", "=", $year)
            ->orderBy("created_at", "asc")
            ->get();

        $data_pvp = Gaji::hitungBonusPVP($daily_report, $gaji, $month, $year);

        // return $this->createSuccessMessage($data_pvp);

        $data_bonus_mingguan = Gaji::where("id_user", $user->id)
            ->whereMonth("created_at", "=", $month)
            ->whereYear("created_at", "=", $year)
            ->where("type", "GAJI_BONUS_MINGGUAN")
            ->get();
        foreach ($data_bonus_mingguan as $dt) {
            $d = new DateTime($dt->created_at);
            $dt->day = $d->format("l");
            $dt->week = ($d->format("W") - $week_awal) + 1;
        }

        $result = new stdClass();
        $result->total_bonus_mingguan = $total_bonus_mingguan;
        $result->total_bonus_bulanan = $total_bonus_bulanan;
        $result->total_bonus_pvp = $total_bonus_pvp;
        $result->total_reimburs = $total_reimburs;
        $result->total_gaji = $total_gaji + $total_reimburs + $total_bonus_bulanan + $total_bonus_mingguan - $total_denda;
        $result->total_denda = $total_denda;
        $result->total_average_pvp = $data_pvp->average_pvp;
        $result->est_total_bonus_pvp = $data_pvp->est_bonus_pvp;
        $result->total_akun_persentase = $data_pvp->total_akun_persentase;
        $result->data_pvp = $data_pvp->data_pvp;
        $result->data_weekly = $data;
        $result->data_denda = $data_denda;
        $result->data_reimburs = $data_reimburs;
        $result->data_bonus_mingguan = $data_bonus_mingguan;

        return $this->createSuccessMessage($result);
    }

    public function getLaporanSLP(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        $date = $request->date;
        $d = new DateTime($date);
        // return $this->createSuccessMessage($d);
        // return $this->createSuccessMessage(Carbon::today());
        $data = DailyReport::whereDate("created_at", '=', $d)
            ->where("id_user", $user->id)
            ->select("id_user", "id_axie", "slp_adventure", "slp_pvp", "slp_quest", "last_inventory", "level_before")
            ->get();
        $gaji = Gaji::whereDate("created_at", "=", $d)
            ->where("id_user", $user->id)
            ->get();
        foreach ($gaji as $dt) {
            if ($dt->id_daily_report == null && $dt->type == "GAJI_HARIAN") {
                $temp = new stdClass();
                $temp->id_user = $dt->id_user;
                $temp->id_axie = $dt->id_axie;
                $temp->slp_adventure = 0;
                $temp->slp_pvp = 0;
                $temp->slp_quest = 0;
                $temp->last_inventory = null;
                $data[] = $temp;
            }
        }
        return $this->createSuccessMessage($data);
    }

    public function getListReport(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        // if ($user->role == 1 || $user->role == 5) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        if (isset($request->id)) {
            $data = DailyReport::join("users", "users.id", "daily_report.id_user")
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->select("daily_report.*", "users.name", "master_axie.email_akun")
                ->where("daily_report.id", $request->id)
                ->first();
        } else {
            $role = MasterRole::where("id", $user->role)->first();
            if ($user->role == 2) {
                //SUPERADMIN
                $data = DailyReport::where("daily_report.status", 0)
                    ->join("users", "users.id", "daily_report.id_user")
                    ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                    ->select("daily_report.*", "users.name", "master_axie.email_akun", "master_axie.level")
                    ->get();
            } else if ($role->jabatan == 2) {
                //OWNER
                $data = DailyReport::where("daily_report.status", 0)
                    ->join("users", "users.id", "daily_report.id_user")
                    ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                    ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                    ->where("master_kantor.id_owner", $user->id)
                    ->select("daily_report.*", "users.name", "master_axie.email_akun", "master_axie.level")
                    ->get();
            } else if ($role->jabatan == 1) {
                //ADMIN & PENGAWAS
                $work_at = MasterKantor::where("id", $user->id_kantor)->first();
                $data = DailyReport::where("daily_report.status", 0)
                    ->join("users", "users.id", "daily_report.id_user")
                    ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                    ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                    ->where("master_kantor.id_owner", $work_at->id_owner)
                    ->select("daily_report.*", "users.name", "master_axie.email_akun", "master_axie.level")
                    ->get();
            }
        }

        return $this->createSuccessMessage($data);
    }

    public function report_axie(Request $request)
    {
        $data = DailyReport::where("id_axie", $request->id_axie)
            ->orderBy("created_at", "DESC")
            ->where("status", 1)
            ->limit(500)
            ->get();
        foreach ($data as $dt) {
            $dt->name = Users::where("id", $dt->id_user)->select("name")->first()->name;
        }
        return $this->createSuccessMessage($data);
    }

    public function get_log_gaji(Request $request)
    {
        $data = LogGaji::where("id_user", $request->id_user)
            ->whereMonth("created_at", "=", $request->month)
            ->whereYear("created_at", "=", $request->year)
            ->orderBy("created_at", "desc")
            ->get();

        return $this->createSuccessMessage($data);
    }
}
