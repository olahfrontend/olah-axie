<?php

namespace App\Http\Controllers\WebServices;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CoachUsers;
use App\Models\DailyReport;
use App\Models\DetailJadwalCuti;
use App\Models\Gaji;
use App\Models\HakAkses;
use App\Models\JadwalCuti;
use App\Models\LogServer;
use App\Models\MasterAxie;
use App\Models\MasterKantor;
use App\Models\MasterRole;
use App\Models\Reimburs;
use App\Models\Setting;
use App\Models\SettingLevel;
use App\Models\UserAxie;
use App\Models\UserContact;
use App\Models\Users;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use stdClass;

class UserWebService extends Controller
{
    //
    public function register(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $user = Auth::user();

        $email = $this->test_input($request->email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return $this->createErrorMessage("Email not valid", 400);
        }

        $checkExist = Users::where("email", $request->email)->first();
        if ($checkExist) {
            return $this->createErrorMessage("Email Already Exists", 400);
        }
        $newUser = new Users();
        $newUser->email = $request->email;
        $newUser->password =  Hash::make($request->password);
        $newUser->name = $request->name;
        if (isset($request->role)) {
            $newUser->role = $request->role;
        }
        $newUser->id_kantor = $request->id_kantor;
        $newUser->save();

        if (isset($request->role)) {
            if ($request->role == 3) {
                //GENERATE SETTING UNTUK ROLE OWNER
                $nama_setting = ["BONUS_HARI_MINGGU", "BONUS_FULL_MINGGU", "BONUS_FULL_BULAN", "BONUS_ARENA", "MINIMAL_BONUS_PVP_SLP"];
                $type = ["FIXED", "PERCENTAGE", "PERCENTAGE", "FIXED", "FIXED"];
                $value = [2000, 15, 15, 1000, 35];
                for ($i = 0; $i < 5; $i++) {
                    $data = new Setting();
                    $data->id_user = $newUser->id;
                    $data->nama_setting = $nama_setting[$i];
                    $data->type = $type[$i];
                    $data->value = $value[$i];
                    $data->save();
                }

                //GENERATE SETTING MINIMAL SLP ADVENTURE PER LVL
                $min_level = [1, 10, 16, 21];
                $max_level = [9, 15, 20, 999];
                $min_slp = [40, 60, 80, 100];
                $value = [8000, 4000, 9000, 10000];
                for ($i = 0; $i < 4; $i++) {
                    $data = new SettingLevel();
                    $data->id_user = $newUser->id;
                    $data->min_level = $min_level[$i];
                    $data->max_level = $max_level[$i];
                    $data->min_slp = $min_slp[$i];
                    $data->value = $value[$i];
                    $data->save();
                }
            }
        }


        $log = new LogServer();
        $log->description = strtoupper($user->email) . " CREATED NEW USER " . strtoupper($newUser->email);
        $log->save();
        return $this->createSuccessMessage($newUser);
    }

    public function login(Request $request)
    {
        Auth::logout();
        $email = $request->email;
        $password = $request->password;

        if (!isset($email) || !isset($password)) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }

        $data = [
            "email" => $email,
            "password" => $password
        ];
        Auth::attempt($data, true);

        if (Auth::check()) {
            $user = Auth::user();
            if ($user->status == 0) {
                Auth::logout();
                return $this->createErrorMessage("User telah dinonaktifkan", 400);
            }
            return $this->createSuccessMessage($user, 200, "Login Success");
        } else {
            return $this->createErrorMessage("Email or Password wrong", 400);
        }
    }

    public function getInitialData()
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $user = Auth::user();
        if ($user->role == 1 || $user->role == 5) {
            //HITUNG TOTAL GAJI HARI INI
            $total_gaji_bulan_ini = Users::getTotalGajiBulanIni($user->id);
            $user->total_gaji_bulan_ini = $total_gaji_bulan_ini;
            $now = new DateTime();

            $user->current_date = $now->format("d-m-Y");
            $user->periode = $now->format("M Y");

            $firstday = $now->modify('first day of this month');
            $week_awal = $firstday->format("W");
            $now = new DateTime();
            $user->week = ($now->format("W") - $week_awal) + 1;
            // print_r($week_awal);

            $month = $now->format("m");
            $year = $now->format("Y");

            $gaji = Gaji::where("id_user", $user->id)
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->get();
            $daily_report = DailyReport::where("id_user", $user->id)
                ->whereMonth("created_at", "=", $month)
                ->whereYear("created_at", "=", $year)
                ->orderBy("created_at", "asc")
                ->get();

            $data_pvp = Gaji::hitungBonusPVP($daily_report, $gaji, $month, $year);
            $user->total_gaji_bulan_ini += $data_pvp->est_bonus_pvp;
            $total_mingguan = 0;
            $minggu = [];
            foreach ($gaji as $dt) {
                $dt->week = ($dt->created_at->format("W") - $week_awal) + 1;
                $index = count($minggu);
                foreach ($minggu as $i => $d) {
                    if ($d->minggu_ke == $dt->week) {
                        $index = $i;
                        break;
                    }
                }

                if (!isset($minggu[$index])) {
                    $minggu[$index] = new stdClass();
                    $minggu[$index]->minggu_ke = $dt->week;
                    $minggu[$index]->bonus = true;
                }

                if ($dt->type == "GAJI_BONUS_MINGGUAN") {
                    $total_mingguan += $dt->gaji;
                } else if ($dt->type == "GAJI_HARIAN") {
                    if ($dt->gaji == 0) {
                        $minggu[$index]->bonus = false;
                    }
                }
            }
            $user->total_bonus_pvp = $data_pvp->est_bonus_pvp;
            $user->total_mingguan = $total_mingguan;
            $user->bonus_minggu = $minggu;
            // return $this->createSuccessMessage($gaji);
            $setting = Setting::where("nama_setting", "SETTING_SERVER_AXIE")->first();
            if ($setting->value == 1) {
                $user->server_status_axie = true;
            } else {
                $user->server_status_axie = false;
            }

            if ($user->role == 5) {
                //ROLE TRAINEE TIDAK DAPAT LIHAT GAJI
                $user->total_gaji_bulan_ini = 0;
                $user->total_bonus_pvp = 0;
                $user->total_mingguan = 0;
            }
        } else {
            $user->data_hakakses = HakAkses::where("id_role", $user->role)
                ->join("master_menu", "master_menu.id", "hak_akses.id_menu")
                ->where("master_menu.id_parent_menu", null)
                ->orderBy("master_menu.urutan", "asc")
                ->get();
            foreach ($user->data_hakakses as $dt) {
                $dt->nama_menu = str_replace("_", " ", $dt->nama_menu);
                if ($dt->have_submenu == 1) {
                    $submenu = HakAkses::where("id_role", $user->role)
                        ->join("master_menu", "master_menu.id", "hak_akses.id_menu")
                        ->where("master_menu.id_parent_menu", $dt->id)
                        ->orderBy("master_menu.urutan", "asc")
                        ->get();
                    $activePage = "";
                    foreach ($submenu as $sub) {
                        $sub->nama_menu = str_replace("_", " ", $sub->nama_menu);
                        $activePage = $activePage . '$activePage == ' . $sub->active_page  . '||';
                    }
                    $activePage = substr($activePage, 0, -2);
                    $dt->active_page = $activePage;
                    $dt->submenu = $submenu;
                }
            }
        }

        return $this->createSuccessMessage($user);
    }

    public function updateProfile(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        if (
            isset($request->no_telp) && isset($request->nama_darurat_1) && isset($request->no_telp_darurat_1) && isset($request->hubungan_darurat_1)
            && isset($request->alamat) && isset($request->no_ktp) && isset($foto_ktp) && isset($foto_selfie)
        ) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }


        $data = Users::where('id', $user->id)->first();
        $data->no_telp = $request->no_telp;
        $data->nama_darurat_1 = $request->nama_darurat_1;
        $data->no_telp_darurat_1 = $request->no_telp_darurat_1;
        $data->hubungan_darurat_1 = $request->hubungan_darurat_1;
        $data->nama_darurat_2 = $request->nama_darurat_2;
        $data->no_telp_darurat_2 = $request->no_telp_darurat_2;
        $data->hubungan_darurat_2 = $request->hubungan_darurat_2;
        $data->alamat = $request->alamat;
        $data->no_ktp = $request->no_ktp;
        $data->foto_ktp = $request->foto_ktp;
        $data->foto_selfie = $request->foto_selfie;

        // if (isset($request->foto_ktp)) {
        //     $foto_ktp = $request->foto_ktp;
        //     $category = "foto";
        //     $url_path_ktp = $this->uploadImage($foto_ktp, $category);
        //     $data->foto_ktp = $url_path_ktp;
        // }

        // if (isset($request->foto_selfie)) {
        //     $foto_selfie = $request->foto_selfie;
        //     $category = "foto";
        //     $url_path_selfie = $this->uploadImage($foto_selfie, $category);
        //     $data->foto_selfie = $url_path_selfie;
        // }

        $data->save();

        $log = new LogServer();
        $log->description = strtoupper($user->email) . " UPDATED PROFILE ";
        $log->save();
        return $this->createSuccessMessage("Update Profile Berhasil");
    }

    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }

    public function getList()
    {
        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please Login First");
        $user = Auth::user();
        if ($user->role == 1 || $user->role == 5) {
            return $this->createErrorMessage("User Not Allowed", 400);
        }

        $role = MasterRole::where("id", $user->role)->first();
        if ($user->role == 2) {
            $data = Users::leftJoin("master_kantor", "master_kantor.id", "users.id_kantor")
                ->join("master_role", "master_role.id", "users.role")
                ->orderBy("users.role")
                ->select("users.*", "master_kantor.alamat as alamat_kantor", "master_role.nama_role")
                ->get();
        } else if ($role->jabatan == 2) {
            $data = Users::where("users.role", "<>", 2)
                ->where("master_kantor.id_owner", $user->id)
                ->leftJoin("master_kantor", "master_kantor.id", "users.id_kantor")
                ->join("master_role", "master_role.id", "users.role")
                ->orderBy("users.name")
                ->select("users.*", "master_kantor.alamat as alamat_kantor", "master_role.nama_role")
                ->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $user->id_kantor)->first();
            $data = Users::where("users.role", "<>", 2)
                ->where("users.role", "<>", 6)
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->leftJoin("master_kantor", "master_kantor.id", "users.id_kantor")
                ->join("master_role", "master_role.id", "users.role")
                ->orderBy("users.name")
                ->select("users.*", "master_kantor.alamat as alamat_kantor", "master_role.nama_role")
                ->get();
        }
        return $this->createSuccessMessage($data);
    }

    public function addContact(Request $request)
    {

        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please login first");
        $contact = $request->contact;
        $user = Auth::user();
        $user_id = $user->id;
        $finalData = stripslashes($contact);
        $data = json_decode($finalData, true);
        $size =  count($data);
        $count = 0;
        if ($size > 100) {
            DB::beginTransaction();

            while ($count < 100) {
                $index = rand(0, $size);
                $exist = UserContact::getContactbyTelephone($data["telp"], $user_id);
                if (!$exist) {
                    $user_contact = new UserContact();
                    $user_contact->user_id = $user_id;
                    $user_contact->name = $data[$index]["nama"];
                    $user_contact->telephone = $data[$index]["telp"];
                    $user_contact->save();
                    array_splice($input, $index, 1);
                    $count++;
                }
            }
            DB::commit();
        } else {
            DB::beginTransaction();
            foreach ($data as $dt) {
                $exist = UserContact::getContactbyTelephone($dt["telp"], $user_id);
                if (!$exist) {
                    $user_contact = new UserContact();
                    $user_contact->user_id = $user_id;
                    $user_contact->name = $dt["nama"];
                    $user_contact->telephone = $dt["telp"];
                    $user_contact->save();
                }
            }
            DB::commit();
        }

        return $this->createSuccessMessage($data);
    }

    public function getContactCount(Request $request)
    {

        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please login first");
        $contact = $request->contact;
        $user = Auth::user();
        $user_id = $user->id;
        $JSON = json_decode($contact, true);
        $count = 0;

        $sender = UserContact::where("user_id", $user_id)->get();
        $count = count($sender);
        $result["total_nomor"] = $count;
        $result["data"] = $sender;
        if ($count == null) {
            $result["total_nomor"] = 0;
            return $this->createSuccessMessage($result);
        }

        return $this->createSuccessMessage($result);
    }

    public function getDetailPegawai(Request $request)
    {
        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please login first");
        $user = Auth::user();
        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not Allowed", 400);
        // }
        $id = $request->id;
        $dataPegawai = Users::where("id", $id)->first();
        $dataAxie = UserAxie::where("user_axie.id_user", $id)
            ->join("master_axie", "master_axie.id", "user_axie.id_axie")
            ->get();
        $dataKontak = UserContact::where("user_id", $id)->get();
        $data["user"] = $dataPegawai;
        $data["axie"] = $dataAxie;
        $data["kontak"] = $dataKontak;

        return $this->createSuccessMessage($data);
    }

    public function getLogServer(Request $request)
    {
        $date = Carbon::today();
        if (isset($request->date)) {
            $date = date_create($request->date);
        }
        $data = LogServer::orderBy("id", "DESC")->whereDate('created_at', '=', $date)->orderBy('created_at', "desc")->get();
        return $this->createSuccessMessage($data);
    }

    public function getLeaderboard(Request $request)
    {
        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please login first");

        $user = Auth::user();

        if ($user->role == 2) {
            //SUPERADMIN
            if (isset($request->month) && isset($request->year)) {
                $data = DB::table('daily_report')
                    ->select('id_axie', 'id_user', DB::raw('round(AVG(slp_pvp),0) as average_pvp'), DB::raw('count(id) as total_play'))
                    ->where("status_server", 1)
                    ->where("status", 1)
                    ->groupBy('id_axie', 'id_user')
                    ->orderBy("average_pvp", "DESC")
                    ->whereMonth('created_at', $request->month)
                    ->whereYear('created_at', $request->year)
                    ->where('level_before', ">", "15")
                    ->get();
            } else {
                $now = Carbon::now();
                $data = DB::table('daily_report')
                    ->select('id_axie', 'id_user', DB::raw('round(AVG(slp_pvp),0) as average_pvp'), DB::raw('count(id) as total_play'))
                    ->where("status_server", 1)
                    ->where("status", 1)
                    ->groupBy('id_axie', 'id_user')
                    ->orderBy("average_pvp", "DESC")
                    ->whereMonth('created_at', $now->month)
                    ->whereYear('created_at', $now->year)
                    ->where('level_before', ">", "15")
                    ->get();
            }
        } else {
            $role = MasterRole::where("id", $user->role)->first();
            //OWNER DAN PEGAWAI PEGAWAINYA
            if ($role->jabatan == 2) {
                $work_at = new stdClass();
                $work_at->id_owner = $user->id;
            } else {
                $work_at = MasterKantor::where("id", $user->id_kantor)->first();
            }

            if (isset($request->month) && isset($request->year)) {
                $data = DB::table('daily_report')
                    ->join("users", "users.id", "daily_report.id_user")
                    ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                    ->select('daily_report.id_axie', 'daily_report.id_user', DB::raw('round(AVG(daily_report.slp_pvp),0) as average_pvp'), DB::raw('count(daily_report.id) as total_play'))
                    ->where("daily_report.status_server", 1)
                    ->where("daily_report.status", 1)
                    ->where("master_kantor.id_owner", $work_at->id_owner)
                    ->groupBy('daily_report.id_axie', 'id_user')
                    ->orderBy("average_pvp", "DESC")
                    ->whereMonth('daily_report.created_at', $request->month)
                    ->whereYear('daily_report.created_at', $request->year)
                    ->where('daily_report.level_before', ">", "15")
                    ->get();
            } else {
                $now = Carbon::now();
                $data = DB::table('daily_report')
                    ->join("users", "users.id", "daily_report.id_user")
                    ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                    ->select('daily_report.id_axie', 'daily_report.id_user', DB::raw('round(AVG(daily_report.slp_pvp),0) as average_pvp'), DB::raw('count(daily_report.id) as total_play'))
                    ->where("daily_report.status_server", 1)
                    ->where("daily_report.status", 1)
                    ->where("master_kantor.id_owner", $work_at->id_owner)
                    ->groupBy('daily_report.id_axie', 'id_user')
                    ->orderBy("average_pvp", "DESC")
                    ->whereMonth('daily_report.created_at', $now->month)
                    ->whereYear('daily_report.created_at', $now->year)
                    ->where('daily_report.level_before', ">", "15")
                    ->get();
            }
        }
        $result = [];
        $ctr = 0;
        foreach ($data as $dt) {
            if (isset($dt->average_pvp)) {
                if ($dt->total_play >= 7) {
                    //lebih dari 7 kali main baru masuk leaderboard
                    $exist = false;
                    $index = $ctr;
                    foreach ($result as $i => $res) {
                        if ($res->id_axie == $dt->id_axie) {
                            $exist = true;
                            $index = $i;
                            break;
                        }
                    }
                    if (!$exist) {
                        $user = Users::where("id", $dt->id_user)->first();
                        $axie = MasterAxie::where("id", $dt->id_axie)->first();
                        $result[$ctr] = new stdClass();
                        $result[$ctr]->rank = $ctr + 1;
                        $result[$ctr]->id_user = $user->id;
                        $result[$ctr]->total_play = $dt->total_play;
                        $result[$ctr]->id_axie = $axie->id;
                        $result[$ctr]->name = $user->name;
                        $result[$ctr]->axie = $axie->email_akun;
                        $result[$ctr]->average_pvp = $dt->average_pvp;
                        $ctr++;
                    } else {
                        if ($result[$index]->total_play < $dt->total_play) {
                            $user = Users::where("id", $dt->id_user)->first();
                            $axie = MasterAxie::where("id", $dt->id_axie)->first();
                            $result[$index]->rank = $ctr + 1;
                            $result[$index]->id_user = $user->id;
                            $result[$index]->total_play = $dt->total_play;
                            $result[$index]->id_axie = $axie->id;
                            $result[$index]->name = $user->name;
                            $result[$index]->axie = $axie->email_akun;
                            $result[$index]->average_pvp = $dt->average_pvp;
                        }
                    }
                }
            }
        }
        usort($result, function ($a, $b) {
            return $a->rank > $b->rank ? 1 : -1;
        });

        $hasil = new stdClass();
        $user = Auth::user();
        $role = MasterRole::where("id", $user->role)->first();
        if ($role->jabatan == 2) {
            $top_10 = [];
            foreach ($result as $data) {
                $exist = false;
                if (count($top_10) < 10) {
                    foreach ($top_10 as $t) {
                        if ($t->id_user == $data->id_user) {
                            $exist = true;
                            break;
                        }
                    }
                    if (!$exist) {
                        $data->rank = count($top_10) + 1;
                        $top_10[count($top_10)] = $data;
                    }
                }
            }
            $hasil->top_10 = $top_10;
            $hasil->data = $result;
        } else {
            $hasil = $result;
        }



        return $this->createSuccessMessage($hasil);
    }

    public function logout()
    {
        // delete all the data in user device/token
        Auth::logout();
        return $this->createSuccessMessage("Success logout");
    }

    public function do_add_detail_cuti(Request $request)
    {
        $detail = DetailJadwalCuti::where("id_pengganti", $request->id_pengganti)
            ->where("id_axie", $request->id_axie)
            ->whereMonth('created_at', '=', date('m'))
            ->get();

        $header_jadwal = JadwalCuti::where("id", $request->id_jadwal_cuti)->first();
        $date1 = new DateTime($header_jadwal->tanggal_cuti_mulai);
        $date2 = new DateTime($header_jadwal->tanggal_cuti_selesai);
        $interval = $date1->diff($date2);
        $total_day = $interval->d;

        foreach ($detail as $d) {
            $header = JadwalCuti::where("id", $d->id_jadwal_cuti)->first();
            $date1 = new DateTime($header->tanggal_cuti_mulai);
            $date2 = new DateTime($header->tanggal_cuti_selesai);

            $interval = $date1->diff($date2);
            $total_day += $interval->d;
            if ($total_day > 5) {
                return $this->createErrorMessage("User sudah memainkan axie tersebut lebih dari 5 hari", 400);
            }
        }

        $data = new DetailJadwalCuti();
        $data->id_jadwal_cuti = $request->id_jadwal_cuti;
        $data->id_pengganti = $request->id_pengganti;
        $data->id_axie = $request->id_axie;
        $data->save();

        return $data;
    }

    public function get_detail_cuti(Request $request)
    {
        $data = new stdClass();
        $detail_jadwal_cuti = DetailJadwalCuti::where("detail_jadwal_cuti.id_jadwal_cuti", $request->id)
            ->join("users", "users.id", "detail_jadwal_cuti.id_pengganti")
            ->join("master_axie", "master_axie.id", "detail_jadwal_cuti.id_axie")
            ->select("users.name", "master_axie.email_akun", "detail_jadwal_cuti.*")
            ->get();
        $data_user = JadwalCuti::where("jadwal_cuti.id", $request->id)
            ->join("users", "users.id", "jadwal_cuti.id_user")
            ->select("users.name", "jadwal_cuti.*")
            ->first();

        $data_axie_user = UserAxie::where("user_axie.id_user", $data_user->id_user)
            ->join("master_axie", "master_axie.id", "user_axie.id_axie")
            ->select("user_axie.*", "master_axie.email_akun")
            ->get();

        $data_axie_available = [];
        foreach ($data_axie_user as $dt) {
            $exist = false;
            foreach ($detail_jadwal_cuti as $d) {
                if ($d->id_axie == $dt->id_axie) {
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                $data_axie_available[] = $dt;
            }
        }
        $data->jadwal_cuti = $detail_jadwal_cuti;
        $data->axie_user = $data_axie_available;
        return json_encode($data);
    }

    public function delete_detail_cuti(Request $request)
    {
        $data = DetailJadwalCuti::where("id", $request->id)->delete();
        // $data->delete();

        if ($data) {
            return $this->createSuccessMessage("Success Delete Data");
        } else {
            return $this->createErrorMessage("Failed Delete Data", 400);
        }
    }

    public function changePassword(Request $request)
    {
        if (!Auth::check()) return $this->createSuccessMessage(null, 200, "Please login first");
        $user = Auth::user();

        if ($user->role == 2) {
            if (isset($request->id_user)) {
                $user->id = $request->id_user;
            }
        }
        $new_password = Hash::make($request->new_password);

        $data = Users::where("id", $user->id)->first();
        $data->password = $new_password;
        $data->save();

        return $this->createSuccessMessage("Password berhasil diganti");
    }

    public function add_kantor(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        // $user = Auth::user();
        // if ($user->role != 2 && $user->role != 3) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        $data = new MasterKantor();
        $data->id_owner = $request->id_owner;
        $data->alamat = $request->alamat;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function get_dashboard(Request $request)
    {
        $data_report = DailyReport::whereDate("created_at", Carbon::today())
            ->where("daily_report.status", 1)
            ->where("daily_report.slp_adventure", "<>", null)
            ->get();
        if (isset($request->id_owner)) {
            $data_report = DailyReport::whereDate("daily_report.created_at", Carbon::today())
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->where("master_axie.id_owner", $request->id_owner)
                ->where("daily_report.status", 1)
                ->where("daily_report.slp_adventure", "<>", null)
                ->select("daily_report.*")
                ->get();
        }
        $total = 0;
        $total_slp = 0;
        foreach ($data_report as $dt) {
            if ($dt->slp_adventure > 0 || $dt->slp_pvp > 0) {
                $total_slp += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                $total++;
            }
        }

        $average_slp_today = 0;
        $total_gain_slp_today = $total_slp;
        if ($total > 0) {
            $average_slp_today = $total_slp / $total;
        }


        $data_report = DailyReport::whereDate("created_at", Carbon::today()->subDays(1))
            ->where("daily_report.status", 1)
            ->where("daily_report.slp_adventure", "<>", null)
            ->get();
        if (isset($request->id_owner)) {
            $data_report = DailyReport::whereDate("daily_report.created_at", Carbon::today()->subDays(1))
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->where("master_axie.id_owner", $request->id_owner)
                ->where("daily_report.status", 1)
                ->where("daily_report.slp_adventure", "<>", null)
                ->select("daily_report.*")
                ->get();
        }
        $total = 0;
        $total_slp = 0;
        foreach ($data_report as $dt) {
            if ($dt->slp_adventure > 0 || $dt->slp_pvp > 0) {
                $total_slp += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                $total++;
            }
        }

        $average_slp_yesterday = 0;
        $total_gain_slp_yesterday = $total_slp;
        if ($total > 0) {
            $average_slp_yesterday = $total_slp / $total;
        }


        $data_report = DailyReport::whereDate("created_at", Carbon::today()->subDays(2))
            ->where("daily_report.status", 1)
            ->where("daily_report.slp_adventure", "<>", null)
            ->get();
        if (isset($request->id_owner)) {
            $data_report = DailyReport::whereDate("daily_report.created_at", Carbon::today()->subDays(2))
                ->join("master_axie", "master_axie.id", "daily_report.id_axie")
                ->where("master_axie.id_owner", $request->id_owner)
                ->where("daily_report.status", 1)
                ->where("daily_report.slp_adventure", "<>", null)
                ->select("daily_report.*")
                ->get();
        }
        $total = 0;
        $total_slp = 0;
        foreach ($data_report as $dt) {
            if ($dt->slp_adventure > 0 || $dt->slp_pvp > 0) {
                $total_slp += $dt->slp_adventure + $dt->slp_pvp + $dt->slp_quest;
                $total++;
            }
        }

        $average_slp_2days = 0;
        $total_gain_slp_2days = $total_slp;
        if ($total > 0) {
            $average_slp_2days = $total_slp / $total;
        }

        $data = new stdClass();
        $data->average_slp_today = number_format($average_slp_today, 0, ",", ".");
        $data->average_slp_yesterday = number_format($average_slp_yesterday, 0, ",", ".");
        $data->average_slp_2days = number_format($average_slp_2days, 0, ",", ".");
        $data->total_gain_slp_today = number_format($total_gain_slp_today, 0, ",", ".");
        $data->total_gain_slp_yesterday = number_format($total_gain_slp_yesterday, 0, ",", ".");
        $data->total_gain_slp_2days = number_format($total_gain_slp_2days, 0, ",", ".");
        return $this->createSuccessMessage($data);
    }

    public function add_reimburs(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();

        if (!isset($request->tanggal) || !isset($request->keterangan) || !isset($request->nominal) || !isset($request->url_bukti)) {
            return $this->createErrorMessage("Parameter tidak lengkap", 400);
        }
        $id_user = $user->id;

        $data = new Reimburs();
        $data->id_user = $id_user;
        $data->tanggal = $request->tanggal;
        $data->keterangan = $request->keterangan;
        $data->nominal = $request->nominal;
        $data->status = 0;
        $data->url_bukti = $request->url_bukti;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function get_list_reimburs()
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        $role = MasterRole::where("id", $user->role)->first();
        if ($user->role == 1 || $user->role == 5) {
            $data = Reimburs::where("id_user", $user->id)
                ->get();
        } else if ($user->role == 2) {
            $data = Reimburs::join("users", "users.id", "reimburs.id_user")
                ->select("reimburs.*", "users.email")
                ->get();
        } else if ($role->jabatan == 2) {
            $data = Reimburs::join("users", "users.id", "reimburs.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $user->id)
                ->select("reimburs.*", "users.email")
                ->get();
        } else if ($role->jabatan == 1) {
            $work_at = MasterKantor::where("id", $user->id_kantor)->first();
            $data = Reimburs::join("users", "users.id", "reimburs.id_user")
                ->join("master_kantor", "master_kantor.id", "users.id_kantor")
                ->where("master_kantor.id_owner", $work_at->id_owner)
                ->select("reimburs.*", "users.email")
                ->get();
        }
        return $this->createSuccessMessage($data);
    }

    public function approve_reimburs(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        // if ($user->role != 2 && $user->role != 3 && $user->role != 6) {
        //     return $this->createErrorMessage("User not allowed", 400);
        // }

        $id_reimburs = $request->id_reimburs;
        $status = $request->status;

        if ($status == 1) {
            //DITERIMA
            $data = Reimburs::where('id', $id_reimburs)->first();
            $data->status = 1;
            $data->save();

            $log = new LogServer();
            $log->description = strtoupper($user->email) . " APPROVE REIMBURS (" . $data->id . ") " . strtoupper($data->keterangan) . " DENGAN NOMINAL " . $data->nominal;
            $log->save();

            $gaji = new Gaji();
            $gaji->id_user = $data->id_user;
            $gaji->id_daily_report = null;
            $gaji->gaji = $data->nominal;
            $gaji->type = "REIMBURS";
            $gaji->id_axie = null;
            $gaji->id_reimburs = $data->id;
            $gaji->save();

            return $this->createSuccessMessage("Reimburs diterima");
        } else {
            //DITOLAK
            $data = Reimburs::where('id', $id_reimburs)->first();
            $data->status = -1;
            $data->save();

            $log = new LogServer();
            $log->description = strtoupper($user->email) . " REJECT REIMBURS (" . $data->id . ") " . strtoupper($data->keterangan) . " DENGAN NOMINAL " . $data->nominal;
            $log->save();

            return $this->createSuccessMessage("Reimburs ditolak");
        }
    }

    public function updatePassword(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }
        $user = Auth::user();
        if ($user->role != 2) {
            return $this->createErrorMessage("User not allowed", 400);
        }


        $data = Users::where("id", $request->id_user)->first();
        $data->password =  Hash::make($request->password);
        $data->save();

        return $this->createSuccessMessage($data, 200, "Password Changed");
    }

    public function add_coach_user(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $exist = CoachUsers::where("id_user", $request->id_user)
            ->first();
        if (isset($exist)) {
            return $this->createErrorMessage("User already coached", 400);
        }

        $data = new CoachUsers();
        $data->id_coach = $request->id_coach;
        $data->id_user = $request->id_user;
        $data->save();

        return $this->createSuccessMessage($data);
    }

    public function delete_coach_user(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $data = CoachUsers::where("id", $request->id)->delete();

        if ($data) {
            return $this->createSuccessMessage("Success Delete Data");
        } else {
            return $this->createErrorMessage("Failed Delete Data", 400);
        }
    }

    public function get_coach_user(Request $request)
    {
        if (!Auth::check()) {
            return $this->createSuccessMessage(null, 200, "Please login first");
        }

        $data = CoachUsers::where("id_coach", $request->id_coach)
            ->join("users", "users.id", "couch_users.id_user")
            ->select("couch_users.id", "users.name", "couch_users.created_at")
            ->get();

        return $this->createSuccessMessage($data);
    }
}
