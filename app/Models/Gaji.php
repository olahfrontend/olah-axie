<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use stdClass;

class Gaji extends Model
{
    //
    protected $table = 'gaji';

    public static function hitungBonusPVP($daily_report, $gaji, $month, $year)
    {
        $data_pvp = [];
        $id_user = 0;
        foreach ($daily_report as $ind => $dt) {
            $id_user = $dt->id_user;
            $index = count($data_pvp);
            foreach ($data_pvp as $i => $dts) {
                if ($dts->id_axie == $dt->id_axie) {
                    $index = $i;
                    break;
                }
            }
            if (!isset($data_pvp[$index])) {
                $data_pvp[$index] = new stdClass();
                $data_pvp[$index]->id_axie = $dt->id_axie;
                $data_pvp[$index]->total_slp_pvp = 0;
                $data_pvp[$index]->total_play = 0;
                $data_pvp[$index]->data_axie = MasterAxie::where("id", $dt->id_axie)->first();
            }
            if ($dt->status_server == 1 && $dt->level_before > 15) {
                $data_pvp[$index]->total_slp_pvp += $dt->slp_pvp;
            } else if ($dt->status_server == 0 && $dt->level_before > 15) {
                if ($ind > 0) {
                    //CEK KALAU SERVER ERROR CEK APAKAH DIA PUSH HARI ITU 
                    //UNTUK CEK APAKAH DIA PUSH BANDINGKAN HARI SEBELUMNYA JIKA BEDA 40% MAKA DIANGGAP TIDAK PUSH
                    $day1_format_ymd = Date("Y-m-d", strtotime($daily_report[$ind - 1]->created_at));
                    $day2_format_ymd = Date("Y-m-d", strtotime($daily_report[$ind]->created_at));
                    $day1 = DB::select("
                    SELECT DATE(daily_report.created_at) as tanggal, AVG(daily_report.slp_pvp) as average 
                    FROM daily_report,master_axie 
                    WHERE master_axie.id = daily_report.id_axie 
                    AND
                    master_axie.id_player = daily_report.id_user 
                    AND 
                    daily_report.id_user = " . $daily_report[$ind - 1]->id_user . " 
                    AND 
                    DATE(daily_report.created_at) = '" . $day1_format_ymd . "'
                    GROUP BY DATE(daily_report.created_at)");

                    $day2 = DB::select("
                    SELECT DATE(daily_report.created_at) as tanggal, AVG(daily_report.slp_pvp) as average 
                    FROM daily_report,master_axie 
                    WHERE master_axie.id = daily_report.id_axie 
                    AND
                    master_axie.id_player = daily_report.id_user 
                    AND 
                    daily_report.id_user = " . $daily_report[$ind]->id_user . " 
                    AND 
                    DATE(daily_report.created_at) = '" . $day2_format_ymd . "'
                    GROUP BY DATE(daily_report.created_at)");

                    // echo $daily_report[$ind]->id_user . "\n";
                    // echo $day1_format_ymd . "\n";
                    // print_r($day1);
                    // echo "\n";
                    if (isset($day1[0]) && isset($day2[0])) {
                        $average_1 = $day1[0]->average;
                        $average_2 = $day2[0]->average;
                        // SYARAT SELISIH 20% DARI AVERAGE SEBELUMNYA
                        $syarat = $average_1 * 20 / 100;
                        if ($average_2 >= $syarat) {
                            //DIHITUNG PVPNYA -> DIANGGAP PUSH
                            $data_pvp[$index]->total_slp_pvp += $dt->slp_pvp;
                        }
                    }
                }
            }
        }

        foreach ($gaji as $dt) {
            //TAMBAH TOTAL PLAY AXIE
            if ($dt->type == "GAJI_HARIAN") {
                $data_daily_report = DailyReport::where('id', $dt->id_daily_report)->first();
                //SYARAT PVP YG DIHITUNG LEVEL 15 KE ATAS SAJA
                if ($data_daily_report->level_before > 15) {
                    $index = count($data_pvp);
                    foreach ($data_pvp as $i => $dts) {
                        if ($dts->id_axie == $dt->id_axie) {
                            $index = $i;
                            break;
                        }
                    }
                    if (!isset($data_pvp[$index])) {
                        $data_pvp[$index] = new stdClass();
                        $data_pvp[$index]->id_axie = $dt->id_axie;
                        $data_pvp[$index]->total_slp_pvp = 0;
                        $data_pvp[$index]->total_play = 0;
                    }
                    $data_pvp[$index]->total_play += 1;
                }
            }
        }


        $average_pvp = 0;
        $id_owner = 0;
        $total_akun = 0;
        $total_akun_persentase = 0;

        //GET TOTAL CUTI PADA BULAN ITU
        $total_cuti = JadwalCuti::where("id_user", $id_user)
            ->whereMonth("tanggal_cuti_mulai", "=", $month)
            ->whereYear("tanggal_cuti_mulai", "=", $year)
            ->selectRaw("SUM(DATEDIFF(tanggal_cuti_selesai,tanggal_cuti_mulai)) AS total")
            ->first();


        $dateFormat = $year . '-' . $month . '-01';
        $format = 'Y-m-d';

        $end_month = Carbon::createFromFormat($format, $dateFormat)->endOfMonth();
        $total_day_in_month = $end_month->format('d');
        // return $total_cuti->total;

        foreach ($data_pvp as $dt) {
            if ($dt->total_play <= 0) {
                $dt->avg_slp = 0;
            } else {
                $dt->avg_slp = number_format($dt->total_slp_pvp / $dt->total_play, 2);
            }
            $axie = MasterAxie::where("id", $dt->id_axie)->first();
            //SYARAT PVP DIHITUNG KETIKA TOTAL PLAY LEBIH DARI 7
            if ($dt->total_play >= 7) {
                $average_pvp += $dt->avg_slp;
                if ($id_owner <= 0) {
                    //AMBIL OWNER YANG PERTAMA KALI SAJA?
                    $id_owner = $axie->id_owner;
                }
                $total_akun++;

                $total_akun_persentase += round(($dt->total_play / ($total_day_in_month - $total_cuti->total)), 1);
            }
        }

        //HITUNG AVERAGE PVP
        // $average_pvp = 0;
        if ($total_akun > 0) {
            $average_pvp = number_format($average_pvp / $total_akun, 0);
        }
        $bonus_pvp = SettingPVP::getValue($id_owner, $average_pvp);

        $est_bonus_pvp = 0;
        if ($bonus_pvp->value > 0) {
            //HITUNG ESTIMASI BONUS PVP
            $est_bonus_pvp = round($average_pvp * $bonus_pvp->value * $total_akun_persentase, 0);
        }

        $result = new stdClass();
        $result->est_bonus_pvp = $est_bonus_pvp;
        $result->average_pvp = $average_pvp;
        $result->data_pvp = $data_pvp;
        $result->total_akun_persentase = $total_akun_persentase;
        return $result;
    }
}
