<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class Users extends Model
{
    //
    protected $table = 'users';
    protected $primaryKey = 'id';

    public static function getTotalGajiBulanIni($id_user)
    {
        $data = Gaji::where("id_user", $id_user)->whereBetween('created_at', [Carbon::now()->startOfMonth(), Carbon::now()->endOfMonth()])->sum("gaji");
        return $data;
    }
}
