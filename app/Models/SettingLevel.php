<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingLevel extends Model
{
    //
    protected $table = 'setting_level';

    public static function getMinimalSLP($id_owner, $level)
    {
        $data = SettingLevel::where("min_level", "<=", $level)
            ->where("max_level", ">=", $level)
            ->where("id_user", $id_owner)
            ->first();

        return $data;
    }
}
