<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    //
    protected $table = 'user_contact';

    public static function getContactCount($user_id)
    {
        $data = UserContact::select('telephone as telephone')
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'DESC')
            ->first();
        return count($data);
    }

    public static function getContactbyTelephone($telephone, $user_id)
    {
        $data = UserContact::select('telephone as telephone')
            ->where('telephone', $telephone)
            ->where('user_id', $user_id)
            ->orderBy('created_at', 'DESC')
            ->get();
        if (count($data) > 0) {
            return true;
        }
        return false;
    }
}
