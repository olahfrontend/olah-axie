<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class DailyReport extends Model
{
    //
    protected $table = 'daily_report';
    public $timestamp = false;

    public static function isComplete($id_user, $id_axie)
    {
        $data = DailyReport::where("id_user", $id_user)
            ->where("id_axie", $id_axie)
            ->whereDate("created_at", Carbon::today())
            ->first();


        if (isset($data)) {
            $jam = Carbon::parse($data->created_at)->format("H");
            if ($jam >= 9) {
                return true;
            }
        }
        return false;
    }
}
