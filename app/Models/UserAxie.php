<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAxie extends Model
{
    //
    protected $table = 'user_axie';

    public static function isValid($id_user, $id_axie)
    {
        $result = UserAxie::where("id_user", $id_user)->where("id_axie", $id_axie)->first();
        if (isset($result)) {
            return true;
        }
        return false;
    }
}
