<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use stdClass;

class SettingPVP extends Model
{
    //
    protected $table = 'setting_pvp';

    public static function getValue($id_owner, $average_slp)
    {
        $data = SettingPVP::where("min_slp", "<=", $average_slp)
            ->where("max_slp", ">=", $average_slp)
            ->where("id_user", $id_owner)
            ->first();

        if (!isset($data)) {
            // echo "asd";
            $data = new stdClass();
            $data->value = 0;
            // $data = json_encode($data);
        }
        // print_r($data);
        // echo $data;
        return $data;
    }
}
