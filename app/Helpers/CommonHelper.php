<?php

namespace App\Helpers;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Mail;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

class CommonHelper
{

    public static function redirectURL($url)
    {
        echo '<script>window.location.href = "' . $url . '"</script>';
    }

    public static function showAlert($judul, $isi, $tipe = "info", $url = "")
    {
        //tipe-> error, successs,info
        echo '<div style="display:none"></div>';
        echo '<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>';
        if ($url == "back") {
            //SHOW MODAL & BACK 1 PAGE
            echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '").then((value)=>{
                    window.location.href = document.referrer;
                });
            </script>';
        } else if ($url != "") {
            //SHOW MODAL & GO TO URL
            echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '").then((value)=>{
                    window.location.href = "' . $url . '";
                });
            </script>';
        } else {
            //DEFAULT HANYA SHOW MODAL
            echo '
            <script>
                swal("' . $judul . '", "' . $isi . '", "' . $tipe . '");
            </script>';
        }
    }

    // public static function sendMail($view, $data, $to_name, $to_email, $subject, $from, $name)
    // {
    //     Mail::send($view, compact('data'), function ($message) use ($to_name, $to_email, $from, $subject, $name) {
    //         $message->to($to_email, $to_name)
    //             ->subject($subject);

    //         $message->from($from, $name);
    //     });
    // }

    public static function getAPI($url)
    {
        $request = Request::create($url, 'GET');

        $response = Route::dispatch($request);
        $res = json_decode($response->getContent());
        return $res;
    }

    public static function getAPIExternal($url)
    {

        //GET TIMESTAMP FROM COINBASE
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chPost, CURLOPT_URL, $url);
        $resultPost = curl_exec($chPost);
        curl_close($chPost);
        $jsonPost = json_decode($resultPost, true);

        return $jsonPost;
    }

    public static function convertDateFormat($data)
    {
        $date = date_create($data);
        $result = date_format($date, "Y-m-d");

        return $result;
    }

    public static function convertDateFormatFrontend($data)
    {
        $data = CommonHelper::convertDateFormat($data);
        $myDateTime = DateTime::createFromFormat('Y-m-d', $data);
        return $myDateTime->format('d M Y');
    }

    public static function sendMail($view, $data, $to_name, $to_email, $subject, $from, $name)
    {
        Mail::send($view, compact('data'), function ($message) use ($to_name, $to_email, $from, $subject, $name) {
            $message->to($to_email, $to_name)
                ->subject($subject);

            $message->from($from, $name);
        });
    }
    public static function getSelisihDay($date_start, $date_end)
    {
        $diff = strtotime($date_end) - strtotime($date_start);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

        return $days + ($months * 30) + ($years * 12 * 30);
    }

    public static function getSelisihMinutes($date_start, $date_end)
    {
        $diff = strtotime($date_end) - strtotime($date_start);
        $years = floor($diff / (365 * 60 * 60 * 24));
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
        $days = floor(($diff - ($years * 365 * 60 * 60 * 24) - ($months * 30 * 60 * 60 * 24)) / (60 * 60 * 24));
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / (60));

        // return $diff;
        // return $minutes;
        // echo strtotime($date_end)."\n";
        // echo strtotime($date_start)."\n";
        // echo $date_start . "\n";
        // echo $date_end . "\n";
        // echo $years . " years " . $months . " months " . $days . " days " . $hours . " hour " . $minutes . " minutes\n";

        return $minutes + ($hours * 60) + ($days * 24 * 60) + ($months * 30 * 24 * 60) + ($years * 12 * 30 * 24 * 60);
        // return $years . " years, " . $months . " months, " . $days . " days\n";
    }

    public static function checkLogin()
    {
        if (!Auth::check()) {
            return null;
        }
        $user = Auth::user();
        return $user;
    }

    public static function checkSession()
    {
        $initialData = CommonHelper::getAPI("/api/initial_data");
        // print_r($initialData);
        if (!isset($initialData->payload)) {
            return null;
        }
        return $initialData;
    }
}
